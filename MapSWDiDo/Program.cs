﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Security.Policy;

namespace MapSWDiDo
{
    class Program
    {
        static void Main(string[] args)
        {
            string _StrCnx_2 = "Data Source=192.168.2.54\\MSSQLSERVER2012;Initial Catalog=FdiaryToDo;User ID=HumanResources;Password=sa123";

            //Boolean typeMap = true;

            //if (typeMap)
            //{
                Console.WriteLine(">>>Map-Init SP" + " Map(s) on Time: [" + DateTime.Now.ToString("HH:mm:ss") + "]");
                using (SqlConnection connUpd = new SqlConnection())
                {
                    connUpd.ConnectionString = _StrCnx_2;
                    connUpd.Open();
                    SqlCommand commandUpd = new SqlCommand("EXEC MapETLSWDeDo 1", connUpd);
                    commandUpd.ExecuteNonQuery();
                    connUpd.Close();
                }
                Console.WriteLine(">>>Map-Success SP " + " Map(s) on Time: [" + DateTime.Now.ToString("HH:mm:ss") + "]");
            //}
            //else
            //{

            //    using (SqlConnection connUpd = new SqlConnection())
            //    {
            //        connUpd.ConnectionString = _StrCnx_2;
            //        connUpd.Open();
            //        SqlCommand commandUpd = new SqlCommand("Delete from FHTestClaim", connUpd);
            //        commandUpd.ExecuteNonQuery();
            //        connUpd.Close();
            //    }

            //    using (SqlConnection conn = new SqlConnection())
            //    {
            //        conn.ConnectionString = _StrCnx;
            //        conn.Open();

            //        SqlCommand command = new SqlCommand("" +
            //            "SELECT " +
            //                "fh_temp.user_id as touch_user_id, " +
            //                "fh_temp.claim_id, " +
            //                "FORMAT(fh_temp.log_datetime, 'MM/dd/yyyy hh:mm', 'en-US') as log_datetime, " +
            //                "(fh_u.first_name + ' ' + fh_u.last_name) as examiner_name, " +
            //                "fc.fh_claim_num as claim_num, " +
            //                "IIF(patindex('%WIP%', fch.fh_claim_num) > 0, fch.fh_claim_num, '--') AS IntakeWIP#, " +
            //                "fc.open_close_status, " +
            //                "fp.policy_desc as LIA_PD_CARGO, " +
            //                "IIF(patindex('%Lia%', fp.policy_desc) > 0, 'Lia', '--') AS Lia, " +
            //                "IIF(patindex('%Physical Damage%', fp.policy_desc) > 0, " +
            //                                                    "'PD' " +
            //                                                    ", IIF(patindex('%Physical%', fp.policy_desc) > 0, '', '--')) AS PD, " +
            //                "IIF(patindex('%Cargo%', fp.policy_desc) > 0, 'Cargo', '--') AS Cargo, " +
            //                "fp.policy_num, " +
            //                "(CONVERT(varchar, fp.policy_begin_date, 101) + ' - ' + CONVERT(varchar, fp.policy_end_date, 101)) as Policy_Effective_Date, " +
            //                "fp.holder_state, " +
            //                "fc.controlling_state, " +
            //                "fp.primary_holder_name as NameINs_DBA, " +
            //                "fcg.item_name as BrokerName, " +
            //                "(fa.first_name + ' ' + fa.middle_initial + ' ' + fa.last_name) as ClaimantName, " +
            //                "fl.defendant_attorney_name as Assigned_to_DC_Name, " +
            //                "fl.trial_date as Assigned_to_DC_Date, " +
            //                "temp_fh_reserve_paid.sum_alae_amt as sum_alae_amt, " +
            //                "temp_fh_reserve_paid.sum_loss_amt as sum_loss_amt, " +
            //                "temp_fh_reserve_paid.sum_alae_amtNegative as sum_alae_amtNegative, " +
            //                "temp_fh_reserve_paid.sum_alae_amtPositive as sum_alae_amtPositive, " +
            //                "temp_fh_reserve_paid.sum_loss_amtNegative as sum_loss_amtNegative, " +
            //                "temp_fh_reserve_paid.sum_loss_amtPositive as sum_loss_amtPositive, " +
            //                "DATEDIFF(day, log_datetime, GETDATE()) as day_diff, " +
            //                "FORMAT(fh_temp.log_datetime, 'MMddyyyyhhmmss', 'en-US') as log_datetime_id, " +
            //                "fh_temp.claim_task, " +
            //                "fc.handler_id, " +
            //                "fh_temp.user_id as touch_user_id, " +
            //                "fh_temp.touch_claim_name, " +
            //                "temp_fh_reserve_paid.P_sum_alae_amt as P_sum_alae_amt, " +
            //                "temp_fh_reserve_paid.P_sum_loss_amt as P_sum_loss_amt, " +
            //                "temp_fh_reserve_paid.P_sum_alae_amtNegative as P_sum_alae_amtNegative, " +
            //                "temp_fh_reserve_paid.P_sum_alae_amtPositive as P_sum_alae_amtPositive, " +
            //                "temp_fh_reserve_paid.P_sum_loss_amtNegative as P_sum_loss_amtNegative, " +
            //                "temp_fh_reserve_paid.P_sum_loss_amtPositive as P_sum_loss_amtPositive, " +
            //                "temp_fh_reserve_paid.R_sum_alae_amt as R_sum_alae_amt, " +
            //                "temp_fh_reserve_paid.R_sum_loss_amt as R_sum_loss_amt, " +
            //                "temp_fh_reserve_paid.R_sum_alae_amtNegative as R_sum_alae_amtNegative, " +
            //                "temp_fh_reserve_paid.R_sum_alae_amtPositive as R_sum_alae_amtPositive, " +
            //                "temp_fh_reserve_paid.R_sum_loss_amtNegative as R_sum_loss_amtNegative, " +
            //                "temp_fh_reserve_paid.R_sum_loss_amtPositive as R_sum_loss_amtPositive, " +
            //                "CONCAT(IIF(temp_fh_reserve_paid.P_sum_alae_amt <> 0.00, 'P', ''), IIF(temp_fh_reserve_paid.R_sum_alae_amt <> 0.00, 'R', '')) as Status_sum_alae_amt, " +
            //                "CONCAT(IIF(temp_fh_reserve_paid.P_sum_loss_amt <> 0.00, 'P', ''), IIF(temp_fh_reserve_paid.R_sum_loss_amt <> 0.00, 'R', '')) as Status_sum_loss_amt " +
            //            "FROM " +
            //            "(((((((((SELECT " +
            //                            "DISTINCT " +
            //                            "fcl_.user_id, " +
            //                            "fclg_.claim_id, " +
            //                            "fclg_.log_datetime, " +
            //                            "fcl_.claim_task, " +
            //                            "(fu_.first_name + ' ' + fu_.last_name) as touch_claim_name " +
            //                            "FROM " +
            //                                "( " +
            //                                    "select " +
            //                                            "MAX(log_datetime) as log_datetime, " +
            //                                            "claim_id " +
            //                                    "from fh_claim_log " +
            //                                    "group by claim_id " +
            //                                ") as fclg_ " +
            //                                "left join fh_claim_log as fcl_ on fcl_.claim_id = fclg_.claim_id and fcl_.log_datetime = fclg_.log_datetime " +
            //                                "left join fh_user as fu_ on fcl_.user_id = fu_.user_id " +
            //                            "WHERE " +
            //                                "DATEDIFF(day, fclg_.log_datetime, GETDATE()) <= 1 " +
            //            ") as fh_temp  LEFT JOIN fh_claim as fc on fc.claim_id = fh_temp.claim_id) " +
            //                            "LEFT JOIN fh_user as fh_u on fc.handler_id = fh_u.user_id) " +
            //                            "LEFT JOIN (select " +
            //                                            "claim_id, fh_claim_num " +
            //                                            "from fh_claim_history " +
            //                                            "where patindex('%WIP%', fh_claim_num)> 0 " +
            //                                            "GROUP BY claim_id, fh_claim_num " +
            //                                            "HAVING count(*) >= 1) as fch on fch.claim_id = fh_temp.claim_id) " +
            //                            "LEFT JOIN fh_policy as fp on fp.policy_id = fc.policy_id) " +
            //                            "LEFT JOIN fh_code_general as fcg on fcg.item_id = fc.claim_code3) " +
            //                            "LEFT JOIN fh_party as fa on fc.claim_id = fa.claim_id and fa.party_type_code = 'C' and fa.party_num = 1) " +
            //                            "LEFT JOIN fh_lawsuit as fl on fl.party_id = fa.party_id) " +
            //                            "LEFT JOIN (select " +
            //                                                "r.claim_id as claim_id, " +
            //                                                "sum(r.alae_amt) as sum_alae_amt, " +
            //                                                "sum(r.loss_amt) as sum_loss_amt, " +
            //                                                "sum(IIF(SIGN(r.alae_amt) = -1, r.alae_amt, 0)) as sum_alae_amtNegative, " +
            //                                                "sum(IIF(SIGN(r.alae_amt) = 1, r.alae_amt, 0)) as sum_alae_amtPositive, " +
            //                                                "sum(IIF(SIGN(r.loss_amt) = -1, r.loss_amt, 0)) as sum_loss_amtNegative, " +
            //                                                "sum(IIF(SIGN(r.loss_amt) = 1, r.loss_amt, 0)) as sum_loss_amtPositive, " +
            //                                                "sum(IIF(r.reserve_or_paid_code = 'P', r.alae_amt, 0)) as P_sum_alae_amt, " +
            //                                                "sum(IIF(r.reserve_or_paid_code = 'P', r.loss_amt, 0)) as P_sum_loss_amt, " +
            //                                                "sum(IIF(SIGN(r.alae_amt) = -1 AND r.reserve_or_paid_code = 'P', r.alae_amt, 0)) as P_sum_alae_amtNegative, " +
            //                                                "sum(IIF(SIGN(r.alae_amt) = 1 AND r.reserve_or_paid_code = 'P', r.alae_amt, 0)) as P_sum_alae_amtPositive, " +
            //                                                "sum(IIF(SIGN(r.loss_amt) = -1 AND r.reserve_or_paid_code = 'P', r.loss_amt, 0)) as P_sum_loss_amtNegative, " +
            //                                                "sum(IIF(SIGN(r.loss_amt) = 1 AND r.reserve_or_paid_code = 'P', r.loss_amt, 0)) as P_sum_loss_amtPositive, " +
            //                                                "sum(IIF(r.reserve_or_paid_code = 'R', r.alae_amt, 0)) as R_sum_alae_amt, " +
            //                                                "sum(IIF(r.reserve_or_paid_code = 'R', r.loss_amt, 0)) as R_sum_loss_amt, " +
            //                                                "sum(IIF(SIGN(r.alae_amt) = -1 AND r.reserve_or_paid_code = 'P', r.alae_amt, 0)) as R_sum_alae_amtNegative, " +
            //                                                "sum(IIF(SIGN(r.alae_amt) = 1 AND r.reserve_or_paid_code = 'P', r.alae_amt, 0)) as R_sum_alae_amtPositive, " +
            //                                                "sum(IIF(SIGN(r.loss_amt) = -1 AND r.reserve_or_paid_code = 'P', r.loss_amt, 0)) as R_sum_loss_amtNegative, " +
            //                                                "sum(IIF(SIGN(r.loss_amt) = 1 AND r.reserve_or_paid_code = 'P', r.loss_amt, 0)) as R_sum_loss_amtPositive " +
            //                                      "from " +
            //                                                "fh_claim c " +
            //                                      "inner join " +
            //                                                "fh_reserve_paid r " +
            //                                                                        "on c.claim_id = r.claim_id and r.reserve_or_paid_code in ('P', 'R') " +
            //                                      "group by r.claim_id) as temp_fh_reserve_paid on temp_fh_reserve_paid.claim_id = fc.claim_id)", conn);



            //        using (SqlDataReader reader = command.ExecuteReader())
            //        {
            //            if (reader.HasRows)
            //            {
            //                int iMap = 0;
            //                while (reader.Read())
            //                {
            //                    try
            //                    {
            //                        using (SqlConnection connUpd = new SqlConnection())
            //                        {

            //                            connUpd.ConnectionString = _StrCnx_2;
            //                            connUpd.Open();

            //                            string stringSQL = "INSERT INTO FHTestClaim (" +
            //                                                                                          "user_id," +
            //                                                                                          "claim_id, " +
            //                                                                                          "log_datetime, " +
            //                                                                                          "examiner_name, " +
            //                                                                                          "claim_num, " +
            //                                                                                          "IntakeWIP#, " +
            //                                                                                          "open_close_status, " +
            //                                                                                          "LIA_PD_CARGO, " +
            //                                                                                          "Lia, " +
            //                                                                                          "PD, " +
            //                                                                                          "Cargo, " +
            //                                                                                          "policy_num, " +
            //                                                                                          "Policy_Effective_Date, " +
            //                                                                                          "holder_state, " +
            //                                                                                          "controlling_state, " +
            //                                                                                          "NameINs_DBA, " +
            //                                                                                          "BrokerName, " +
            //                                                                                          "ClaimantName, " +
            //                                                                                          "Assigned_to_DC_Name, " +
            //                                                                                          "Assigned_to_DC_Date, " +
            //                                                                                          "sum_alae_amt, " +
            //                                                                                          "sum_loss_amt, " +
            //                                                                                          "sum_alae_amtNegative, " +
            //                                                                                          "sum_alae_amtPositive, " +
            //                                                                                          "sum_loss_amtNegative, " +
            //                                                                                          "sum_loss_amtPositive, " +
            //                                                                                          "diff_day, " +
            //                                                                                          "diff_date," +
            //                                                                                          "map_date, " +
            //                                                                                          "log_datetime_id, " +
            //                                                                                          "claim_task, " +
            //                                                                                          "handler_id, " +
            //                                                                                          "touch_user_id, " +
            //                                                                                          "touch_claim_name, " +
            //                                                                                          "sync_status)" +
            //                                                                               "VALUES(" +
            //                                                                                          "@user_id," +
            //                                                                                          "@claim_id, " +
            //                                                                                          "@log_datetime, " +
            //                                                                                          "@examiner_name, " +
            //                                                                                          "@claim_num, " +
            //                                                                                          "@IntakeWIP#, " +
            //                                                                                          "@open_close_status, " +
            //                                                                                          "@LIA_PD_CARGO, " +
            //                                                                                          "@Lia, " +
            //                                                                                          "@PD, " +
            //                                                                                          "@Cargo, " +
            //                                                                                          "@policy_num, " +
            //                                                                                          "@Policy_Effective_Date, " +
            //                                                                                          "@holder_state, " +
            //                                                                                          "@controlling_state, " +
            //                                                                                          "@NameINs_DBA, " +
            //                                                                                          "@BrokerName, " +
            //                                                                                          "@ClaimantName, " +
            //                                                                                          "@Assigned_to_DC_Name, " +
            //                                                                                          "@Assigned_to_DC_Date, " +
            //                                                                                          "@sum_alae_amt, " +
            //                                                                                          "@sum_loss_amt, " +
            //                                                                                          "@sum_alae_amtNegative, " +
            //                                                                                          "@sum_alae_amtPositive, " +
            //                                                                                          "@sum_loss_amtNegative, " +
            //                                                                                          "@sum_loss_amtPositive, " +
            //                                                                                          "@diff_day, " +
            //                                                                                          "@diff_date, " +
            //                                                                                          "@map_date, " +
            //                                                                                          "@log_datetime_id, " +
            //                                                                                          "@claim_task, " +
            //                                                                                          "@handler_id, " +
            //                                                                                          "@touch_user_id, " +
            //                                                                                          "@touch_claim_name, " +
            //                                                                                          "@sync_status)";

            //                            SqlCommand commandUpd = new SqlCommand(stringSQL, connUpd);
            //                            commandUpd.Parameters.AddWithValue("@user_id", reader[0]);
            //                            commandUpd.Parameters.AddWithValue("@claim_id", reader[1]);
            //                            commandUpd.Parameters.AddWithValue("@log_datetime", reader[2]);
            //                            commandUpd.Parameters.AddWithValue("@examiner_name", reader[3]);
            //                            commandUpd.Parameters.AddWithValue("@claim_num", reader[4]);
            //                            commandUpd.Parameters.AddWithValue("@IntakeWIP#", reader[5]);
            //                            commandUpd.Parameters.AddWithValue("@open_close_status", reader[6]);
            //                            commandUpd.Parameters.AddWithValue("@LIA_PD_CARGO", reader[7]);
            //                            commandUpd.Parameters.AddWithValue("@Lia", reader[8]);
            //                            commandUpd.Parameters.AddWithValue("@PD", reader[9]);
            //                            commandUpd.Parameters.AddWithValue("@Cargo", reader[10]);
            //                            commandUpd.Parameters.AddWithValue("@policy_num", reader[11]);
            //                            commandUpd.Parameters.AddWithValue("@Policy_Effective_Date", reader[12]);
            //                            commandUpd.Parameters.AddWithValue("@holder_state", reader[13]);
            //                            commandUpd.Parameters.AddWithValue("@controlling_state", reader[14]);
            //                            commandUpd.Parameters.AddWithValue("@NameINs_DBA", reader[15]);
            //                            commandUpd.Parameters.AddWithValue("@BrokerName", reader[16]);
            //                            commandUpd.Parameters.AddWithValue("@ClaimantName", reader[17]);
            //                            commandUpd.Parameters.AddWithValue("@Assigned_to_DC_Name", reader[18]);
            //                            commandUpd.Parameters.AddWithValue("@Assigned_to_DC_Date", reader[19]);
            //                            commandUpd.Parameters.AddWithValue("@sum_alae_amt", reader[20]);
            //                            commandUpd.Parameters.AddWithValue("@sum_loss_amt", reader[21]);
            //                            commandUpd.Parameters.AddWithValue("@sum_alae_amtNegative", reader[22]);
            //                            commandUpd.Parameters.AddWithValue("@sum_alae_amtPositive", reader[23]);
            //                            commandUpd.Parameters.AddWithValue("@sum_loss_amtNegative", reader[24]);
            //                            commandUpd.Parameters.AddWithValue("@sum_loss_amtPositive", reader[25]);
            //                            commandUpd.Parameters.AddWithValue("@diff_day", reader[26]);
            //                            commandUpd.Parameters.AddWithValue("@diff_date", Convert.ToString(reader[2]) + " - " + Convert.ToString((DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"))));
            //                            commandUpd.Parameters.AddWithValue("@map_date", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            //                            commandUpd.Parameters.AddWithValue("@sync_status", "0");
            //                            commandUpd.Parameters.AddWithValue("@log_datetime_id", reader[27]);
            //                            commandUpd.Parameters.AddWithValue("@claim_task", reader[28]);
            //                            commandUpd.Parameters.AddWithValue("@handler_id", reader[29]);
            //                            commandUpd.Parameters.AddWithValue("@touch_user_id", reader[30]);
            //                            commandUpd.Parameters.AddWithValue("@touch_claim_name", reader[31]);

            //                            commandUpd.ExecuteReader();

            //                            stringSQL = stringSQL.Replace("@user_id", "'" + Convert.ToString(reader[0]) + "'");
            //                            stringSQL = stringSQL.Replace("@claim_id", "'" + Convert.ToString(reader[1]) + "'");
            //                            stringSQL = stringSQL.Replace("@log_datetime", "'" + Convert.ToString(reader[2]) + "'");
            //                            stringSQL = stringSQL.Replace("@examiner_name", "'" + Convert.ToString(reader[3]) + "'");
            //                            stringSQL = stringSQL.Replace("@claim_num", "'" + Convert.ToString(reader[4]) + "'");
            //                            stringSQL = stringSQL.Replace("@IntakeWIP#", "'" + Convert.ToString(reader[5]) + "'");
            //                            stringSQL = stringSQL.Replace("@open_close_status", "'" + Convert.ToString(reader[6]) + "'");
            //                            stringSQL = stringSQL.Replace("@LIA_PD_CARGO", "'" + Convert.ToString(reader[7]) + "'");
            //                            stringSQL = stringSQL.Replace("@Lia", "'" + Convert.ToString(reader[8]) + "'");
            //                            stringSQL = stringSQL.Replace("@PD", "'" + Convert.ToString(reader[9]) + "'");
            //                            stringSQL = stringSQL.Replace("@Cargo", Convert.ToString(reader[10]) + "'");
            //                            stringSQL = stringSQL.Replace("@policy_num", "'" + Convert.ToString(reader[11]) + "'");
            //                            stringSQL = stringSQL.Replace("@Policy_Effective_Date", "'" + Convert.ToString(reader[12]) + "'");
            //                            stringSQL = stringSQL.Replace("@holder_state", "'" + Convert.ToString(reader[13]) + "'");
            //                            stringSQL = stringSQL.Replace("@controlling_state", "'" + Convert.ToString(reader[14]) + "'");
            //                            stringSQL = stringSQL.Replace("@NameINs_DBA", "'" + Convert.ToString(reader[15]) + "'");
            //                            stringSQL = stringSQL.Replace("@BrokerName", "'" + Convert.ToString(reader[16]) + "'");
            //                            stringSQL = stringSQL.Replace("@ClaimantName", "'" + Convert.ToString(reader[17]) + "'");
            //                            stringSQL = stringSQL.Replace("@Assigned_to_DC_Name", "'" + Convert.ToString(reader[18]) + "'");
            //                            stringSQL = stringSQL.Replace("@Assigned_to_DC_Date", "'" + Convert.ToString(reader[19]) + "'");
            //                            stringSQL = stringSQL.Replace("@sum_alae_amt", "'" + Convert.ToString(reader[20]) + "'");
            //                            stringSQL = stringSQL.Replace("@sum_loss_amt", "'" + Convert.ToString(reader[21]) + "'");
            //                            stringSQL = stringSQL.Replace("@sum_alae_amtNegative", "'" + reader[22] + "'");
            //                            stringSQL = stringSQL.Replace("@sum_alae_amtPositive", "'" + reader[23] + "'");
            //                            stringSQL = stringSQL.Replace("@sum_loss_amtNegative", "'" + reader[24] + "'");
            //                            stringSQL = stringSQL.Replace("@sum_loss_amtPositive", "'" + reader[25] + "'");
            //                            stringSQL = stringSQL.Replace("@diff_day", "'" + Convert.ToString(reader[26]) + "'");
            //                            stringSQL = stringSQL.Replace("@diff_date", "'" + Convert.ToString(Convert.ToString(reader[2]) + " - " + Convert.ToString((DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")))) + "'");
            //                            stringSQL = stringSQL.Replace("@map_date", "'" + Convert.ToString(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "'");
            //                            stringSQL = stringSQL.Replace("@sync_status", "0");
            //                            stringSQL = stringSQL.Replace("@log_datetime_id", "'" + Convert.ToString(reader[27]) + "'");
            //                            stringSQL = stringSQL.Replace("@claim_task", "'" + Convert.ToString(reader[28]) + "'");
            //                            stringSQL = stringSQL.Replace("@handler_id", "'" + Convert.ToString(reader[29]) + "'");
            //                            stringSQL = stringSQL.Replace("@touch_user_id", "'" + Convert.ToString(reader[30]) + "'");
            //                            stringSQL = stringSQL.Replace("@touch_claim_name", "'" + Convert.ToString(reader[31]) + "'");
            //                            stringSQL = stringSQL.Replace("@sync_status", "'" + Convert.ToString("0") + "'");

            //                            WriteLog(stringSQL);
            //                            Boolean status = SetUserSWtoEmployeeDiDo(_StrCnx_2, Convert.ToString(reader[0]), Convert.ToString(reader[3]));

            //                            Console.WriteLine(">>>Map-Employee " + "--- Map(s)-Employee on Time: [" + DateTime.Now.ToString("HH:mm:ss") + "]");
            //                            connUpd.Close();
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ex.ToString();
            //                    }
            //                    iMap++;
            //                }
            //                Console.WriteLine(">>>Map-Success " + iMap + " Map(s) on Time: [" + DateTime.Now.ToString("HH:mm:ss") + "]");
            //            }
            //            else
            //            {
            //                Console.WriteLine("<<<Map Error Pending at [" + DateTime.Now.ToString("HH:mm:ss") + "]");
            //            }
            //        }
            //        conn.Close();
            //    }
            //}
        }



        //private static Boolean SetUserSWtoEmployeeDiDo(string _StrCnx_2, string user_id, string full_name)
        //{
        //    try
        //    {
        //        Boolean ban = false;
        //        using (SqlConnection conD = new SqlConnection())
        //        {
        //            conD.ConnectionString = _StrCnx_2;
        //            conD.Open();
        //            SqlCommand commandD = new SqlCommand("select * from FdETLMapping where FHUserId = '" + user_id + "'", conD);

        //            using (SqlDataReader readerD = commandD.ExecuteReader())
        //            {
        //                if (!readerD.HasRows)
        //                {
        //                    ban = true;
        //                }
        //            }
        //            conD.Close();
        //        }

        //        if (ban)
        //        {
        //            using (SqlConnection conD = new SqlConnection())
        //            {
        //                conD.ConnectionString = _StrCnx_2;
        //                conD.Open();

        //                SqlCommand commandInsert = new SqlCommand("INSERT INTO FdETLMapping (" +
        //                                                                                    "FHUserId," +
        //                                                                                    "FHName," +
        //                                                                                    "RHId)" +
        //                                                                        "VALUES(" +
        //                                                                                    "@FHUserId," +
        //                                                                                    "@FHName," +
        //                                                                                    "@RHId)", conD);
        //                commandInsert.Parameters.AddWithValue("@FHUserId", user_id);
        //                commandInsert.Parameters.AddWithValue("@FHName", full_name);
        //                commandInsert.Parameters.AddWithValue("@RHId", 0);
        //                commandInsert.ExecuteNonQuery();
        //                conD.Close();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //        ex.ToString();
        //    }
        //    return true;
        //}

        //public static void WriteLog(string strLog)
        //{
        //    string logFilePath = @"..\..\..\Log\Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
        //    FileInfo logFileInfo = new FileInfo(logFilePath);
        //    DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
        //    if (!logDirInfo.Exists) logDirInfo.Create();
        //    using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
        //    {
        //        using (StreamWriter log = new StreamWriter(fileStream))
        //        {
        //            log.WriteLine("////\n" + strLog);
        //        }
        //    }
        //}
    }
}
