﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading;


namespace RemindersConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string _Subject = "";
            string _Body = "";
            string _StrCnx = "Data Source=192.168.2.54\\MSSQLSERVER2012;Initial Catalog=FdiaryToDo;User ID=HumanResources;Password=sa123";

            DataTable dtDepts = GetAllDepartments(_StrCnx);

            while (true)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = _StrCnx;
                    conn.Open();
                    SqlCommand command = new SqlCommand("" +
                        " select RemId, FdSubject, CatName, FdIdDept, GenName, Email, RemDate, RemIdFD, RemId_usr" +
                        " from FdReminders, Fdiary, FdCategory, FdGender, EmployeeEmails " +
                        " where RemStatus = 1 " +
                        " and RemIdFD = FdId" +
                        " and FdIdCategory = CatId" +
                        " and GenId = FdIdGender" +
                        " and EmployeeID = RemId_usr" +
                        " and RemDate <= getdate()" +
                        " order by RemDate asc", conn);

                    // Add the parameters.
                    //command.Parameters.Add(new SqlParameter("0", 1));

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            int iSend = 0;
                            while (reader.Read())
                            {
                                _Subject = "";
                                _Body = "";

                                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1)
                                {
                                    Credentials = new WebCredentials("notification@gcib.net", "123%Abcd"),
                                    UseDefaultCredentials = false
                                };
                                service.AutodiscoverUrl("notification@gcib.net", RedirectionUrlValidationCallback);


                                EmailMessage email = new EmailMessage(service);

                                //email.ToRecipients.Add("mperez@gcib.net");
                                email.ToRecipients.Add(reader[5].ToString().Trim());
                                
                                //_Subject = "Test 01 ";
                                _Subject = "Reminder Notification Acknowledgment [id "+ reader[0]  + "]";
                                _Body = @"<html>
                                            <body>
                                                <h3><strong> Reminder Subject:</strong ></h3>
                                                <p>" + HtmlToPlainText(reader[1].ToString()) + @"</p>
                                                <h3><strong> General information:<br/></strong></h3>
                                                <table border='1' cellspacing='1' cellpadding='1' style='border:1px solid #0000FF; border-collapse: collapse;'>
                                                    <tbody>
                                                        <tr>
                                                            <th style = 'border-color: #0000ff; text-align:center;'>&nbsp;&nbsp;Id&nbsp;&nbsp;</th>
                                                            <th style = 'border-color: #0000ff; text-align:center;'>&nbsp;&nbsp;Category&nbsp;&nbsp;</th>
                                                            <th style = 'border-color: #0000ff; text-align:center;'>&nbsp;&nbsp;Department(s)&nbsp;&nbsp;</th>
                                                            <th style = 'border-color: #0000ff; text-align:center;'>&nbsp;&nbsp;Gender&nbsp;&nbsp;</th>
                                                        </tr>
                                                        <tr>
                                                            <td style='border-color:#0000ff; text-align:left;'>" + reader[7] + @"</td>
                                                            <td style='border-color:#0000ff; text-align:left;'>" + reader[2] + @"</td>";
                                if (reader[3].ToString().Equals("-"))
                                {
                                                _Body += @"<td style='border-color:#0000ff; text-align:left;'> - </td>";
                                }
                                else
                                {
                                                _Body += @"<td style='border-color:#0000ff; text-align:left;'>" + GetDepartm(reader[3].ToString(), dtDepts) + @"</td>";
                                }
                                _Body +=                @"<td style='border-color:#0000ff; text-align:left;'>" + reader[4] + @"</td>                                                            
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <h3><strong> Scheduled Date / Time for this reminder Reminder:  <span style='color: #ff0000;'>" + reader[6] + @"</span></strong></h3>
                                                
                                                <p> To review Events, Tasks and Reminders, please access the following Link:
                                                <a title = 'Di Do' href = 'http://192.168.30.4:3003/ToDo.aspx' target = '_blank' rel = 'noopener'> http://192.168.30.4:3003/ToDo.aspx </a></p>
                                              </body>
                                            </html>";
                                email.Subject = _Subject;
                                email.Body = _Body;
                                try
                                {
                                    email.Send();

                                    using (SqlConnection connUpd = new SqlConnection())
                                    {
                                        connUpd.ConnectionString = _StrCnx;
                                        connUpd.Open();
                                        SqlCommand commandUpd = new SqlCommand(" update FdReminders set RemStatus = 2, RemUpdate = getdate() where RemId = @id", connUpd);
                                        commandUpd.Parameters.AddWithValue("@id", reader[0]);
                                        commandUpd.ExecuteNonQuery();
                                        connUpd.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ex.ToString();
                                }
                                iSend++;
                            }
                            Console.WriteLine(">>>Send "+ iSend + " Mail(s) on Time: [" + DateTime.Now.ToString("HH:mm:ss") + "]");
                        }
                        else
                        {
                            Console.WriteLine("<<<NO Reminders Pending at [" + DateTime.Now.ToString("HH:mm:ss") + "]");
                        }

                    }
                    conn.Close();
                }
                Thread.Sleep(60000);
            }
        }

        private static DataTable GetAllDepartments(string _StrCnx)
        {
            var dt = new DataTable("Depatments");
            try
            {
                
                dt.Columns.Add("id");
                dt.Columns.Add("Name");

                using (SqlConnection conD = new SqlConnection())
                {
                    conD.ConnectionString = _StrCnx;
                    conD.Open();
                    SqlCommand commandD = new SqlCommand(" select DepartmentID,DepartmentName from Departments ", conD);
                    using (SqlDataReader readerD = commandD.ExecuteReader())
                    {
                        if (readerD.HasRows)
                        {
                            while (readerD.Read())
                            {
                                var row = dt.NewRow();
                                row["id"] = readerD[0].ToString();
                                row["name"] = readerD[1].ToString();
                                dt.Rows.Add(row);
                            }
                        }
                    }
                    conD.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ex.ToString();
            }
            return dt;
        }

        private static string GetDepartm(string v, DataTable dtDepts)
        {
            string r = "";
            try
            {
                string[] arrSelected = v.Split(',');
                if (arrSelected.Length > 0)
                {
                    foreach (string ssel in arrSelected)
                    {
                        if (!ssel.Equals(""))
                        {
                            if (dtDepts.Rows.Count > 0)
                            {
                                foreach (DataRow row in dtDepts.Rows)
                                {
                                    if (ssel == row.Field<string>(0))
                                    {
                                        r += row.Field<string>(1) + ',';
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Console.WriteLine(ex.ToString());
                r = "-";
            }
            return r;
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
    }
}
