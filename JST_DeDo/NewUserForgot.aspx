﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewUserForgot.aspx.cs" Inherits="JST_DeDo.NewUserForgot" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="Scripts/Bs4/bootstrap.min.js"></script>
    <script type="text/javascript" src="Scripts/Bs4/jquery.min.js"></script>
    <link href="Styles/Bs4/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container col-md-12 order-md-1">
            <h4 class="mb-3">Recover or Gnerate new Access</h4>
            <div class="row">
                <div id="divEmployee" runat="server" class="col-md-3 mb-3">
                    <label for="ddFdIdusuarioFilter">Select Employee: </label>
                    <br />
                    <asp:DropDownList ID="ddFdIdusuarioFilter" runat="server" AutoPostBack="false" CssClass="form-control">
                        <asp:ListItem Text="Select Employee" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input name="" class="form-control" placeholder="Email" type="email" id="EmailMail" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 mb-3">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-md-3 mb-3">
                    <p class="text-center"><a href="Login.aspx" class="btn">Back to Login.</a></p>
                </div>
                <div class="col-md-3 mb-3">
                    <asp:Button ID="BtnSendMail" runat="server" Text="Send Password by Email" OnClick="BtnSendMail_Click" class="btn btn-primary btn-xs" Enabled="true" Visible="true" />
                </div>
                <div class="col-md-3 mb-3"></div>
                <div class="col-md-3 mb-3"></div>
            </div>
            <hr class="mb-4" />
            <div class="alert alert-danger" id="divErr" runat="server" visible="false">
                <strong>
                    <asp:Label runat="server" ID="LblMsjErr" Text=""></asp:Label>
                </strong>
            </div>
            <div class="alert alert-success" id="divSuccess" runat="server" visible="false">
                <strong>
                    <asp:Label runat="server" ID="LblSucces" Text=""></asp:Label>
                </strong>
            </div>
        </div>
    </form>
</body>
</html>