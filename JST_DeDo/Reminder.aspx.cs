﻿using System;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace JST_DeDo
{
    public partial class Reminder : Page
    {
        DataTable dtRem = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            //ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    SetCurrentTimeToText();
                    SetDatesToTextBox();
                }
            }
        }

        private void SetDatesToTextBox()
        {
            try
            {
                string strBd = Request.QueryString["bd"] == null ? "" : Request.QueryString["bd"].ToString();
                string strEd = Request.QueryString["ed"] == null ? "" : Request.QueryString["ed"].ToString();
                string strFu = Request.QueryString["fu"] == null ? "" : Request.QueryString["fu"].ToString();
                DateTime EndD = DateTime.Today;
                DateTime FollD = DateTime.Today;
                TimeSpan tsStar = new TimeSpan(8, 0, 0);

                if (!strBd.Equals(""))
                {
                    TxtRBD.Text = String.Format("{0:MM/dd/yyyy hh:mm tt}", strBd);
                }

                if (!strEd.Equals(""))
                {
                    TxtRED.Text = string.Format("{0:MM/dd/yyyy hh:mm tt}", strEd);
                }
                else
                {
                    EndD = DateTime.Today.AddYears(4);
                    EndD = EndD.Add(tsStar);
                    TxtRED.Text = EndD.ToString("MM/dd/yyyy hh: mm tt");
                }

                if (!strFu.Equals(""))
                {
                    TxtRFU.Text = string.Format("{0:MM/dd/yyyy hh:mm tt}", strFu);
                }
                else
                {
                    FollD = DateTime.Today.AddYears(4);
                    FollD = FollD.Add(tsStar);
                    TxtRFU.Text = FollD.ToString("MM/dd/yyyy hh: mm tt");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private void SetCurrentTimeToText()
        {
            HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlCD.FindControl("txtCD");
            txtFdReqDate.Value = DateTime.Now.ToLocalTime().ToString("MM/dd/yyyy hh: mm tt");
        }

        protected void BtnSaveReminders_Click(object sender, EventArgs e)
        {
            try
            {
                int valor = 0;
                string opc = "";
                DateTime dtDate = DateTime.Today;

                dtDate = DateTime.Parse(TxtRBD.Text);
                try { valor = int.Parse(TxtBDNumber1.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddBD1.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtBDNumber1.Value = "";
                }

                try{valor = int.Parse(TxtBDNumber2.Value);}catch (Exception){valor = 0;}
                if (valor > 0)
                {
                    opc = ddBD2.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtBDNumber2.Value = "";
                }
                try { valor = int.Parse(TxtBDNumber3.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddBD3.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtBDNumber3.Value = "";
                }

                try { valor = int.Parse(TxtBDNumber4.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddBD4.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtBDNumber4.Value = "";
                }
                try { valor = int.Parse(TxtBDNumber5.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddBD5.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtBDNumber5.Value = "";
                }




                dtDate = DateTime.Parse(TxtRED.Text);
                try { valor = int.Parse(TxtEDNumber1.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddED1.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtEDNumber1.Value = "";
                }

                try { valor = int.Parse(TxtEDNumber2.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddED2.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtEDNumber2.Value = "";
                }
                try { valor = int.Parse(TxtEDNumber3.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddED3.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtEDNumber3.Value = "";
                }

                try { valor = int.Parse(TxtEDNumber4.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddED4.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtEDNumber4.Value = "";
                }
                try { valor = int.Parse(TxtEDNumber5.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddED5.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtEDNumber5.Value = "";
                }


                dtDate = DateTime.Parse(TxtRFU.Text);
                try { valor = int.Parse(TxtFUNumber1.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddFU1.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtFUNumber1.Value = "";
                }

                try { valor = int.Parse(TxtFUNumber2.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddFU2.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtFUNumber2.Value = "";
                }
                try { valor = int.Parse(TxtFUNumber3.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddFU3.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtFUNumber3.Value = "";
                }

                try { valor = int.Parse(TxtFUNumber4.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddFU4.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtFUNumber4.Value = "";
                }
                try { valor = int.Parse(TxtFUNumber5.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddFU5.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtFUNumber5.Value = "";
                }





                dtDate = DateTime.Parse(txtCD.Value);
                try { valor = int.Parse(TxtCDNumber1.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddCD1.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtCDNumber1.Value = "";
                }

                try { valor = int.Parse(TxtCDNumber2.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddCD2.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtCDNumber2.Value = "";
                }
                try { valor = int.Parse(TxtCDNumber3.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddCD3.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtCDNumber3.Value = "";
                }

                try { valor = int.Parse(TxtCDNumber4.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddCD4.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtCDNumber4.Value = "";
                }
                try { valor = int.Parse(TxtCDNumber5.Value); } catch (Exception) { valor = 0; }
                if (valor > 0)
                {
                    opc = ddCD5.SelectedValue;
                    AddReminder(int.Parse(opc), valor, RadBDBefore.Checked, dtDate);
                    TxtCDNumber5.Value = "";
                }

                if (dtRem.Rows.Count > 0)
                {
                    Session["SessReminders"] = dtRem;
                }
                else
                {
                    Session.Remove("SessReminders");
                }
                try
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseThisReminders", "<script language='javascript'>window.parent.$('#BtnReminders').click();</script>");
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    throw;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private void AddReminder(int opc, int valor, bool @checked, DateTime dtDate)
        {
            try
            {
                switch (opc)
                {
                    case 1:
                        if (@checked)
                        {
                            dtDate = dtDate.AddMinutes(-valor);
                        }
                        else
                        {
                            dtDate = dtDate.AddMinutes(valor);
                        }
                        break;
                    case 2:
                        if (@checked)
                        {
                            dtDate = dtDate.AddHours(-valor);
                        }
                        else
                        {
                            dtDate = dtDate.AddHours(valor);
                        }
                        break;
                    case 3:
                        if (@checked)
                        {
                            dtDate = dtDate.AddDays(-valor);
                        }
                        else
                        {
                            dtDate = dtDate.AddDays(valor);
                        }
                        break;
                    case 4:
                        valor += valor * 6;
                        if (@checked)
                        {
                            dtDate = dtDate.AddDays(-valor);
                        }
                        else
                        {
                            dtDate = dtDate.AddDays(valor);
                        }
                        break;
                    default:
                        break;
                }

                if (dtRem.Columns.Count > 0)
                {
                    dtRem.Rows.Add(dtDate);
                }
                else
                {
                    dtRem.Columns.Add("Start", typeof(DateTime));
                    dtRem.Rows.Add(dtDate);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

        }

        protected void BtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseThisReminders", "<script language='javascript'>window.parent.$('#BtnReminders').click();</script>");
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }
    }
}