﻿using JST_DeDo.Models;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JST_DeDo
{
    public partial class Test : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                GetNotifications();
            }
        }

        private void GetNotifications()
        {
            try
            {
                string t = "";
                int idUsr = Request.QueryString["usr"] == null ? 0 : Convert.ToInt32(Request.QueryString["usr"].ToString());
                using (FdiaryEntities de = new FdiaryEntities())
                {
                    int usr = Convert.ToInt32(idUsr);
                    var rem = de.FdReminders.Where(r => r.RemId_usr == idUsr).OrderByDescending(r => r.RemDate).ToList();
                    if (rem.Count > 0)
                    {
                        lblJavaScript.Text += "<script type='text/javascript'>";
                        foreach (var r in rem)
                        {
                            // t = "Reminder :";
                            // t += "<br>";
                            t = Server.HtmlDecode(r.RemTitle);
                            t += "<br>";
                            t += " scheduled on:  ";
                            //t += "<br>";
                            try
                            {
                                t += string.Format("{0:MM/dd/yyyy hh:mm tt}", r.RemDate);
                            }
                            catch (Exception)
                            {
                                t +=  " --- ";
                            }
                            lblJavaScript.Text += "fxReminderScreen('" + t + "');";
                            
                        }
                        lblJavaScript.Text += "</script>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }
    }
}