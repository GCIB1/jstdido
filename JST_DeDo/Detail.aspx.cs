﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JST_DeDo
{
    public partial class Detail : System.Web.UI.Page
    {
        FdiaryEntities db = new FdiaryEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            int idUsr = Request.QueryString["id"] == null ? 0 : Convert.ToInt32(Request.QueryString["id"].ToString());
            int id = Request.QueryString["e"] == null ? 0 : Convert.ToInt32(Request.QueryString["e"].ToString());
            if (!this.IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    PopulateHeaders(id);
                    PopulateFDiaries(id);
                }
            }
        }

        private void PopulateFDiaries(int detail)
        {
            try
            {
                if (detail > 0)
                {
                    List<Event> ev = new List<Event>();
                    var frecuency = db.FdFrequencies.ToList();
                    ev = db.Events.Where(e => e.idFd == detail).OrderBy(e => e.event_start).ToList();
                    if (ev.Count > 0)
                    {
                        gDetail.DataSource = ev.ToList();
                        gDetail.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private void PopulateHeaders(int detail)
        {
            try
            {
                if (detail > 0)
                {
                    DateTime dtReq, dtFin, dtFoll = DateTime.Today;
                    var category = db.FdCategories.ToList();
                    var deptos = db.Departments.ToList();
                    var employees = db.Employees.ToList();
                    var gender = db.FdGenders.ToList();
                    var status = db.FdStatus.ToList();
                    var frecuency = db.FdFrequencies.ToList();
                    var timeZ = db.FdTimeZones.ToList();

                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        int d = Convert.ToInt32(detail);
                        var v = de.Fdiaries.Where(a => a.FdId == d).FirstOrDefault();
                        if (v != null)
                        {
                            foreach (var cat in category)
                            {
                                if (v.FdIdCategory == cat.CatId)
                                {
                                    TxtCat.Text = cat.CatName;
                                    break;
                                }
                            }

                            if (!v.FdIdDept.Equals(""))
                            {
                                TxtDep.Text =  DeptostoString(v.FdIdDept, deptos);
                            }
                            if (!v.FdIdusuario.Equals(""))
                            {
                                TxtEmp.Text = EmployeetoString(v.FdIdusuario, employees);
                            }

                            foreach (var gen in gender)
                            {
                                if (v.FdIdGender == gen.GenId)
                                {
                                    TxtGen.Text = gen.GenName;
                                    break;
                                }
                            }

                            dtReq = DateTime.Parse(v.FdReqDate.ToString());
                            TxtReq.Value = dtReq.ToString("MM/dd/yyyy hh:mm tt");

                            dtFin = DateTime.Parse(v.FdFinishDate.ToString());
                            TxtFin.Value = dtFin.ToString("MM/dd/yyyy hh:mm tt");

                            dtFoll = DateTime.Parse(v.FdFollow.ToString());
                            TxtFol.Value = dtFoll.ToString("MM/dd/yyyy hh:mm tt");

                            foreach (var st in status)
                            {
                                if (v.FdStatus == st.StatusId)
                                {
                                    TxtSta.Text = st.StatusName;
                                    break;
                                }
                            }

                            foreach (var fre in frecuency)
                            {
                                if (v.FdIdFrequency == fre.FId)
                                {
                                    TxtFre.Text = fre.FFrequency;
                                    break;
                                }
                            }

                            foreach (var tz in timeZ)
                            {
                                if (v.FdIdTz == tz.TzId)
                                {
                                    TxtTim.Text = tz.TzName;
                                    break;
                                }
                            }
                            spnSub.InnerHtml = Server.HtmlDecode(v.FdSubject);
                            //spnSub.InnerHtml = "'" + v.FdSubject.ToString() + "'";
                            //TxtSub.Text = v.FdSubject == null ? "" : v.FdSubject.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private void BindEStatus(DropDownList ddS, List<EStatu> status)
        {
            try
            {
                ddS.Items.Clear();
                ddS.AppendDataBoundItems = true;
                ddS.DataTextField = "EStatusName";
                ddS.DataValueField = "EStatId";
                ddS.DataSource = status;
                ddS.DataBind();
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private List<EStatu> PopulateStatus()
        {
            using (FdiaryEntities fd = new FdiaryEntities())
            {
                return fd.EStatus.Where(s => s.EStatusActivo == 1).OrderBy(s => s.EStatusOrder).ToList();
            }
        }

        protected void GDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int id = Request.QueryString["e"] == null ? 0 : Convert.ToInt32(Request.QueryString["e"].ToString());
            gDetail.PageIndex = e.NewPageIndex;
            PopulateFDiaries(id);
        }

        protected void GDetail_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void GDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddS = (e.Row.FindControl("ddEStatus") as DropDownList);
                    string sStat = (e.Row.FindControl("lblStatus") as Label).Text;
                    BindEStatus(ddS, PopulateStatus());
                    ddS.Items.FindByValue(sStat).Selected = true;

                    //1   Open
                    //2   Done
                    //3   Close
                    //4   Deleted

                    if (sStat.Equals("2"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Orange;
                        e.Row.ForeColor = System.Drawing.Color.White;
                        e.Row.Font.Strikeout = true;
                    }
                    if (sStat.Equals("3"))
                    {
                        e.Row.Font.Strikeout = true;
                    }
                    // Urgent=marron, 
                    if (sStat.Equals("5"))
                    {
                        e.Row.BackColor = System.Drawing.Color.Maroon;
                        e.Row.ForeColor = System.Drawing.Color.White;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        protected void DdFdStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList dds = (DropDownList)sender;
                GridViewRow row = (GridViewRow)dds.NamingContainer;

                int r = row.RowIndex;

                int idD = Convert.ToInt32(gDetail.DataKeys[row.RowIndex].Value);

                if (idD > 0)
                {
                    DateTime dtupd= DateTime.Today;
                    TimeSpan tsStar = DateTime.Now.TimeOfDay;

                    dtupd = dtupd.Add(tsStar);
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        var ev = de.Events.Where(evt => evt.event_id == idD).FirstOrDefault();
                        if (ev != null)
                        {
                            ev.idStatus = Convert.ToInt32(dds.SelectedValue);
                            ev.event_upd = dtupd;
                        }
                        de.SaveChanges();
                        int id = Request.QueryString["e"] == null ? 0 : Convert.ToInt32(Request.QueryString["e"].ToString());
                        PopulateFDiaries(id);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private String DeptostoString(String str, List<Department> deptos)
        {
            string r = "";
            try
            {
                string[] arrDept = str.Split(',');
                if (arrDept.Length > 0)
                {
                    foreach (string pos in arrDept)
                    {
                        if (!pos.Equals(""))
                        {
                            foreach (var dep in deptos)
                            {
                                if (Convert.ToInt32(pos) == dep.DepartmentID)
                                {
                                    r += dep.DepartmentName + ',';
                                    break;
                                }
                            }
                        }
                    }
                    if (r.Length > 0)
                    {
                        r = r.TrimEnd(',');
                    }
                }
            }
            catch (Exception)
            {
                r = "";
                throw;
            }
            return r;
        }

        private String EmployeetoString(String str, List<Employee> employees)
        {
            string r = "";
            try
            {
                string[] arrEmp = str.Split(',');
                if (arrEmp.Length > 0)
                {
                    foreach (string pos in arrEmp)
                    {
                        if (!pos.Equals(""))
                        {
                            foreach (var empl in employees)
                            {
                                if (Convert.ToInt32(pos) == empl.EmployeeID)
                                {
                                    r += empl.EmployeeName + ',';
                                    break;
                                }
                            }
                        }
                    }
                    if (r.Length > 0)
                    {
                        r = r.TrimEnd(',');
                    }
                }
            }
            catch (Exception)
            {
                r = "";
                throw;
            }
            return r;
        }
    }
}