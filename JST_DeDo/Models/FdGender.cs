//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JST_DeDo.Models
{
    using System;
    using System.Collections.Generic;
    [Serializable]
    public partial class FdGender
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FdGender()
        {
            this.Fdiaries = new HashSet<Fdiary>();
        }
    
        public int GenId { get; set; }
        public string GenName { get; set; }
        public Nullable<int> GenOrder { get; set; }
        public Nullable<int> GenStatus { get; set; }
        public Nullable<System.DateTime> GenCreate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fdiary> Fdiaries { get; set; }
    }
}
