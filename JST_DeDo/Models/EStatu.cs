//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JST_DeDo.Models
{
    using System;
    using System.Collections.Generic;
    [Serializable]
    public partial class EStatu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EStatu()
        {
            this.Events = new HashSet<Event>();
        }
    
        public int EStatId { get; set; }
        public string EStatusName { get; set; }
        public Nullable<int> EStatusActivo { get; set; }
        public Nullable<int> EStatusOrder { get; set; }
        public Nullable<System.DateTime> EStatusCreate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }
    }
}
