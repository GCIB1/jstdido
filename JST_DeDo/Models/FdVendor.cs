//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JST_DeDo.Models
{
    using System;
    using System.Collections.Generic;
    [Serializable]
    public partial class FdVendor
    {
        public int VndId { get; set; }
        public string VndName { get; set; }
        public Nullable<int> VndOrder { get; set; }
        public int VndStatus { get; set; }
        public Nullable<System.DateTime> VndCreate { get; set; }
    }
}
