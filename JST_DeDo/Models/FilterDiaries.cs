﻿namespace JST_DeDo
{
    internal class FilterDiaries
    {
        public FilterDiaries()
        {

        }

        public int IdStatus { get; set; }
        public string DetalsRem { get; set; }
        public int Id { get; set; }
        public string Category { get; set; }
        public string Department { get; set; }
        public string Gender { get; set; }
        public string Employee { get; set; }
        public string TimeZone { get; set; }
        public string Frequency { get; set; }
        public string Begin_Date { get; set; }
        public string End_Date { get; set; }
        public string Follow_UpReview { get; set; }
        public string Subject { get; set; }
        public string Status { get; set; }
    }
}