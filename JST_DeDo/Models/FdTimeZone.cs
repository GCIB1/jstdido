//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JST_DeDo.Models
{
    using System;
    using System.Collections.Generic;
    [Serializable]
    public partial class FdTimeZone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FdTimeZone()
        {
            this.Fdiaries = new HashSet<Fdiary>();
        }
    
        public int TzId { get; set; }
        public string TzName { get; set; }
        public string TzFullName { get; set; }
        public Nullable<int> TzActive { get; set; }
        public Nullable<int> TzOrder { get; set; }
        public Nullable<System.DateTime> TzCreate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fdiary> Fdiaries { get; set; }
    }
}
