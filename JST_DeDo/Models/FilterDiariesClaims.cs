﻿namespace JST_DeDo
{
    internal class FilterDiariesClaims
    {
        public FilterDiariesClaims()
        {

        }

        public int IdStatus { get; set; }
        public string DetalsRem { get; set; }
        public int Id { get; set; }
        public string Category { get; set; }
        public string Department { get; set; }
        public string Gender { get; set; }
        public string Employee { get; set; }
        public string TimeZone { get; set; }
        public int Recived { get; set; }
        public int Closed { get; set; }
        public int Pending_Days { get; set; }
        public int Pending30_Days { get; set; }
        public int Pending60_Days { get; set; }
        public int Pending90_Day { get; set; }

        public int PhoneCallsIn { get; set; }
        public int PhoneCallsOut { get; set; }
        public int EmailsIn { get; set; }
        public int EmailsOut { get; set; }

        public int LossRuns { get; set; }
        public int FNOL { get; set; }
        public int Clerance { get; set; }

        public string LossRun_Clearance { get; set; }
        public string Frequency { get; set; }
        public string Begin_Date { get; set; }
        public string End_Date { get; set; }
        public string Follow_UpReview { get; set; }
        public string Subject { get; set; }
        public string Status { get; set; }
    }
}