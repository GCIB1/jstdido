﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="JST_DeDo.Recurrency" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Frecuency</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="Scripts/jquery-3.2.1.js"></script>
    <script src="Scripts/bootstrap-multiselect.js"></script>
    <script src="Scripts/Bs3/bootstrap.min.js"></script>
    <script src="Scripts/JSTDeDo.js"></script>
    <script src="Scripts/autosize.js"></script>
    <script src="Scripts/Bs3/bootstrap-datetimepicker.js"></script>

    <link href="Styles/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/bootstrap-multiselect.css" rel="stylesheet" />
    <link href="Styles/fdiaryGrid.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script>    
        function doDetailClaims()
        {
            if (document.getElementById('DivIDDetClaims').style.visibility == 'visible') {
                document.getElementById('DivIDDetClaims').style.visibility = 'hidden'
            } else {
                document.getElementById('DivIDDetClaims').style.visibility = 'visible';
                document.getElementById('IDetailClaims').src = 'ClaimDetails/Index';
            }
        }
        function isNumber(evt)
        {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
    <form id="frmRecurr" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <input runat="server" id="TxtClaims" type="text" value="" hidden="hidden"/>
        
        <div style="vertical-align: middle; text-align: right">
            <asp:Button ID="Button4" runat="server" Text="Close" CssClass="btn btn-warning btn-xs" OnClick="BtnClose_Click" ToolTip="Close this window" />
            &nbsp;|&nbsp;
            <asp:Button ID="Button5" Text="Save" runat="server" CssClass="btn btn-primary btn-xs" OnClick="BtnInsertGrid_Click" />
        </div>

        <div  id="DivIDDetClaims" style="position: absolute;z-index: 999;background-color: azure;width: 100%;height: 89%;left: 0%;top: 11%;visibility: hidden;">
            <iframe runat="server" id="IDetailClaims" style="border-style: none; width: 100%; height: 100%;"></iframe>
        </div>


        <asp:Panel ID="PnlClaims" runat="server" Visible="True">
            <div>
                <table class="table table-borderless" id="TblClm1">
                    <thead>
                        <tr>
                            <th style="width:9%">
                                
                            </th>
                            <th style="width:9%">
                                Received claims
                            </th>
                            <th style="width:9%">
                                Closed claims
                            </th>
                            <th style="width:9%">
                                Pending claims
                            </th>
                            <th style="width:9%">
                                Pend 30 days
                            </th>
                            <th style="width:9%">
                                Pend 60 days
                            </th>
                            <th style="width:9%">
                                Pend 90 day +
                            </th>
                            <th>
                                Loss run / Clearance
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:Button CssClass="btn btn-primary" runat="server" Text="Show/Hide Claims" ToolTip="( Show / Hide ) Assigned Claims..." OnClientClick="doDetailClaims();return false;" />
                            </td>
                            <td>
                                <input runat="server" id="TxtReceivedC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td>
                            <td>
                                <input runat="server" id="TxtClosedC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtPendingC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtPending30C" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtPending60C" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtPending90C" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <asp:TextBox ID="TxtLossRunC" runat="server"  Text="" CssClass="form-control" Visible="true" AutoPostBack="false"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>


                <table class="table table-borderless" id="TblClm2">
                    <thead>
                        <tr>
                            <th style="width:11%">
                            </th>
                            <th style="width:9%">
                                Phone calls In	
                            </th>
                            <th style="width:9%">
                                Phone calls Out	
                            </th>
                            <th style="width:9%">
                                Emails In	
                            </th>
                            <th style="width:9%">
                                Emails Out
                            </th>
                            <th style="width:9%">
                                Loss Runs
                            </th>
                            <th style="width:9%">
                                FNOL
                            </th>
                            <th style="width:9%">
                                Clerance
                            </th>
                            <th>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input runat="server" id="TxtPhoneCallsInC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtPhoneCallsOutC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtEmailsInC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)" style="text-align:right;" />
                            </td><td>
                                <input runat="server" id="TxtEmailsOutC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)"  style="text-align:right;" />
                            </td>
                            <td>
                                <input runat="server" id="TxtLRunsC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)"  style="text-align:right;" />
                            </td>
                            <td>
                                <input runat="server" id="TxtFNOLC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)"  style="text-align:right;" />
                            </td>
                            <td>
                                <input runat="server" id="TxtCleranceC" type="number" min="0" max="999" step="1" value="" class="form-control" ononkeypress="return isNumber(event)"  style="text-align:right;" />
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </asp:Panel>
        
        <div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="application" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body" style="height: 500px;">
                            <iframe runat="server" id="ifMtto" style="border-style: none; width: 100%; height: 100%;"></iframe>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal Reminders -->
            <div class="modal fade" id="NewRem" role="application" data-backdrop="static" data-keyboard="false" runat="server">
                <div class="modal-dialog modal-lg">

                    <div class="modal-content" style="height: 700px; width: 999px; left: -94px;">
                        <div class="modal-body" style="width: 100%;">
                            <iframe runat="server" id="ifReminders" style="border-style: none; width: 100%; height: 100%;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <div>
                    <div>
                        <div>

                            <table class="table table-borderless" id="TblCat">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:LinkButton ID="LinkButton" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='Category/Index';return false;" runat="server" Text="Category" />
                                        </th>
                                        <th style="width: 250px">
                                            <asp:LinkButton ID="LinkButton1" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='Department/Index';return false;" runat="server" Text="Department" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="LinkButton2" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='Gender/Index';return false;" runat="server" Text="Gender" />
                                        </th>
                                        <th style="width: 250px">
                                            <asp:LinkButton ID="LinkButton3" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='Employee/Index';return false;" runat="server" Text="Employee(s)" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div id="divCtrlCategory" runat="server">
                                                <asp:DropDownList ID="ddFdIdCategory" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Select Category" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divCtrlDepartment" runat="server" class="">
                                                <select runat="server" id="sDepartment" multiple="true" class="" size="8" style="width: 100px"></select>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divCtrlGender" runat="server" class="">
                                                <asp:DropDownList ID="ddFdIdGender" runat="server" AutoPostBack="False" CssClass="form-control">
                                                    <asp:ListItem Text="Select Department" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="container-fluid " id="divCtrlEmployee" runat="server">
                                                <select runat="server" id="sEmployees" multiple="true" size="8" class="form-contro"></select>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-borderless" id="TblDates">
                                <thead>
                                    <tr>
                                        <th>Begin Date
                                        </th>
                                        <th>End Date
                                        </th>
                                        <th>Follow Up/Review
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="LinkButton6" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='Status/Index';return false;" runat="server" Text="Status" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div id="divCtrlReqdate" class="input-append date form_datetimeB" style="width: 140px;" runat="server">
                                                <input id="txtFdReqDate" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                <span class="add-on"><i class="icon-remove"></i></span>
                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divCtrlfinish" class="input-append date form_datetimeE" style="width: 140px;" runat="server">
                                                <input id="txtFdFinishDate" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                <span class="add-on"><i class="icon-remove"></i></span>
                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divCtrlFollow" class="input-append date form_datetimeF" style="width: 140px;" data-date-format="mm/dd/yyyy" runat="server">
                                                <input id="txtFdFollow" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                <span class="add-on"><i class="icon-remove"></i></span>
                                                <span class="add-on"><i class="icon-calendar"></i></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divCtrlStatus" runat="server" class="">
                                                <asp:DropDownList ID="ddFdStatus" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="Select Status" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-borderless" id="TblFrec">
                                <thead>
                                    <tr>
                                        <th>
                                            <asp:LinkButton ID="LinkButton4" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='Frequency/Index';return false;" runat="server" Text="Frequency" />

                                        </th>
                                        <th></th>
                                        <th>Reminder(s)
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="LinkButton5" data-toggle="modal" data-target="#myModal" OnClientClick="document.getElementById('ifMtto').src='TimeZone/Index';return false;" runat="server" Text="Time Zone" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div id="divCtrlFrequency" runat="server">
                                                <asp:DropDownList ID="ddFdIdFrequency" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="DdFdIdFrequency_TextChanged">
                                                    <asp:ListItem Text="Select Frequency" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div1" runat="server">
                                            </div>
                                        </td>
                                        <td>

                                            <div id="divCtrlReminder" runat="server" class="container-fluid">
                                                <button type="button" class="btn btn-primary btn-xs"
                                                    data-toggle="modal" data-target="#NewRem"
                                                    onclick="StarReminders();"
                                                    title="Click to Add a Reminder(s)">
                                                    Reminders
                                                    <span class="badge" id="spnReminders">
                                                        <asp:Label ID="LblRem" runat="server" Text="0"></asp:Label>
                                                    </span>
                                                </button>
                                            </div>

                                        </td>
                                        <td>
                                            <div id="divCtrlTimeZone" runat="server" class="container-fluid">
                                                <asp:DropDownList ID="ddFdIdTimeZone" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Select Time Zone" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <div id="divDOW" class="collapse" style="width: 98%;" runat="server">
                                                <p>Select at least one day:</p>
                                                <div>
                                                    <asp:CheckBox ID="ChkSunday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Sunday" />
                                                    <asp:CheckBox ID="ChkMonday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Monday" />
                                                    <asp:CheckBox ID="ChkTuesday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Tuesday" />
                                                    <asp:CheckBox ID="ChkWednesday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Wednesday" />
                                                    <asp:CheckBox ID="ChkThursday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Thursday" />
                                                    <asp:CheckBox ID="ChkFriday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Friday" />
                                                    <asp:CheckBox ID="ChkSaturday" OnCheckedChanged="DdFdIdFrequency_TextChanged" CssClass="btn btn-primary btn-xs" Checked="false" runat="server" AutoPostBack="true" Text="Saturday" />
                                                </div>
                                            </div>

                                            <div id="divQrts" class="collapse" style="width: 50%; background-color: #D9EDF7;" runat="server">
                                                <table class="table table-borderless" id="TblQrts" style="background-color: #D9EDF7;">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <div id="div1HQrts" class="collapse" runat="server">
                                                                    <p>Please enter below the date for the selected quarter:</p>
                                                                    <p>1st Quarter</p>
                                                                </div>
                                                            </th>
                                                            <th>
                                                                <div id="div2HQrts" class="collapse" runat="server">
                                                                    <p>Please enter below the date for the selected quarter:</p>
                                                                    <p>2nd Quarter</p>
                                                                </div>
                                                            </th>
                                                            <th>
                                                                <div id="div3HQrts" class="collapse" runat="server">
                                                                    <p>Please enter below the date for the selected quarter:</p>
                                                                    <p>3rd Quarter</p>
                                                                </div>
                                                            </th>
                                                            <th>
                                                                <div id="div4HQrts" class="collapse" runat="server">
                                                                    <p>Please enter below the date for the selected quarter:</p>
                                                                    <p>4th Quarter</p>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div id="div1Qrts" class="collapse" style="width: 98%;" runat="server">
                                                                    <div>
                                                                        <div id="div4" class="input-append date form_datetime1Q" style="width: 140px;" runat="server">
                                                                            <input id="Txt1Qrts" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                                            <span class="add-on"><i class="icon-remove"></i></span>
                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div id="div2Qrts" class="collapse" style="width: 98%;" runat="server">
                                                                    <div>
                                                                        <div id="div5" class="input-append date form_datetime2Q" style="width: 140px;" runat="server">
                                                                            <input id="Txt2Qrts" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                                            <span class="add-on"><i class="icon-remove"></i></span>
                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div id="div3Qrts" class="collapse" style="width: 98%;" runat="server">
                                                                    <div>
                                                                        <div id="div7" class="input-append date form_datetime3Q" style="width: 140px;" runat="server">
                                                                            <input id="Txt3Qrts" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                                            <span class="add-on"><i class="icon-remove"></i></span>
                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div id="div4Qrts" class="collapse" style="width: 98%;" runat="server">
                                                                    <div>
                                                                        <div id="div9" class="input-append date form_datetime4Q" style="width: 140px;" runat="server">
                                                                            <input id="Txt4Qrts" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                                                            <span class="add-on"><i class="icon-remove"></i></span>
                                                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="DivMsgFrec" class="alert alert-info" visible="false" runat="server">
                                                <strong>Frequency Configuration!</strong><br />
                                                <asp:Label ID="lblConfFrec" runat="server" Text=""></asp:Label>
                                            </div>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-borderless" id="TblSubj">
                                <thead>
                                    <tr>
                                        <th>Subject
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div id="divCtrlSubj" runat="server">

                                                <asp:TextBox ID="replyBody" Width="100%" runat="server" TextMode="MultiLine"
                                                    oncopy="return false"
                                                    onpaste="return false"
                                                    oncut="return false"
                                                    ValidateRequestMode="Disabled"
                                                    Rows="12" />
                                                <asp:HtmlEditorExtender ID="replyBody_HtmlEditorExtender" runat="server" Enabled="True" TargetControlID="replyBody">
                                                    <Toolbar>
                                                        <asp:Undo />
                                                        <asp:Redo />
                                                        <asp:HorizontalSeparator />
                                                        <asp:Bold />
                                                        <asp:Italic />
                                                        <asp:Underline />
                                                        <asp:StrikeThrough />
                                                        <asp:Subscript />
                                                        <asp:Superscript />
                                                        <asp:HorizontalSeparator />
                                                        <asp:JustifyLeft />
                                                        <asp:JustifyCenter />
                                                        <asp:JustifyRight />
                                                        <asp:JustifyFull />
                                                        <asp:HorizontalSeparator />
                                                        <asp:InsertOrderedList />
                                                        <asp:InsertUnorderedList />
                                                        <asp:RemoveFormat />
                                                        <asp:BackgroundColorSelector />
                                                        <asp:ForeColorSelector />
                                                        <asp:HorizontalSeparator />
                                                    </Toolbar>


                                                </asp:HtmlEditorExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Required" ForeColor="Red" Display="Dynamic" ValidationGroup="Add" ControlToValidate="replyBody"></asp:RequiredFieldValidator>

                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div style="vertical-align: middle; text-align: right">
                                <asp:Button ID="BtnDelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClick="BtnDelete_Click" Enabled="false" />
                                &nbsp;|&nbsp;
                                <asp:Button ID="BtnCancel" runat="server" Text="Clear" CssClass="btn btn-warning btn-xs" OnClick="BtnCancel_Click" ToolTip="Clear form" />
                                <asp:Button ID="BtnClone" runat="server" Text="Clone" CssClass="btn btn-info btn-xs" Enabled="false" OnClick="BtnClone_Click" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-warning btn-xs" OnClick="BtnClose_Click" ToolTip="Close this window" />
                                &nbsp;|&nbsp;
                                <asp:Button ID="BtnInsertGrid" Text="Save" runat="server" CssClass="btn btn-primary btn-xs" OnClick="BtnInsertGrid_Click" />

                            </div>

                            <div id="divCtrlDiary" runat="server" visible="false">
                                <asp:TextBox ID="txtID" runat="server" Enabled="false" Text="" CssClass="form-control" Visible="true"></asp:TextBox>
                            </div>

                        </div>
                        <div id="DivMsg" class="alert alert-success" visible="false" runat="server">
                            <asp:Label ID="LblMsg" runat="server" Text=""></asp:Label>
                        </div>
                        <div id="DivMsgclone" class="alert alert-warning" visible="false" runat="server">
                            <asp:Label ID="LblMsgClone" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="BtnCrtlFrequency" runat="server" OnClick="DdFdIdFrequency_TextChanged" Visible="true" Text="" Width="0px" Height="0px" class="btn btn-link" />
        <asp:Button ID="BtnReminders" runat="server" OnClick="BtnReminders_Click" Visible="true" Text="" Width="0px" Height="0px" class="btn btn-link" />
    </form>
    <script>
        $(document).ready(function () {
            $('#sEmployees').multiselect();
        });

        $(document).ready(function () {
            $('#sDepartment').multiselect();
        });
    </script>
    <script type="text/javascript">
        var today, datepicker, q1S, q2S, q3S, q4S, q1E, q2E, q3E, q4E;
        today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        q1S = new Date(new Date().getFullYear(), 0, 1);
        q1E = new Date(new Date().getFullYear(), 02, 31);
        q2S = new Date(new Date().getFullYear(), 03, 1);
        q2E = new Date(new Date().getFullYear(), 05, 30);
        q3S = new Date(new Date().getFullYear(), 06, 1);
        q3E = new Date(new Date().getFullYear(), 08, 30);
        q4S = new Date(new Date().getFullYear(), 09, 1);
        q4E = new Date(new Date().getFullYear(), 11, 31);

        $(".form_datetimeB").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            todayBtn: true,
            startDate: today,
            minuteStep: 5
        });

        $(".form_datetimeE").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            todayBtn: true,
            startDate: today,
            minuteStep: 5
        });

        $(".form_datetimeF").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            todayBtn: true,
            startDate: today,
            minuteStep: 5
        });




        $(".form_datetime1Q").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            startDate: q1S,
            endDate: q1E,
            todayBtn: false,
            todayHighlight: false,
            minuteStep: 5
        }).on('changeDate', function () {
            window.$('#BtnCrtlFrequency').click();
        });

        $(".form_datetime2Q").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            startDate: q2S,
            endDate: q2E,
            todayBtn: false,
            todayHighlight: false,
            minuteStep: 5
        }).on('changeDate', function () {
            window.$('#BtnCrtlFrequency').click();
        });

        $(".form_datetime3Q").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            startDate: q3S,
            endDate: q3E,
            todayBtn: false,
            todayHighlight: false,
            minuteStep: 5
        }).on('changeDate', function () {
            window.$('#BtnCrtlFrequency').click();
        });

        $(".form_datetime4Q").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            startDate: q4S,
            endDate: q4E,
            todayBtn: false,
            todayHighlight: false,
            minuteStep: 5
        }).on('changeDate', function () {
            window.$('#BtnCrtlFrequency').click();
        });


        function StarReminders() {
            var bd = document.getElementById('txtFdReqDate').value;
            var ed = document.getElementById('txtFdFinishDate').value;
            var fu = document.getElementById('txtFdFollow').value;
            document.getElementById('ifReminders').src = 'Reminder.aspx?bd=' + bd + '&ed=' + ed + '&fu=' + fu;
            return false;
        }

    </script>
</body>
</html>
