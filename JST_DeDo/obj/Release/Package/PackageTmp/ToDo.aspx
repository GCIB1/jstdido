﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToDo.aspx.cs" Inherits="JST_DeDo.JST" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>To Do</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Modal -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- DataTable -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/> <!-- Mostrar Paginado numeros -->

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css"/> <!-- Check Boxes -->

    <link href="Styles/CheckBoxes.css" rel="stylesheet" /> <!-- Check Boxes -->

    <!-- Notificaciones -->
        <script src="Scripts/Notify/toastr.min.js"></script>
        <link href="Styles/Notify/toastr.min.css" rel="stylesheet" />
    <!-- Notificaciones -->

    <script type="text/javascript"> 
        function fxReminderScreen(strInfo) {
        toastr.success(strInfo, 'DeDo Notifications!',
        {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-left",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "3000",
            "hideDuration": "1000",
            "timeOut": "60000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            })
	    }
    </script>


    <style type="text/css" class="init">
	
	div.container {
		width: 80%;
	}
	</style>

    <script>
        function doPrint2() {
            var prtHeader = document.getElementById('<%=divTitle.ClientID %>');
            var tableReg = document.getElementById('example');
            var cellsOfRow = "";

            var body = document.getElementsByTagName("body")[0];
            var tabla = document.createElement("table");
            var tblBody = document.createElement("tbody");
            var hilera = document.createElement("tr");

            var ren = 1;
            if (tableReg.rows.length > 30) {
                ren = 2;
            }

            for (var i = 0; i < tableReg.rows.length; i++) {
                cellsOfRow = tableReg.rows[ren].getElementsByTagName('td');
            }
            for (var j = 1; j < cellsOfRow.length; j++) {
                if (j > 0) {
                    var celda = document.createElement("td");
                    var text = tableReg.rows[ren - 1].cells[j].textContent;
                    var textoCelda = document.createTextNode(text);
                    celda.appendChild(textoCelda);
                    celda.style.textAlign = 'Center';
                    celda.style.padding = '15px';
                    celda.style.fontSize = '11px';
                    celda.style.fontWeight = 'bold';
                    hilera.appendChild(celda);
                }
            }
            tblBody.appendChild(hilera);

            for (var i = ren; i < tableReg.rows.length - 1; i++) {
                var hilera = document.createElement("tr");
                for (var j = 1; j < cellsOfRow.length; j++) {
                    if (j > 0) {
                        var celda = document.createElement("td");
                        var text = tableReg.rows[i].cells[j].textContent;
                        var textoCelda = document.createTextNode(text);
                        celda.appendChild(textoCelda);
                        celda.style.border = '1px solid #ddd';
                        celda.style.textAlign = 'left';
                        celda.style.padding = '15px';
                        celda.style.borderWidth = '0 1px 1px 0 !important';
                        celda.style.fontSize = '9px';
                        hilera.appendChild(celda);
                    }
                }
                tblBody.appendChild(hilera);
            }

            tabla.appendChild(tblBody);
            body.appendChild(tabla);
            tabla.style.border = '1px solid #ddd';
            tabla.style.textAlign = 'left';
            tabla.style.borderCollapse = 'collapse';
            tabla.style.borderWidth = '0 1px 1px 0 !important';

            var WinPrint = window.open('', '', 'left=100,top=100,width=1000,height=1000,toolbar=0,scrollbars=1,status=0,resizable=1');
            WinPrint.document.write(prtHeader.outerHTML);
            WinPrint.document.write(tabla.outerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
</head>
<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">
                    <div class="col-md-12 order-md-1" id="divTitle" runat="server">&nbsp;To Do</div>
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li></li>
                <li></li>
                <li>
                    <a href="#">
                        <asp:Label runat="server" Text="" ID="LblUsr" Font-Bold="true" ForeColor="lightgreen"></asp:Label>
                        &nbsp;
                        <asp:LoginName ID="txtEmail" runat="server" Font-Bold="true" ForeColor="White" />
                    </a>
                    
                </li>
                <li>
                    
                </li>
                <li>
                    <a href="ChangePass.aspx">Change Password.
                    </a>
                </li>
                <li>
                    <a id="BtnPuente" href="javascript:__doPostBack('BtnReminders','')">
                        <div id="divCtrlReminder" runat="server" class="container-fluid">
                        <span class="badge" id="spnReminders">
                            <asp:Label ID="LblReminders" runat="server" Text="" Visible="True" ForeColor="springgreen"></asp:Label>
                        </span>
                        </div>
                    </a>
                </li>
                <li>
                    <asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="glyphicon glyphicon-log-out" />
                </li>
            </ul>
        </div>
    </nav>
    <form id="form1" runat="server">
        <asp:Label ID="lblJavaScript" runat="server" Text="" ></asp:Label>
        <input runat="server" id="TxtClaimsToDo" type="text" value="" hidden="hidden" />

        

        <!-- Modal NewReg -->
        <div class="modal fade" id="NewReg" role="application" data-backdrop="static" data-keyboard="false" runat="server">
            <div class="modal-dialog modal-lg" style="width: 1300px">
                <div class="modal-content">
                    <div class="modal-body" style="height: 855px; width: 1300px;">
                        <iframe runat="server" id="ifNewReg" style="border-style: none; width: 100%; height: 100%;"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div style="width: 98%;">
                <div class="row">
                    <div class="col-md-3 mb-3"></div>
                    <div class="col-md-3 mb-3"></div>
                    <div class="col-md-3 mb-3"></div>
                    <div class="col-md-3 mb-3">
                        <asp:Button ID="BtnReminders" runat="server" CssClass="btn-link" OnClick="BtnReminders_Click1" Text="" Width="0px" Height="1px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <%--<asp:Button ID="BtnClaims" runat="server" Text="Claims" CssClass="btn btn-warning btn-xs" ToolTip="Run Claims process" OnClick="BtnClaims_Click" Visible="False" />--%>

                        <asp:Image ID="ImgShowEmployee" runat="server" data-toggle="collapse" data-target="#divEmployeeInfo" ImageUrl="~/Img/icons8-name-tag-24.png" ToolTip="Employee data" />
                        <asp:Button ID="BtnClear" data-toggle="modal" data-target="#NewReg" runat="server" Text="Clear" ToolTip="Clear all Filters" CssClass="btn btn-primary btn-xs" OnClick="BtnClear_Click" />
                        <asp:Button ID="BtnPrint2" runat="server" CssClass="btn btn-primary btn-xs glyphicon glyphicon-print" Text="Print" OnClientClick="doPrint2();" OnClick="BtnClear_Click" />
                        <button id="minusBtn" class="btn btn-primary btn-xs glyphicon glyphicon-minus" onclick="minus();return false;" title="Zoom ( - )"></button>
                        <button id="plusBtn" class="btn btn-primary btn-xs glyphicon glyphicon-plus" onclick="plus();return false;" title="Zoom ( + )"></button>
                        <button id="btnInsertGrid" data-toggle="modal" data-target="#NewReg" class="btn btn-success btn-xs" onclick="document.getElementById('ifNewReg').src ='Create.aspx';return false;">Add New</button>
                        <button id="btnGrid" data-toggle="modal" data-target="#NewReg" onclick="" hidden="hidden">test</button>
                    </div>
                </div>
            </div>
            <div id="divEmployeeInfo" class="collapse" style="width: 98%;">
                <div class="panel-heading">
                    <asp:Image ID="Image2" runat="server" Height="80px" Width="89px" ImageUrl="~/Img/whois.png" />
                    <br />
                    <asp:Label runat="server">Jasbir Singh Thandi + Job Position</asp:Label>
                </div>
                <div class="panel-body" style="background-color: #FFCC66">
                    <div class="row">
                        <div class="col-md-9 mb-3">
                            [Ext. 112] | Work Schedule: M-F 7:00AM - 5:00PM | Lunch: 12:00PM - 1:00PM
                        </div>
                        <div class="col-md-3 mb-3">
                        </div>
                    </div>
                </div>
            </div>


            <div id="divlblResult" runat="server" class="container col-md-12 order-md-1" visible="true">
                <div class="row">
                    <div id="div1" runat="server" class="col-md-3 mb-3">
                        <asp:Label ID="lblResult" runat="server" Text="" Font-Bold="True"></asp:Label>
                    </div>
                    <div id="divButtons" runat="server" class="col-md-6 mb-3">

                    </div>
                    <div id="div3" runat="server" class="col-md-3 mb-3">
                        <asp:CheckBox ID="chkShowAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkShowAll_CheckedChanged" Text="Show deleted" TextAlign="Right" Checked="false" CssClass="form-check-input" />
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="ChkHideDone" runat="server" AutoPostBack="true" OnCheckedChanged="ChkHideDone_CheckedChanged" Text="Hide Done" TextAlign="Right" Checked="false" CssClass="form-check-input" />
                    </div>
                </div>
            </div>
            <hr />
             <table class="table table-borderless" id="TblClaim1">
                    <tbody>
                        <tr>
                            <td style="width:10%">
                            </td>
                            <td>
                                <div id="Checkers" class="container col-md-12 order-md-1" style="width: 100%;display: table-cell;vertical-align: middle;">
	                                <asp:PlaceHolder ID="PlaceHolderChekers" runat="server" />
                                </div>
                            </td>
                            <td style="width:10%">
                            </td>
                        </tr>
                    </tbody>
                </table>
            
        </div>

        <div id="NewDataTable" style="width: 98%;">
            <asp:PlaceHolder ID="PlaceHolderDiary" runat="server" />
        </div>
    </form>
</body>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "pagingType": "full_numbers"
            ,
            "iDisplayLength": 25
        });
    });

    function CheckSH(obj, col)
    {
        var table = $('#example').DataTable();
        if (document.getElementById(obj).checked)
        {
            table.column(col).visible(true);
        }
        else
        {
            table.column(col).visible(false);
        }
    }

</script>
</html>