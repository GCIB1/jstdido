﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="JST_DeDo.Detail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/fdiaryGrid.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.2.1.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Styles/pagination-ys.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/pagination-ys.css" rel="stylesheet" />

    <style>
        .table-striped > tbody > tr:nth-child(odd) > td {
            background-color: #F7F7DE;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <h2>Events and Task details.</h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:HyperLink ID="HyperLink1" runat="server" Text="Back  De Do" NavigateUrl="~/ToDo.aspx" CssClass="navbar-link"></asp:HyperLink>
                </div>
                <div class="panel-body">
                    <table class="table table-borderless" id="TblCat">
                        <thead>
                            <tr>
                                <th>Category
                                </th>
                                <th style="width: 240px">Department
                                </th>
                                <th>Gender
                                </th>
                                <th style="width: 190px">Employee(s)
                                </th>
                                <th>Begin Date
                                </th>
                                <th>End Date
                                </th>
                                <th>Follow Up/Review
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div id="divCtrlCategory" runat="server">
                                        <asp:TextBox ID="TxtCat" runat="server" AutoPostBack="false" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlDepartment" runat="server">
                                        <asp:TextBox ID="TxtDep" runat="server" AutoPostBack="false" TextMode="MultiLine" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlGender" runat="server" class="">
                                        <asp:TextBox ID="TxtGen" runat="server" AutoPostBack="false" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlEmployee" runat="server" class="">
                                        <asp:TextBox ID="TxtEmp" runat="server" AutoPostBack="false" TextMode="MultiLine" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlReqdate" class="input-append date form_datetimeB" style="width: 140px;" runat="server">
                                        <input id="TxtReq" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlfinish" class="input-append date form_datetimeE" style="width: 140px;" runat="server">
                                        <input id="TxtFin" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlFollow" class="input-append date form_datetimeF" style="width: 140px;" data-date-format="mm/dd/yyyy" runat="server">
                                        <input id="TxtFol" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <table class="table table-borderless" id="Tbl2">
                        <thead>
                            <tr>
                                <th style="width: 240px">Status
                                </th>
                                <th>Frequency
                                </th>
                                <th>Time Zone
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div id="divCtrlStatus" runat="server" class="">
                                        <asp:TextBox ID="TxtSta" runat="server" AutoPostBack="false" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlFrec" runat="server" class="">
                                        <asp:TextBox ID="TxtFre" runat="server" AutoPostBack="false" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                    <div id="divCtrlTz" runat="server" class="">
                                        <asp:TextBox ID="TxtTim" runat="server" AutoPostBack="false" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <table class="table table-borderless" id="TblSubj">
                        <thead>
                            <tr>
                                <th>Subject
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div id="divCtrlSubj" runat="server" class="">
                                        <span runat="server" id="spnSub"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th>
                                </th>
                            </tr>
                        </thead></table>


                    <div id="divGrid" style="width: 100%;">
                        <asp:GridView ID="gDetail" runat="server" AutoGenerateColumns="False"
                            DataKeyNames="event_id"
                            AllowPaging="True"
                            OnPageIndexChanging="GDetail_PageIndexChanging"
                            AllowSorting="True"
                            OnSorting="GDetail_Sorting"
                            OnRowDataBound="GDetail_RowDataBound"
                            ShowHeaderWhenEmpty="True"
                            BackColor="White"
                            BorderColor="#DEDFDE"
                            BorderStyle="None"
                            BorderWidth="1px"
                            CellPadding="4"
                            ForeColor="Black"
                            GridLines="Vertical"
                            CssClass="myGridCCS"
                            HorizontalAlign="Center"
                            ShowFooter="true"
                            PageSize="30" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" AllowCustomPaging="True">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>

                                <asp:TemplateField HeaderText="Id" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <%#Eval("idFd") %> / <%#Eval("event_id") %>
                                    </ItemTemplate>
                                    <HeaderStyle ForeColor="White" HorizontalAlign="Center"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Event Start" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStar" runat="server" Text='<%# Bind("event_start", "{0:MM/dd/yyyy hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle ForeColor="White" HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Event End" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnd" runat="server" Text='<%# Bind("event_end", "{0:MM/dd/yyyy hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle ForeColor="White" Width="18%" HorizontalAlign="Center"></HeaderStyle>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Event Update" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUpdate" runat="server" Text='<%# Bind("event_upd", "{0:MM/dd/yyyy hh:mm tt}") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle ForeColor="White" Width="18%" HorizontalAlign="Center"></HeaderStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" HeaderStyle-ForeColor="White">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("idStatus") %>' Visible="false" />
                                        <asp:DropDownList ID="ddEStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdFdStatus_SelectedIndexChanged" CssClass="form-control">
                                            <asp:ListItem Text="Select Status" Value="1"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle ForeColor="White" Width="13%" HorizontalAlign="Center"></HeaderStyle>
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F7DE" CssClass="myGridCSS" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                            <SortedAscendingHeaderStyle BackColor="#848384" />
                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                            <SortedDescendingHeaderStyle BackColor="#575357" />

                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Wrap="True" />
                    <PagerStyle CssClass="pagination-ys"  VerticalAlign="Middle" BackColor="" ForeColor="#333333" HorizontalAlign="Left" Font-Size="XX-Small" />
                    <PagerSettings Mode="NextPreviousFirstLast" FirstPageText=" First" LastPageText=" Last" NextPageText=" Next" PreviousPageText=" Previous" Position="TopAndBottom" PageButtonCount="10" />


                            <FooterStyle BackColor="#CCCC99" />
                        </asp:GridView>
                    </div>


                </div>
            </div>
        </div>



    </form>
</body>
</html>
