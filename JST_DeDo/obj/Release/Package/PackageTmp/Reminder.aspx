﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/Reminder.aspx.cs" Inherits="JST_DeDo.Reminder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/Bs4/jquery.min.js"></script>
    <script src="Scripts/Bs4/bootstrap.min.js"></script>
    <script src="Scripts/Bs3/bootstrap-datetimepicker.js"></script>


    <link href="Styles/Bs4/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <style>
        .border {
            display: inline-block;
            width: 100%;
            height: 100%;
        }
    </style>
</head>
<body>
    <form id="formRmd" runat="server">
        <div>
            <h2>Reminder programming.</h2>
            <hr />
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm border border-primary">
                    <div>
                        <asp:Label runat="server" CssClass="" Text=" Begin Date " Font-Size="Medium" Font-Bold="True" />
                        <asp:Image ID="Image" runat="server" data-toggle="collapse" data-target="#divBD1" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                        <br />
                        <asp:RadioButton ID="RadBDBefore" runat="server" Text="Before" GroupName="BeginD" AutoPostBack="false" />
                        <asp:RadioButton ID="RadBDAfter" runat="server" Text="After" GroupName="BeginD" AutoPostBack="false" Checked="True" />
                        <br />
                        <asp:TextBox ID="TxtRBD" runat="server" TextMode="DateTime" CssClass="form-control" Enabled="false" Width="140px" Text=""></asp:TextBox>
                        <hr />
                    </div>
                    <div id="DivBeginDate" runat="server">
                        <div id="divBD1" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtBDNumber1" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddBD1" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="ImgBDLess1" runat="server" data-toggle="collapse" data-target="#divBD1" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Img1BDMore" runat="server" data-toggle="collapse" data-target="#divBD2" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divBD2" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtBDNumber2" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddBD2" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image2" runat="server" data-toggle="collapse" data-target="#divBD2" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image3" runat="server" data-toggle="collapse" data-target="#divBD3" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divBD3" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtBDNumber3" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddBD3" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image4" runat="server" data-toggle="collapse" data-target="#divBD3" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image5" runat="server" data-toggle="collapse" data-target="#divBD4" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divBD4" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtBDNumber4" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddBD4" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image6" runat="server" data-toggle="collapse" data-target="#divBD4" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image7" runat="server" data-toggle="collapse" data-target="#divBD5" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divBD5" runat="server" class="collapse" style="width: 108%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtBDNumber5" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddBD5" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image8" runat="server" data-toggle="collapse" data-target="#divBD5" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm border border-success">
                    <div>
                        <asp:Label runat="server" CssClass="" Text=" End Date." Font-Size="Medium" Font-Bold="True" />
                        <asp:Image ID="Image18" runat="server" data-toggle="collapse" data-target="#divED1" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                        <br />
                        <asp:RadioButton ID="RadioButton3" runat="server" Text="Before" GroupName="ED" AutoPostBack="false" />
                        <asp:RadioButton ID="RadioButton4" runat="server" Text="After" GroupName="ED" AutoPostBack="false" Checked="true" />
                        <br />
                        <asp:TextBox ID="TxtRED" runat="server" TextMode="DateTime" CssClass="form-control" Enabled="false" Width="140px"></asp:TextBox>
                        <hr />
                    </div>
                    <div>
                        <div id="divED1" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtEDNumber1" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddED1" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image1" runat="server" data-toggle="collapse" data-target="#divED1" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image9" runat="server" data-toggle="collapse" data-target="#divED2" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divED2" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtEDNumber2" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddED2" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image10" runat="server" data-toggle="collapse" data-target="#divED2" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image11" runat="server" data-toggle="collapse" data-target="#divED3" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divED3" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtEDNumber3" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddED3" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image12" runat="server" data-toggle="collapse" data-target="#divED3" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image13" runat="server" data-toggle="collapse" data-target="#divED4" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divED4" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtEDNumber4" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddED4" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image14" runat="server" data-toggle="collapse" data-target="#divED4" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image15" runat="server" data-toggle="collapse" data-target="#divED5" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divED5" runat="server" class="collapse" style="width: 108%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtEDNumber5" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddED5" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image16" runat="server" data-toggle="collapse" data-target="#divED5" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm border border-danger" id="DivFU">
                    <div>
                        <asp:Label runat="server" CssClass="" Text=" Follow Up/Review" Font-Size="Medium" Font-Bold="True" />
                        <asp:Image ID="Image19" runat="server" data-toggle="collapse" data-target="#divFU1" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                        <br />
                        <asp:RadioButton ID="RadioButton5" runat="server" Text="Before" GroupName="ChkFU" AutoPostBack="false" />
                        <asp:RadioButton ID="RadioButton6" runat="server" Text="After" GroupName="ChkFU" AutoPostBack="false" Checked="true" />
                        <br />
                        <asp:TextBox ID="TxtRFU" runat="server" TextMode="DateTime" CssClass="form-control" Enabled="false" Width="140px"></asp:TextBox>
                        <hr />
                    </div>
                    <div>
                        <div id="divFU1" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtFUNumber1" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddFU1" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image21" runat="server" data-toggle="collapse" data-target="#divFU1" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image22" runat="server" data-toggle="collapse" data-target="#divFU2" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divFU2" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtFUNumber2" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddFU2" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image23" runat="server" data-toggle="collapse" data-target="#divFU2" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image24" runat="server" data-toggle="collapse" data-target="#divFU3" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divFU3" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtFUNumber3" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddFU3" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image25" runat="server" data-toggle="collapse" data-target="#divFU3" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image26" runat="server" data-toggle="collapse" data-target="#divFU4" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divFU4" runat="server" class="collapse" style="width: 115%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtFUNumber4" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddFU4" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image27" runat="server" data-toggle="collapse" data-target="#divFU4" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            <asp:Image ID="Image28" runat="server" data-toggle="collapse" data-target="#divFU5" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="divFU5" runat="server" class="collapse" style="width: 108%;">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input runat="server" id="TxtFUNumber5" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddFU5" runat="server" AutoPostBack="false" CssClass="form-control">
                                                <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Image ID="Image29" runat="server" data-toggle="collapse" data-target="#divFU5" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm border border-warning" id="DivCD">
                    <div>
                        <asp:Label runat="server" CssClass="" Text=" Custom Date " Font-Size="Medium" Font-Bold="True" />
                        <asp:Image ID="Image20" runat="server" data-toggle="collapse" data-target="#divCD1" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                        <br />
                        <asp:RadioButton ID="RadioButton7" runat="server" Text="Before" GroupName="RadCD" AutoPostBack="false" />
                        <asp:RadioButton ID="RadioButton8" runat="server" Text="After" GroupName="RadCD" AutoPostBack="false" Checked="true" />
                        <br />
                        <div id="divCtrlRe" class="input-append date form_datetimeB" style="width: 140px;" runat="server">
                            <div id="divCtrlCD" class="input-append date form_datetimeCD" style="width: 140px;" data-date-format="mm/dd/yyyy" runat="server">
                                <input id="txtCD" size="16" type="text" value="" readonly="false" class="form-control" runat="server" />
                                <span class="add-on"><i class="icon-remove"></i></span>
                                <span class="add-on"><i class="icon-calendar"></i></span>
                            </div>
                            <hr />
                        </div>
                        <div>
                            <div id="divCD1" runat="server" class="collapse" style="width: 115%;">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input runat="server" id="TxtCDNumber1" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCD1" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Image ID="Image30" runat="server" data-toggle="collapse" data-target="#divCD1" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                                <asp:Image ID="Image31" runat="server" data-toggle="collapse" data-target="#divCD2" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="divCD2" runat="server" class="collapse" style="width: 115%;">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input runat="server" id="TxtCDNumber2" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCD2" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Image ID="Image32" runat="server" data-toggle="collapse" data-target="#divCD2" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                                <asp:Image ID="Image33" runat="server" data-toggle="collapse" data-target="#divCD3" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="divCD3" runat="server" class="collapse" style="width: 115%;">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input runat="server" id="TxtCDNumber3" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCD3" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Image ID="Image34" runat="server" data-toggle="collapse" data-target="#divCD3" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                                <asp:Image ID="Image35" runat="server" data-toggle="collapse" data-target="#divCD4" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="divCD4" runat="server" class="collapse" style="width: 115%;">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input runat="server" id="TxtCDNumber4" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCD4" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Image ID="Image36" runat="server" data-toggle="collapse" data-target="#divCD4" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                                <asp:Image ID="Image37" runat="server" data-toggle="collapse" data-target="#divCD5" ImageUrl="~/Img/icons8-mas-16.png" ToolTip="Click to Add new Reminder" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="divCD5" runat="server" class="collapse" style="width: 108%;">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input runat="server" id="TxtCDNumber5" type="number" min="1" max="365" step="1" value="" class="form-control" maxlength="365" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCD5" runat="server" AutoPostBack="false" CssClass="form-control">
                                                    <asp:ListItem Text="Minute(s)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Hour(s)" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Day(s)" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Week(s)" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Image ID="Image38" runat="server" data-toggle="collapse" data-target="#divCD5" ImageUrl="~/Img/icons8-menos-16.png" ToolTip="Click to Remove" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div style="vertical-align: middle; text-align: right">
            <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-warning btn-xs" OnClick="BtnClose_Click" ToolTip="Close this window" />
            &nbsp;|&nbsp;
            <asp:Button ID="BtnSaveReminders" Text="Save" runat="server" CssClass="btn btn-primary btn-xs" OnClick="BtnSaveReminders_Click" />
        </div>
    </form>
    <script type="text/javascript">
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $(".form_datetimeCD").datetimepicker({
            format: "mm/dd/yyyy HH:ii P",
            showMeridian: true,
            autoclose: true,
            todayBtn: true,
            startDate: today,
            minuteStep: 5
        });
    </script>
</body>
</html>
