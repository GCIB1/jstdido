﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="JST_DeDo.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script type="text/javascript" src="Scripts/Bs4/bootstrap.min.js"></script>
    <script type="text/javascript" src="Scripts/Bs4/jquery.min.js"></script>
    <link href="Styles/Bs4/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css"/>
</head>
<body>
    <div class="container">
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form id="form1" runat="server">
                        <h2 class="card-title text-center mb-4 mt-1">ToDo Sign in</h2>
                        <hr />
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                </div>
                                <%--<input name="" class="form-control" placeholder="Email" type="email" id="txtEmail" runat="server" />--%>
                                <input name="" class="form-control" placeholder="Email" type="text" id="txtEmail" runat="server" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-lock"></i></span>
                                </div>
                                <input class="form-control" placeholder="******" type="password" id="txtPassw" runat="server" />
                            </div>
                        </div>
                        <div>
                            <p class="text-center">
                                <asp:Button CssClass="btn btn-primary" runat="server" OnClick="ValidateUser" Text=" Login " />
                            </p>
                        </div>
                        <div class="alert alert-danger" id="divErr" runat="server" visible="false">
                            <strong>
                                <asp:Label runat="server" ID="LblMsj1" Text=""></asp:Label>
                            </strong>
                            <asp:Label runat="server" ID="Label2" Text=""></asp:Label>
                        </div>
                        <hr />
                        <p class="text-center"><a href="NewUserForgot.aspx" class="btn">Forgot password?
                            <br />
                            New user?</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

