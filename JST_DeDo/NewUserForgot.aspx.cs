﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace JST_DeDo
{
    public partial class NewUserForgot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LblMsjErr.Text = "";
                LblSucces.Text = "";
                DropDownList ddE = (DropDownList)divEmployee.FindControl("ddFdIdusuarioFilter");
                BindEmployees(ddE, PopulateEmployees());
            }
        }

        private List<Employee> PopulateEmployees()
        {
            using (FdiaryEntities de = new FdiaryEntities())
            {
                return de.Employees.Where(e => e.Status == 1).OrderBy(e => e.EmployeeName).ToList();
            }
        }

        private void BindEmployees(DropDownList ddFdIdusuario, List<Employee> employee)
        {
            ddFdIdusuario.Items.Clear();
            ddFdIdusuario.AppendDataBoundItems = true;
            ddFdIdusuario.DataTextField = "EmployeeName";
            ddFdIdusuario.DataValueField = "EmployeeID";
            ddFdIdusuario.DataSource = employee;
            ddFdIdusuario.DataBind();
        }

        protected void BtnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                divErr.Visible = false;
                bool save = true;
                DateTime dtupd = DateTime.Today;
                TimeSpan tsStar = DateTime.Now.TimeOfDay;
                dtupd = dtupd.Add(tsStar);
                int idMail = 0;
                int idEmpl = Convert.ToInt32(ddFdIdusuarioFilter.SelectedValue);
                string p, pe = "";
                p = "123abc";
                pe = Utils.Util.Encrypt(p);
                string sDomain = "";

                try
                {
                    string sEmail = EmailMail.Value.ToString().Trim();
                    sDomain = sEmail.Substring(sEmail.IndexOf("@") + 1);
                    if (sDomain.IndexOf(".") > 0)
                    {
                        sDomain = sDomain.Substring(0, sDomain.IndexOf("."));
                    }
                }
                catch (Exception)
                {
                    sDomain = "";
                }

                if (sDomain.Length > 0)
                {




                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        var dom = de.EmailDomains.Where(d => d.EmailDomain1.Contains(sDomain)).ToList();
                        if (dom.Count > 0)
                        {
                            var ml = de.EmployeeEmails.Where(m => m.Email == EmailMail.Value).FirstOrDefault();
                            if (ml != null)
                            {
                                var objLogin = de.FdLogins.Where(l => l.LEmailID == ml.EmployeeEmailID).FirstOrDefault();
                                if (objLogin != null)
                                {
                                    objLogin.LPassw = pe;
                                }
                                de.SaveChanges();
                            }
                            else
                            {
                                var objMail = new EmployeeEmail()
                                {
                                    EmployeeID = idEmpl,
                                    Email = EmailMail.Value
                                };
                                de.EmployeeEmails.Add(objMail);
                                de.SaveChanges();

                                idMail = objMail.EmployeeEmailID;

                                var objLogin = new FdLogin()
                                {
                                    LEmpID = idEmpl,
                                    LEmailID = idMail,
                                    LPassw = pe,
                                    LCreateDate = dtupd,
                                    LProfile = "2",
                                    LDepartment = "-"
                                };
                                try
                                {
                                    de.FdLogins.Add(objLogin);
                                    de.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    save = false;
                                    ex.ToString();
                                    divErr.Visible = true;
                                    LblMsjErr.Text = ex.ToString();
                                }
                            }
                            if (save)
                            {
                                divSuccess.Visible = true;
                                LblSucces.Text = "Done! please check your inbox.";
                                BtnSendMail.Visible = false;
                                //Utils.Util.SendEmail(EmailMail.Value, p);
                                Utils.Util.SendNEWEmail(EmailMail.Value, p);
                            }
                            else
                            {
                                BtnSendMail.Visible = true;
                            }
                        }
                        else
                        {
                            divErr.Visible = true;
                            LblMsjErr.Text = "Email must be one of the corporation. Example ('someone@gcib.net, someone@ghins.com...')";
                        }
                    }
                }
                else
                {
                    divErr.Visible = true;
                    LblMsjErr.Text = "Please check the mail syntax.";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                divErr.Visible = true;
                LblMsjErr.Text = ex.ToString();
                throw;
            }

        }
    }
}