﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowNotifications.aspx.cs" Inherits="JST_DeDo.Test" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Jquery - notification popup box using toastr JS</title>

    <script src="Scripts/Notify/jquery.min.js"></script>
    <script src="Scripts/Notify/toastr.min.js"></script>
    <link href="Styles/Notify/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/Notify/toastr.min.css" rel="stylesheet" />

    <script type="text/javascript"> 
        function fxReminderScreen(strInfo) {
        //$(".success").click(function(){
	    //$(".error").click(function(){
        //$(".warning").click(function(){
        //$(".info").click(function ()
        toastr.info(strInfo, 'DeDo Notifications!',
        {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "3000",
            "hideDuration": "1000",
            "timeOut": "50000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        })
	}
    </script>

</head>
<body>
    <asp:Label ID="lblJavaScript" runat="server" Text="" ></asp:Label>
	<button class="info btn btn-info">Info</button>
</body>
</html>