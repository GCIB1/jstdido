﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace JST_DeDo
{

    public partial class Recurrency : Page
    {
        private bool BClaims = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    if (Session["SsDepartments"] != null)
                    {
                        List<Department> d = (List<Department>)Session["SsDepartments"];
                        if (d != null)
                        {
                            var dep = d.Find(x => x.DepartmentID == 2);
                            if (dep != null)
                            {
                                BClaims = true;
                            }
                        }
                    }
                    PopulateControls();
                }
            }
        }

        public HtmlControl Iframe => this.ifReminders;


        private void BindFrecuency(DropDownList ddFdFrecuency, List<FdFrequency> frecuency)
        {
            ddFdFrecuency.Items.Clear();
            ddFdFrecuency.AppendDataBoundItems = true;
            ddFdFrecuency.DataTextField = "FFrequency";
            ddFdFrecuency.DataValueField = "FId";
            ddFdFrecuency.DataSource = frecuency;
            ddFdFrecuency.DataBind();
        }

        private List<FdFrequency> PopulateFrequency()
        {
            using (FdiaryEntities fd = new FdiaryEntities())
            {
                return fd.FdFrequencies.Where(f => f.FFStatus == 1).OrderBy(f => f.FFOrder).ToList();
            }
        }

        protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date.CompareTo(DateTime.Today) < 0)
            {
                e.Day.IsSelectable = false;
            }
        }

        private void SetCurrentTimeToText()
        {
            HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlReqdate.FindControl("txtFdReqDate");
            txtFdReqDate.Value = DateTime.Now.ToLocalTime().ToString("MM/dd/yyyy hh: mm tt");
        }

        private void BindGender(DropDownList ddFdGender, List<FdGender> gender)
        {
            ddFdGender.Items.Clear();
            ddFdGender.AppendDataBoundItems = true;
            ddFdGender.DataTextField = "GenName";
            ddFdGender.DataValueField = "GenId";
            ddFdGender.DataSource = gender;
            ddFdGender.DataBind();
        }

        private void BindStatus(DropDownList ddFdStatus, List<FdStatu> status)
        {
            ddFdStatus.Items.Clear();
            ddFdStatus.AppendDataBoundItems = true;
            ddFdStatus.DataTextField = "StatusName";
            ddFdStatus.DataValueField = "StatusId";
            ddFdStatus.DataSource = status;
            ddFdStatus.DataBind();
        }

        private void BindCategory(DropDownList ddFdCategory, List<FdCategory> category)
        {
            ddFdCategory.Items.Clear();
            ddFdCategory.AppendDataBoundItems = true;
            ddFdCategory.DataTextField = "CatName";
            ddFdCategory.DataValueField = "CatId";
            ddFdCategory.DataSource = category;
            ddFdCategory.DataBind();
        }

        private void BindTimeZone(DropDownList ddFdTimeZone, List<FdTimeZone> tz)
        {
            ddFdTimeZone.Items.Clear();
            ddFdTimeZone.AppendDataBoundItems = true;
            ddFdTimeZone.DataTextField = "TzName";
            ddFdTimeZone.DataValueField = "TzId";
            ddFdTimeZone.DataSource = tz;
            ddFdTimeZone.DataBind();
        }

        private List<Department> PopulateDepartments()
        {
            using (FdiaryEntities dep = new FdiaryEntities())
            {
                return dep.Departments.OrderBy(d => d.DepartmentName).ToList();
            }
        }

        private void BindListDeparments(List<Department> departments)
        {
            sDepartment.Items.Clear();
            sDepartment.DataTextField = "DepartmentName";
            sDepartment.DataValueField = "DepartmentID";
            sDepartment.DataSource = departments;
            sDepartment.DataBind();
        }

        private void BindDeparments(DropDownList ddFdIdDept, List<Department> departments)
        {
            ddFdIdDept.Items.Clear();
            ddFdIdDept.AppendDataBoundItems = true;
            ddFdIdDept.DataTextField = "DepartmentName";
            ddFdIdDept.DataValueField = "DepartmentID";
            ddFdIdDept.DataSource = departments;
            ddFdIdDept.DataBind();
        }

        private List<Employee> PopulateEmployees()
        {
            using (FdiaryEntities de = new FdiaryEntities())
            {
                return de.Employees.Where(e => e.Status == 1).OrderBy(e => e.EmployeeName).ToList();
            }
        }

        private void BindListEmployees(List<Employee> employee)
        {
            sEmployees.Items.Clear();
            sEmployees.DataTextField = "EmployeeName";
            sEmployees.DataValueField = "EmployeeID";
            sEmployees.DataSource = employee;
            sEmployees.DataBind();
        }

        private void BindEmployees(DropDownList ddFdIdusuario, List<Employee> employee)
        {
            ddFdIdusuario.Items.Clear();
            ddFdIdusuario.AppendDataBoundItems = true;
            ddFdIdusuario.DataTextField = "EmployeeName";
            ddFdIdusuario.DataValueField = "EmployeeID";
            ddFdIdusuario.DataSource = employee;
            ddFdIdusuario.DataBind();
        }

        private void BindStatus(DropDownList ddStatus, int statusId)
        {
            ddStatus.Items.Clear();
            ddStatus.Items.Add(new ListItem { Text = "Select Status", Value = "0" });
            ddStatus.AppendDataBoundItems = true;
            ddStatus.DataTextField = "StatusName";
            ddStatus.DataValueField = "StatusId";
            ddStatus.DataSource = statusId > 0 ? PopulateStatus() : null;
            ddStatus.DataBind();
        }

        private List<FdStatu> PopulateStatus()
        {
            using (FdiaryEntities fd = new FdiaryEntities())
            {
                return fd.FdStatus.Where(s => s.StatusActivo == 1).OrderBy(s => s.StatusOrder).ToList();
            }
        }

        private List<FdCategory> PopulateCategory()
        {
            using (FdiaryEntities fd = new FdiaryEntities())
            {
                return fd.FdCategories.Where(c => c.CatStatus == 1).OrderBy(c => c.CatOrder).ToList();
            }
        }

        private List<FdTimeZone> PopulateTimeZone()
        {
            using (FdiaryEntities fd = new FdiaryEntities())
            {
                return fd.FdTimeZones.Where(t => t.TzActive == 1).OrderBy(t => t.TzOrder).ToList();
            }
        }

        private List<FdGender> PopulateGender()
        {
            using (FdiaryEntities fd = new FdiaryEntities())
            {
                return fd.FdGenders.Where(g => g.GenStatus == 1).OrderBy(g => g.GenOrder).ToList();
            }
        }


        private void PopulateControls()
        {
            try
            {
                string strId = Request.QueryString["i"] == null ? "" : Request.QueryString["i"].ToString();
                string strEmpId = Session["SsEmpId"] == null ? "" : Session["SsEmpId"].ToString();
                string strDepIds = Session["SsDepartmentsCodes"] == null ? "" : Session["SsDepartmentsCodes"].ToString();
                DropDownList ddCat = (DropDownList)divCtrlCategory.FindControl("ddFdIdCategory");
                BindCategory(ddCat, PopulateCategory());
                BindListDeparments(PopulateDepartments());
                DropDownList ddGen = (DropDownList)divCtrlGender.FindControl("ddFdIdGender");
                BindGender(ddGen, PopulateGender());
                BindListEmployees(PopulateEmployees());

                DropDownList ddTz = (DropDownList)divCtrlTimeZone.FindControl("ddFdIdTimeZone");
                BindTimeZone(ddTz, PopulateTimeZone());
                DropDownList ddFrec = (DropDownList)divCtrlFrequency.FindControl("ddFdIdFrequency");
                BindFrecuency(ddFrec, PopulateFrequency());
                DropDownList ddS = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");
                BindStatus(ddS, PopulateStatus());

                HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlReqdate.FindControl("txtFdReqDate");
                HtmlInputText txtFdFinishDate = (HtmlInputText)divCtrlfinish.FindControl("txtFdFinishDate");
                HtmlInputText txtFdFollow = (HtmlInputText)divCtrlFollow.FindControl("txtFdFollow");
                DropDownList ddStatus = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");

                TxtClaims.Value = BClaims.ToString();

                if (strId.Equals(""))
                {
                    txtFdFinishDate.Value = "";
                    txtFdFollow.Value = "";
                    txtID.Text = "";
                    BtnDelete.Visible = false;
                    BtnClone.Visible = false;
                    SetCurrentTimeToText();
                    PnlClaims.Visible = false;
                    if (!strEmpId.Equals("0"))
                    {
                        SelectedOptions((HtmlSelect)divCtrlEmployee.FindControl("sEmployees"), strEmpId);
                    }

                }
                else
                {
                    DateTime dtReq, dtFin, dtFoll = DateTime.Today;
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        int d = Convert.ToInt32(strId);
                        var v = de.Fdiaries.Where(a => a.FdId == d).FirstOrDefault();
                        if (v != null)
                        {
                            ddCat.SelectedValue = v.FdIdCategory.ToString();
                            SelectedOptions((HtmlSelect)divCtrlDepartment.FindControl("sDepartment"), v.FdIdDept);
                            ddGen.SelectedValue = v.FdIdGender.ToString();
                            SelectedOptions((HtmlSelect)divCtrlEmployee.FindControl("sEmployees"), v.FdIdusuario);

                            ddFrec.SelectedValue = v.FdIdFrequency.ToString();
                            ddTz.SelectedValue = v.FdIdTz.ToString();

                            dtReq = DateTime.Parse(v.FdReqDate.ToString());
                            txtFdReqDate.Value = dtReq.ToString("MM/dd/yyyy hh:mm tt");

                            dtFin = DateTime.Parse(v.FdFinishDate.ToString());
                            txtFdFinishDate.Value = dtFin.ToString("MM/dd/yyyy hh:mm tt");
                            dtFoll = DateTime.Parse(v.FdFollow.ToString());
                            txtFdFollow.Value = dtFoll.ToString("MM/dd/yyyy hh:mm tt");
                            ddStatus.SelectedValue = v.FdStatus.ToString();
                            replyBody.Text = Server.HtmlDecode(v.FdSubject);
                            txtID.Text = Server.HtmlDecode(v.FdId.ToString());

                            if (BClaims)
                            {
                                TxtReceivedC.Value = v.FdRecivedC == null ? "0" : v.FdRecivedC.ToString();
                                TxtClosedC.Value = v.FdClosedC == null ? "0" : v.FdClosedC.ToString();
                                TxtPendingC.Value = v.FdPendingC == null ? "0" : v.FdPendingC.ToString();
                                TxtPending30C.Value = v.FdPending30C == null ? "0" : v.FdPending30C.ToString();
                                TxtPending60C.Value = v.FdPending60C == null ? "0" : v.FdPending60C.ToString();
                                TxtPending90C.Value = v.FdPending90C == null ? "0" : v.FdPending90C.ToString();
                                TxtLossRunC.Text = v.FdLostRunC ?? "";

                                TxtPhoneCallsInC.Value = v.FdPhoneCallsInC == null ? "0" : v.FdPhoneCallsInC.ToString();
                                TxtPhoneCallsOutC.Value = v.FdPhoneCallsOutC == null ? "0" : v.FdPhoneCallsOutC.ToString();
                                TxtEmailsInC.Value = v.FdEmailsInC == null ? "0" : v.FdEmailsInC.ToString();
                                TxtEmailsOutC.Value = v.FdEmailsOutC == null ? "0" : v.FdEmailsOutC.ToString();

                                TxtLRunsC.Value = v.FdLRunC == null ? "0" : v.FdLRunC.ToString();
                                TxtFNOLC.Value = v.FdFNOLC == null ? "0" : v.FdFNOLC.ToString();
                                TxtCleranceC.Value = v.FdCleranceC == null ? "0" : v.FdCleranceC.ToString();
                            }
                        }
                    }
                    BtnDelete.Visible = true;
                    BtnDelete.Enabled = true;
                    BtnClone.Visible = true;
                    BtnClone.Enabled = true;
                }
                if (BClaims)
                {
                    PnlClaims.Visible = true;
                }
                if (!strDepIds.Equals(""))
                {
                    SelectedOptions((HtmlSelect)divCtrlDepartment.FindControl("sDepartment"), strDepIds);
                }
                BtnCancel.Visible = true;
                BtnCancel.Enabled = true;
            }
            catch (Exception exp)
            {
                exp.ToString();
                throw;
            }
        }

        protected void BtnInsertGrid_Click(object sender, EventArgs e)
        {
            bool Bdone = true;
            bool Bnew = true;
            string strErr = "";
            string strEvTit = "";
            string strEvDesc = "";
            int idDiary, iRecivedC, iClosedC, iPendingC, iPending30C, iPending60C, iPending90C, iPhoneCallsInC, iPhoneCallsOutC, iEmailsInC, iEmailsOutC, iLRunsC, iFNOLC, iCleranceC = 0;
            
            

            Page.Validate("Add");
            if (Page.IsValid)
            {
                string usrID = Session["SsEmpId"] == null ? "0" : Session["SsEmpId"].ToString();
                DropDownList ddFdIdCategory = (DropDownList)divCtrlCategory.FindControl("ddFdIdCategory");
                string strDepartment = Utils.Util.SelectToString((HtmlSelect)divCtrlDepartment.FindControl("sDepartment"));
                DropDownList ddFdIdGender = (DropDownList)divCtrlGender.FindControl("ddFdIdGender");
                string strUser = Utils.Util.SelectToString((HtmlSelect)divCtrlEmployee.FindControl("sEmployees"));
                DropDownList ddFdIdFrecuency = (DropDownList)divCtrlFrequency.FindControl("ddFdIdFrequency");
                DropDownList ddFdIdTimeZone = (DropDownList)divCtrlTimeZone.FindControl("ddFdIdTimeZone");
                HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlReqdate.FindControl("txtFdReqDate");
                HtmlInputText txtFdFinishDate = (HtmlInputText)divCtrlfinish.FindControl("txtFdFinishDate");
                HtmlInputText txtFdFollow = (HtmlInputText)divCtrlFollow.FindControl("txtFdFollow");
                TextBox txtFdStatus = (TextBox)divCtrlStatus.FindControl("txtFdStatus");
                DropDownList ddStatus = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");

                DateTime dtReq = DateTime.Today;
                DateTime dtfDate = DateTime.Today.AddDays(2);
                DateTime dtFollow = DateTime.Today.AddDays(2);

                if (txtFdReqDate.Value != null)
                {
                    if (txtFdReqDate.Value.Length > 8)
                    {
                        dtReq = DateTime.Parse(txtFdReqDate.Value.Trim());
                    }
                }
                if (txtFdFinishDate.Value != null)
                {
                    if (txtFdFinishDate.Value.Length > 5)
                    {
                        dtfDate = DateTime.Parse(txtFdFinishDate.Value.Trim());
                    }
                }
                if (txtFdFollow.Value != null)
                {
                    if (txtFdFollow.Value.Length > 5)
                    {
                        dtFollow = DateTime.Parse(txtFdFollow.Value.Trim());
                    }
                }

                if (txtID.Text.Equals(""))                                      //NEW DATA
                {
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        var Fdiary = new Fdiary()
                        {
                            FdIdDept = strDepartment,
                            FdIdusuario = strUser,
                            FdIdGender = Convert.ToInt32(ddFdIdGender.SelectedValue),
                            FdIdFrequency = Convert.ToInt32(ddFdIdFrecuency.SelectedValue),
                            FdIdTz = Convert.ToInt32(ddFdIdTimeZone.SelectedValue),
                            FdIdCategory = Convert.ToInt32(ddFdIdCategory.SelectedValue),
                            FdReqDate = dtReq,
                            FdFinishDate = dtfDate,
                            FdFollow = dtFollow,

                            FdSubject = Server.HtmlEncode(replyBody.Text),
                            FdStatus = Convert.ToInt32(ddStatus.SelectedValue),
                            FdCreationDate = DateTime.Today
                        };
                        if (!usrID.Equals(""))
                        {
                            Fdiary.FdCreateBy = Convert.ToInt32(usrID);
                        }

                        if (TxtClaims.Value.Equals("True"))
                        {
                            try
                            {
                                iRecivedC = Convert.ToInt32(TxtReceivedC.Value);
                            }
                            catch (Exception)
                            {
                                iRecivedC = 0;
                            }

                            try
                            {
                                iClosedC = Convert.ToInt32(TxtClosedC.Value);
                            }
                            catch (Exception)
                            {
                                iClosedC = 0;
                            }

                            try
                            {
                                iPendingC = Convert.ToInt32(TxtPendingC.Value);
                            }
                            catch (Exception)
                            {
                                iPendingC = 0;
                            }

                            try
                            {
                                iPending30C = Convert.ToInt32(TxtPending30C.Value);
                            }
                            catch (Exception)
                            {
                                iPending30C = 0;
                            }

                            try
                            {
                                iPending60C = Convert.ToInt32(TxtPending60C.Value);
                            }
                            catch (Exception)
                            {
                                iPending60C = 0;
                            }
                            try
                            {
                                iPending90C = Convert.ToInt32(TxtPending90C.Value);
                            }
                            catch (Exception)
                            {
                                iPending90C = 0;
                            }
                            try
                            {
                                iPhoneCallsInC = Convert.ToInt32(TxtPhoneCallsInC.Value);
                            }
                            catch (Exception)
                            {
                                iPhoneCallsInC = 0;
                            }
                            try
                            {
                                iPhoneCallsOutC = Convert.ToInt32(TxtPhoneCallsOutC.Value);
                            }
                            catch (Exception)
                            {
                                iPhoneCallsOutC = 0;
                            }
                            try
                            {
                                iEmailsInC = Convert.ToInt32(TxtEmailsInC.Value);
                            }
                            catch (Exception)
                            {
                                iEmailsInC = 0;
                            }
                            try
                            {
                                iEmailsOutC = Convert.ToInt32(TxtEmailsOutC.Value);
                            }
                            catch (Exception)
                            {
                                iEmailsOutC = 0;
                            }


                            try
                            {
                                iLRunsC = Convert.ToInt32(TxtLRunsC.Value);
                            }
                            catch (Exception)
                            {
                                iLRunsC = 0;
                            }

                            try
                            {
                                iFNOLC = Convert.ToInt32(TxtFNOLC.Value);
                            }
                            catch (Exception)
                            {
                                iFNOLC = 0;
                            }

                            try
                            {
                                iCleranceC = Convert.ToInt32(TxtCleranceC.Value);
                            }
                            catch (Exception)
                            {
                                iCleranceC = 0;
                            }




                            Fdiary.FdRecivedC = iRecivedC;
                            Fdiary.FdClosedC = iClosedC;
                            Fdiary.FdPendingC = iPendingC;
                            Fdiary.FdPending30C = iPending30C;
                            Fdiary.FdPending60C = iPending60C;
                            Fdiary.FdPending90C = iPending90C;
                            Fdiary.FdLostRunC = TxtLossRunC.Text;
                            Fdiary.FdPhoneCallsInC = iPhoneCallsInC;
                            Fdiary.FdPhoneCallsOutC = iPhoneCallsOutC;
                            Fdiary.FdEmailsInC = iEmailsInC;
                            Fdiary.FdEmailsOutC = iEmailsOutC;

                            Fdiary.FdLRunC= iLRunsC;
                            Fdiary.FdFNOLC = iFNOLC;
                            Fdiary.FdCleranceC= iCleranceC;
                        }
                        try
                        {
                            de.Fdiaries.Add(Fdiary);
                            de.SaveChanges();
                            idDiary = Fdiary.FdId;
                            strEvTit = ClearStr(Server.HtmlEncode(replyBody.Text), 50);
                            strEvDesc = ClearStr(Server.HtmlEncode(replyBody.Text), 500);
                            Bnew = true;
                        }
                        catch (Exception ex)
                        {
                            strErr = ex.ToString();
                            Bdone = false;
                            throw;
                        }

                        if (Bdone)
                        {
                            LblMsg.Text = "New data added!";
                        }
                        else
                        {
                            LblMsg.Text = strErr;
                        }
                    }
                }
                else                                                            //UPDATE
                {
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        idDiary = Convert.ToInt32(txtID.Text);
                        var f = de.Fdiaries.Where(a => a.FdId == idDiary).FirstOrDefault();
                        if (f != null)
                        {
                            f.FdIdDept = strDepartment;
                            f.FdIdusuario = strUser;
                            f.FdIdGender = Convert.ToInt32(ddFdIdGender.SelectedValue);
                            f.FdIdFrequency = Convert.ToInt32(ddFdIdFrecuency.SelectedValue);
                            f.FdIdTz = Convert.ToInt32(ddFdIdTimeZone.SelectedValue);
                            f.FdIdCategory = Convert.ToInt32(ddFdIdCategory.SelectedValue);
                            f.FdReqDate = dtReq;
                            f.FdFinishDate = dtfDate;
                            f.FdstrFinishDate = String.Format("{0:MM/dd/yyyy hh:mm tt}", dtfDate);
                            f.FdFollow = dtFollow;
                            f.FdSubject = Server.HtmlEncode(replyBody.Text);
                            f.FdStatus = Convert.ToInt32(ddFdStatus.SelectedValue);
                            strEvTit = ClearStr(Server.HtmlEncode(replyBody.Text), 50);
                            strEvDesc = ClearStr(Server.HtmlEncode(replyBody.Text), 500);

                            if (!usrID.Equals(""))
                            {
                                f.FdCreateBy = Convert.ToInt32(usrID);
                            }
                            if (TxtClaims.Value.Equals("True"))
                            {
                                try
                                {
                                    iRecivedC = Convert.ToInt32(TxtReceivedC.Value);
                                }
                                catch (Exception)
                                {
                                    iRecivedC = 0;
                                }

                                try
                                {
                                    iClosedC = Convert.ToInt32(TxtClosedC.Value);
                                }
                                catch (Exception)
                                {
                                    iClosedC = 0;
                                }

                                try
                                {
                                    iPendingC = Convert.ToInt32(TxtPendingC.Value);
                                }
                                catch (Exception)
                                {
                                    iPendingC = 0;
                                }

                                try
                                {
                                    iPending30C = Convert.ToInt32(TxtPending30C.Value);
                                }
                                catch (Exception)
                                {
                                    iPending30C = 0;
                                }

                                try
                                {
                                    iPending60C = Convert.ToInt32(TxtPending60C.Value);
                                }
                                catch (Exception)
                                {
                                    iPending60C = 0;
                                }

                                try
                                {
                                    iPending90C = Convert.ToInt32(TxtPending90C.Value);
                                }
                                catch (Exception)
                                {
                                    iPending90C = 0;
                                }

                                try
                                {
                                    iPhoneCallsInC = Convert.ToInt32(TxtPhoneCallsInC.Value);
                                }
                                catch (Exception)
                                {
                                    iPhoneCallsInC = 0;
                                }
                                try
                                {
                                    iPhoneCallsOutC = Convert.ToInt32(TxtPhoneCallsOutC.Value);
                                }
                                catch (Exception)
                                {
                                    iPhoneCallsOutC = 0;
                                }
                                try
                                {
                                    iEmailsInC = Convert.ToInt32(TxtEmailsInC.Value);
                                }
                                catch (Exception)
                                {
                                    iEmailsInC = 0;
                                }
                                try
                                {
                                    iEmailsOutC = Convert.ToInt32(TxtEmailsOutC.Value);
                                }
                                catch (Exception)
                                {
                                    iEmailsOutC = 0;
                                }

                                try
                                {
                                    iLRunsC = Convert.ToInt32(TxtLRunsC.Value);
                                }
                                catch (Exception)
                                {
                                    iLRunsC = 0;
                                }

                                try
                                {
                                    iFNOLC = Convert.ToInt32(TxtFNOLC.Value);
                                }
                                catch (Exception)
                                {
                                    iFNOLC = 0;
                                }

                                try
                                {
                                    iCleranceC = Convert.ToInt32(TxtCleranceC.Value);
                                }
                                catch (Exception)
                                {
                                    iCleranceC = 0;
                                }


                                f.FdRecivedC = iRecivedC;
                                f.FdClosedC = iClosedC;
                                f.FdPendingC = iPendingC;
                                f.FdPending30C = iPending30C;
                                f.FdPending60C = iPending60C;
                                f.FdPending90C = iPending90C;
                                f.FdLostRunC = TxtLossRunC.Text;
                                f.FdPhoneCallsInC = iPhoneCallsInC;
                                f.FdPhoneCallsOutC = iPhoneCallsOutC;
                                f.FdEmailsInC = iEmailsInC;
                                f.FdEmailsOutC = iEmailsOutC;

                                f.FdLRunC = iLRunsC;
                                f.FdFNOLC = iFNOLC;
                                f.FdCleranceC = iCleranceC;
                            }
                        }
                        try
                        {
                            de.SaveChanges();
                            Bnew = false;
                        }
                        catch (Exception ex)
                        {
                            strErr = ex.ToString();
                            Bdone = false;
                            throw;
                        }

                        if (!Bdone)
                        {
                            LblMsg.Text = strErr;
                        }
                        else
                        {
                            LblMsg.Text = "[" + txtID.Text + " ] Data update! ";
                        }
                    }
                }

                if (Bdone)
                {
                    BtnDelete.Enabled = false;
                    BtnClone.Enabled = false;
                    BtnCancel.Enabled = false;
                    BtnInsertGrid.Enabled = false;
                    DivMsg.Visible = true;
                    DataTable dtEventos = RecalculaFechas();
                    if (dtEventos.Rows.Count > 0)
                    {
                        if (Bnew)
                        {
                            using (FdiaryEntities de = new FdiaryEntities())
                            {
                                foreach (DataRow row in dtEventos.Rows)
                                {
                                    var ev = new Event()
                                    {
                                        title = strEvTit,
                                        description = strEvDesc,
                                        event_start = row.Field<DateTime>(1),
                                        event_end = row.Field<DateTime>(2),
                                        all_day = false,
                                        idFd = idDiary,
                                        idStatus = 1
                                    };
                                    de.Events.Add(ev);
                                }
                                try
                                {
                                    de.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    ex.ToString();
                                    Bnew = false;
                                    throw;
                                }
                            };
                        }
                        else
                        {
                            DateTime dtupd = DateTime.Today;
                            TimeSpan tsStar = DateTime.Now.TimeOfDay;
                            dtupd = dtupd.Add(tsStar);

                            using (FdiaryEntities de = new FdiaryEntities())
                            {
                                idDiary = Convert.ToInt32(txtID.Text);
                                var cal = de.Events.Where(d => d.idFd == idDiary);

                                int sSel = int.Parse(ddFdStatus.SelectedValue);

                                if (sSel == 2 || sSel == 3 || sSel == 4)
                                {
                                    sSel = 2;
                                }
                                if (cal != null)
                                {
                                    foreach (var c in cal)
                                    {
                                        c.title = strEvTit;
                                        c.description = strEvDesc;
                                        if (c.idStatus == 1 && sSel == 2)
                                        {
                                            c.idStatus = sSel;
                                            c.event_upd = dtupd;
                                        }
                                    }
                                    try
                                    {
                                        de.SaveChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        ex.ToString();

                                        throw;
                                    }
                                }
                            }
                        }
                    }

                    if (Bnew)      //SAVE REMINDERS IF HAVE
                    {
                        try
                        {
                            if (Session["SessReminders"] is DataTable dtReminders)
                            {
                                if (dtReminders.Rows.Count > 0)
                                {
                                    if (strUser.Trim().Equals(""))
                                    {

                                    }
                                    else
                                    {
                                        string[] arrEmployes = strUser.Split(',');
                                        if (arrEmployes.Length > 0)
                                        {
                                            foreach (string empl in arrEmployes)
                                            {
                                                if (!empl.Equals(""))
                                                {
                                                    using (FdiaryEntities de = new FdiaryEntities())
                                                    {
                                                        foreach (DataRow rDate in dtReminders.Rows)
                                                        {
                                                            var dRem = new FdReminder()
                                                            {
                                                                RemTitle = ClearStr(Utils.Util.HtmlToPlainText(replyBody.Text), 90),
                                                                RemDate = rDate.Field<DateTime>(0),
                                                                RemIdFD = idDiary,
                                                                RemStatus = 1,
                                                                RemId_usr = Convert.ToInt32(empl)
                                                            };
                                                            de.FdReminders.Add(dRem);
                                                        }
                                                        try
                                                        {
                                                            de.SaveChanges();
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ex.ToString();
                                                            Bnew = false;
                                                            throw;
                                                        }
                                                    };
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                            Bdone = false;
                        }
                    }
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseThis", "<script language='javascript'>window.parent.$('#BtnClear').click();</script>");
            }
            return;
        }

        private bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        private string ClearStr(string r, int Maxcar)
        {
            try
            {
                if (r.Length > Maxcar)
                {
                    r = r.Substring(0, Maxcar);
                }
            }
            catch (Exception e)
            {
                e.ToString();
                throw;
            }
            return r;
        }

        private void Clearfrm()
        {
            try
            {
                DropDownList ddIdCat = (DropDownList)divCtrlCategory.FindControl("ddFdIdCategory");
                BindListDeparments(PopulateDepartments());
                DropDownList ddIdGen = (DropDownList)divCtrlGender.FindControl("ddFdIdGender");
                BindListEmployees(PopulateEmployees());
                DropDownList ddIdFrec = (DropDownList)divCtrlFrequency.FindControl("ddFdIdFrequency");
                DropDownList ddIdTz = (DropDownList)divCtrlTimeZone.FindControl("ddFdIdTimeZone");
                HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlReqdate.FindControl("txtFdReqDate");
                HtmlInputText txtFdFinishDate = (HtmlInputText)divCtrlfinish.FindControl("txtFdFinishDate");
                HtmlInputText txtFdFollow = (HtmlInputText)divCtrlFollow.FindControl("txtFdFollow");
                DropDownList ddStatus = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");
                ddIdCat.SelectedIndex = -1;
                ddIdGen.SelectedIndex = -1;
                ddIdFrec.SelectedIndex = -1;
                ddIdTz.SelectedIndex = -1;
                txtFdReqDate.Value = "";
                txtFdFinishDate.Value = "";
                txtFdFollow.Value = "";
                ddStatus.SelectedIndex = -1;
                DivMsgFrec.Visible = false;
                lblConfFrec.Text = "";
            }
            catch (Exception exp)
            {
                exp.ToString();
                throw;
            }
        }

        private void SelectedOptions(HtmlSelect htmlSelect, string strSelected)
        {
            try
            {
                string[] arrSelected = strSelected.Split(',');
                if (arrSelected.Length > 0)
                {
                    foreach (string ssel in arrSelected)
                    {
                        if (!ssel.Equals(""))
                        {
                            foreach (ListItem item in htmlSelect.Items)
                            {
                                if (ssel == item.Value)
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                exc.ToString();
                throw;
            }
        }

        protected void BtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseThis", "<script language='javascript'>window.parent.$('#BtnClear').click();</script>");
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Clearfrm();
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            int fdId = 0;
            try
            {
                fdId = Convert.ToInt32(txtID.Text);
            }
            catch (Exception)
            {
                fdId = 0;
                throw;
            }
            if (fdId > 0)
            {
                using (FdiaryEntities de = new FdiaryEntities())
                {
                    var v = de.Fdiaries.Where(a => a.FdId == fdId).FirstOrDefault();
                    if (v != null)
                    {
                        v.FdStatus = 3;
                    }
                    de.SaveChanges();

                    BtnDelete.Enabled = false;
                    BtnClone.Enabled = false;
                    BtnCancel.Enabled = false;
                    BtnInsertGrid.Enabled = false;

                    DropDownList ddStatus = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");
                    Bloqueo();
                    DivMsg.Visible = true;
                    LblMsg.Text = "Done !";
                }
            }
        }

        private void Bloqueo()
        {
            DropDownList ddIdCat = (DropDownList)divCtrlCategory.FindControl("ddFdIdCategory");
            BindListDeparments(PopulateDepartments());
            DropDownList ddIdGen = (DropDownList)divCtrlGender.FindControl("ddFdIdGender");
            BindListEmployees(PopulateEmployees());
            DropDownList ddIdFrec = (DropDownList)divCtrlFrequency.FindControl("ddFdIdFrequency");
            DropDownList ddIdTz = (DropDownList)divCtrlTimeZone.FindControl("ddFdIdTimeZone");
            HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlReqdate.FindControl("txtFdReqDate");
            HtmlInputText txtFdFinishDate = (HtmlInputText)divCtrlfinish.FindControl("txtFdFinishDate");
            HtmlInputText txtFdFollow = (HtmlInputText)divCtrlFollow.FindControl("txtFdFollow");
            DropDownList ddStatus = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");

            ddIdCat.Enabled = false;
            sDepartment.Disabled = true;
            ddIdGen.Enabled = false;
            sEmployees.Disabled = true;
            ddIdFrec.Enabled = false;
            ddIdTz.Enabled = false;
            txtFdReqDate.Disabled = false;
            txtFdFinishDate.Disabled = false;
            txtFdFollow.Disabled = false;
            ddStatus.Enabled = false;
            try
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseThis", "<script language='javascript'>window.parent.$('#BtnClear').click();</script>");
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        protected void BtnClone_Click(object sender, EventArgs e)
        {
            bool Bdone = true;
            string strErr = "";

            DropDownList ddFdIdCategory = (DropDownList)divCtrlCategory.FindControl("ddFdIdCategory");
            string strDepartment = Utils.Util.SelectToString((HtmlSelect)divCtrlDepartment.FindControl("sDepartment"));
            DropDownList ddFdIdGender = (DropDownList)divCtrlGender.FindControl("ddFdIdGender");
            string strUser = Utils.Util.SelectToString((HtmlSelect)divCtrlEmployee.FindControl("sEmployees"));
            DropDownList ddFdIdFrecuency = (DropDownList)divCtrlFrequency.FindControl("ddFdIdFrequency");
            DropDownList ddFdIdTimeZone = (DropDownList)divCtrlTimeZone.FindControl("ddFdIdTimeZone");

            HtmlInputText txtFdReqDate = (HtmlInputText)divCtrlReqdate.FindControl("txtFdReqDate");

            HtmlInputText txtFdFinishDate = (HtmlInputText)divCtrlfinish.FindControl("txtFdFinishDate");
            HtmlInputText txtFdFollow = (HtmlInputText)divCtrlFollow.FindControl("txtFdFollow");
            TextBox txtFdStatus = (TextBox)divCtrlStatus.FindControl("txtFdStatus");
            DropDownList ddStatus = (DropDownList)divCtrlStatus.FindControl("ddFdStatus");

            DateTime dtReq = DateTime.Today;
            DateTime dtfDate = DateTime.Today.AddDays(2);
            DateTime dtFollow = DateTime.Today.AddDays(2);

            if (txtFdReqDate.Value != null)
            {
                if (txtFdReqDate.Value.Length > 8)
                {
                    dtReq = DateTime.Parse(txtFdReqDate.Value.Trim());
                }
            }
            if (txtFdFinishDate.Value != null)
            {
                if (txtFdFinishDate.Value.Length > 5)
                {
                    dtfDate = DateTime.Parse(txtFdFinishDate.Value.Trim());
                }
            }
            if (txtFdFollow.Value != null)
            {
                if (txtFdFollow.Value.Length > 5)
                {
                    dtFollow = DateTime.Parse(txtFdFollow.Value.Trim());
                }
            }

            using (FdiaryEntities de = new FdiaryEntities())
            {
                de.Fdiaries.Add(new Fdiary
                {
                    FdIdDept = strDepartment,
                    FdIdusuario = strUser,
                    FdIdGender = Convert.ToInt32(ddFdIdGender.SelectedValue),
                    FdIdFrequency = Convert.ToInt32(ddFdIdFrecuency.SelectedValue),
                    FdIdTz = Convert.ToInt32(ddFdIdTimeZone.SelectedValue),
                    FdIdCategory = Convert.ToInt32(ddFdIdCategory.SelectedValue),
                    FdReqDate = dtReq,
                    FdFinishDate = dtfDate,
                    FdFollow = dtFollow,
                    FdSubject = "",
                    FdStatus = Convert.ToInt32(ddStatus.SelectedValue),
                    FdCreationDate = DateTime.Today
                });

                try
                {
                    de.SaveChanges();
                }
                catch (Exception ex)
                {
                    strErr = ex.ToString();
                    Bdone = false;
                    throw;
                }

                if (!Bdone)
                {
                    LblMsg.Text = strErr;
                }
                else
                {
                    LblMsgClone.Text = "New data Cloned !";
                }

                DivMsgclone.Visible = true;
            }

            if (Bdone)
            {
                BtnDelete.Enabled = false;
                BtnClone.Enabled = false;
                BtnCancel.Enabled = false;
                BtnInsertGrid.Enabled = false;
                try
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseThis", "<script language='javascript'>window.parent.$('#BtnClear').click();</script>");
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    throw;
                }
            }
        }

        private DataTable RecalculaFechas()
        {
            DataTable tEvent = new DataTable();
            int iJump, i = 0;
            double dCicles = 0;
            try
            {
                tEvent.Columns.Add("Name", typeof(string));
                tEvent.Columns.Add("Start", typeof(DateTime));
                tEvent.Columns.Add("End", typeof(DateTime));

                int s = Convert.ToInt32(ddFdIdFrequency.SelectedItem.Value);
                string opc = "";

                DateTime ReqD = DateTime.Today;
                DateTime EndD = DateTime.Today;
                DateTime EndDEnd = DateTime.Today;
                DateTime FollD = DateTime.Today;

                DateTime dateInsert;
                DateTime x20MinsLater;


                TimeSpan tsStar = new TimeSpan(8, 0, 0);
                TimeSpan tsEnd = new TimeSpan(9, 0, 0);

                string fNum = "";
                string fOpc = "";

                if (txtFdReqDate.Value != null)
                {
                    if (txtFdReqDate.Value.Length > 8)
                    {
                        ReqD = DateTime.Parse(txtFdReqDate.Value.Trim());
                    }
                }
                if (txtFdFinishDate.Value != null)
                {
                    if (txtFdFinishDate.Value.Length > 5)
                    {
                        EndD = DateTime.Parse(txtFdFinishDate.Value.Trim());
                    }
                    else
                    {
                        EndD = DateTime.Today.AddYears(4);
                        EndD = EndD.Add(tsStar);
                    }
                }
                if (txtFdFollow.Value != null)
                {
                    if (txtFdFollow.Value.Length > 5)
                    {
                        FollD = DateTime.Parse(txtFdFollow.Value.Trim());
                    }
                    else
                    {
                        FollD = DateTime.Today.AddYears(4);
                        FollD = FollD.Add(tsStar);
                    }
                }

                if (s == 0 || s == 14)
                {
                    DivMsgFrec.Visible = false;
                    lblConfFrec.Text = "";
                    x20MinsLater = EndD.AddMinutes(20);
                    tEvent.Rows.Add("", EndD, x20MinsLater);
                }
                else
                {
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        var f = de.FdFrequencies.Where(o => o.FId == s).FirstOrDefault();
                        if (f != null)
                        {
                            opc = f.FConfig;
                        }
                    }
                    if (opc.Length > 0)
                    {
                        string[] arrOptions = opc.Split(',');
                        if (arrOptions.Length > 0)
                        {
                            fNum = arrOptions[0];
                            fOpc = arrOptions[1];
                        }

                        if (!fNum.Equals("") && !fOpc.Equals(""))
                        {
                            dCicles = Math.Truncate((EndD - ReqD).TotalDays);

                            if (fOpc.Equals("Q"))
                            {
                                dCicles = GetMonths(ReqD, EndD);
                                if (dCicles > 0)
                                {
                                    if (fNum.Equals("0"))
                                    {
                                        tEvent = DtInserMonthDates(3, ReqD, EndD, dCicles, true, true);
                                    }
                                    if (fNum.Equals("1") || fNum.Equals("2") || fNum.Equals("3") || fNum.Equals("4"))
                                    {
                                        dCicles = Math.Truncate((EndD - ReqD).TotalDays);
                                        DataTable tdaysQrt = new DataTable();
                                        tdaysQrt.Columns.Add("date", typeof(DateTime));
                                        DateTime qrt1 = new DateTime();
                                        DateTime qrt2 = new DateTime();
                                        DateTime qrt3 = new DateTime();
                                        DateTime qrt4 = new DateTime();

                                        if (Txt1Qrts.Value != null && !Txt1Qrts.Value.Equals(""))
                                        {
                                            qrt1 = DateTime.Parse(Txt1Qrts.Value.Trim());
                                            tdaysQrt.Rows.Add(qrt1);
                                        }

                                        if (Txt2Qrts.Value != null && !Txt2Qrts.Value.Equals(""))
                                        {
                                            qrt2 = DateTime.Parse(Txt2Qrts.Value.Trim());
                                            tdaysQrt.Rows.Add(qrt2);
                                        }

                                        if (Txt3Qrts.Value != null && !Txt3Qrts.Value.Equals(""))
                                        {
                                            qrt3 = DateTime.Parse(Txt3Qrts.Value.Trim());
                                            tdaysQrt.Rows.Add(qrt3);
                                        }

                                        if (Txt4Qrts.Value != null && !Txt4Qrts.Value.Equals(""))
                                        {
                                            qrt4 = DateTime.Parse(Txt4Qrts.Value.Trim());
                                            tdaysQrt.Rows.Add(qrt4);
                                        }
                                        if (tdaysQrt.Rows.Count > 0)
                                        {
                                            for (i = 0; i < dCicles; i++)
                                            {
                                                dateInsert = ReqD.AddDays(i);
                                                foreach (DataRow rDate in tdaysQrt.Rows)
                                                {
                                                    if (rDate.Field<DateTime>(0).Month == dateInsert.Month && rDate.Field<DateTime>(0).Day == dateInsert.Day)
                                                    {
                                                        x20MinsLater = dateInsert.AddMinutes(20);
                                                        tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                                        break;
                                                    }
                                                }
                                            }
                                            dateInsert = EndD;
                                            x20MinsLater = dateInsert.AddMinutes(20);
                                            tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                        }
                                    }
                                }
                            }

                            if (fOpc.Equals("D"))
                            {
                                iJump = int.Parse(fNum);
                                if (dCicles > 0)
                                {
                                    for (i = 0; i < dCicles; i += iJump)
                                    {
                                        dateInsert = ReqD.AddDays(i);
                                        x20MinsLater = dateInsert.AddMinutes(20);
                                        tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                    }
                                    dateInsert = EndD;
                                    x20MinsLater = dateInsert.AddMinutes(20);
                                    tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                }
                            }
                            if (fOpc.Equals("M"))
                            {
                                iJump = int.Parse(fNum);
                                if (dCicles > 0)
                                {
                                    switch (iJump)
                                    {
                                        case 1:
                                            for (i = 1; i < dCicles; i++)
                                            {
                                                dateInsert = ReqD.AddDays(i);
                                                if (dateInsert.Day == ReqD.Day)
                                                {
                                                    x20MinsLater = dateInsert.AddMinutes(20);
                                                    tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                                }
                                            }
                                            break;
                                        case 6:
                                            dCicles = GetMonths(ReqD, EndD);

                                            for (i = 6; i < dCicles; i += iJump)
                                            {
                                                dateInsert = ReqD.AddMonths(i);
                                                x20MinsLater = dateInsert.AddMinutes(20);
                                                tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                            }
                                            break;
                                        case 12:
                                            dCicles = GetDifferenceInYears(ReqD, EndD);
                                            for (i = 1; i <= dCicles; i++)
                                            {
                                                dateInsert = ReqD.AddYears(i);
                                                x20MinsLater = dateInsert.AddMinutes(20);
                                                tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    dateInsert = EndD;
                                    x20MinsLater = dateInsert.AddMinutes(20);
                                    tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                }
                            }
                            if (fOpc.Equals("W"))
                            {
                                if (dCicles > 0)
                                {
                                    List<int> days = new List<int>();
                                    if (ChkSunday.Checked) { days.Add(0); }
                                    if (ChkMonday.Checked) { days.Add(1); }
                                    if (ChkTuesday.Checked) { days.Add(2); }
                                    if (ChkWednesday.Checked) { days.Add(3); }
                                    if (ChkThursday.Checked) { days.Add(4); }
                                    if (ChkFriday.Checked) { days.Add(5); }
                                    if (ChkSaturday.Checked) { days.Add(6); }
                                    if (days.Count > 0)
                                    {
                                        for (i = 0; i <= dCicles; i++)
                                        {
                                            dateInsert = ReqD.AddDays(i);

                                            foreach (int doW in days)
                                            {
                                                if (doW == (int)dateInsert.DayOfWeek)
                                                {
                                                    x20MinsLater = dateInsert.AddMinutes(20);
                                                    tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    dateInsert = EndD;
                                    x20MinsLater = dateInsert.AddMinutes(20);
                                    tEvent.Rows.Add("", dateInsert, x20MinsLater);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return tEvent;
        }

        private DataTable DtInserMonthDates(int iJump, DateTime reqD, DateTime endD, double dCicles, Boolean equal, Boolean addEndD)
        {
            DataTable dtR = new DataTable();
            dtR.Columns.Add("Name", typeof(string));
            dtR.Columns.Add("Start", typeof(DateTime));
            dtR.Columns.Add("End", typeof(DateTime));
            DateTime x20MinsLater;
            DateTime dateInsert;
            int i = 0;
            try
            {
                if (equal)
                {
                    for (i = iJump; i <= dCicles; i += iJump)
                    {
                        dateInsert = reqD.AddMonths(i);
                        x20MinsLater = dateInsert.AddMinutes(20);
                        dtR.Rows.Add("", dateInsert, x20MinsLater);
                    }
                }
                else
                {
                    for (i = iJump; i < dCicles; i += iJump)
                    {
                        dateInsert = reqD.AddMonths(i);
                        x20MinsLater = dateInsert.AddMinutes(20);
                        dtR.Rows.Add("", dateInsert, x20MinsLater);
                    }
                }

                if (addEndD)
                {
                    dateInsert = endD;
                    x20MinsLater = dateInsert.AddMinutes(20);
                    dtR.Rows.Add("", dateInsert, x20MinsLater);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                return dtR;
                throw;
            }
            return dtR;
        }

        public int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            int years = endDate.Year - startDate.Year;

            if (startDate.Month == endDate.Month &&// if the start month and the end month are the same
                endDate.Day < startDate.Day// AND the end day is less than the start day
                || endDate.Month < startDate.Month)// OR if the end month is less than the start month
            {
                years--;
            }

            return years;
        }

        public int GetMonths(DateTime FROM_DATE, DateTime TO_DATE)
        {
            if (FROM_DATE > TO_DATE)
            {
                throw new Exception("Start Date is greater than the End Date");
            }
            int months = ((TO_DATE.Year * 12) + TO_DATE.Month) - ((FROM_DATE.Year * 12) + FROM_DATE.Month);
            if (TO_DATE.Day >= FROM_DATE.Day)
            {
                months++;
            }
            return months;
        }

        protected void DdFdIdFrequency_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int opc = int.Parse(ddFdIdFrequency.SelectedItem.Value);

                if (opc == 5)
                {
                    divDOW.Attributes["class"] = "collapse in";
                    RecalculaFechas();
                }
                else if (opc >= 8 && opc <= 12)
                {
                    divQrts.Attributes["class"] = "collapse in";
                    if (opc == 8)
                    {
                        ClrQrts(99);
                    }
                    if (opc == 9)
                    {
                        Txt2Qrts.Value = "";
                        Txt3Qrts.Value = "";
                        Txt4Qrts.Value = "";
                        ClrQrts(1);
                    }
                    if (opc == 10)
                    {
                        Txt1Qrts.Value = "";
                        Txt3Qrts.Value = "";
                        Txt4Qrts.Value = "";
                        ClrQrts(2);
                    }
                    if (opc == 11)
                    {
                        Txt1Qrts.Value = "";
                        Txt2Qrts.Value = "";
                        Txt4Qrts.Value = "";
                        ClrQrts(3);
                    }
                    if (opc == 12)
                    {
                        Txt1Qrts.Value = "";
                        Txt2Qrts.Value = "";
                        Txt3Qrts.Value = "";
                        ClrQrts(4);
                    }
                    RecalculaFechas();
                }
                else
                {
                    divDOW.Attributes["class"] = "collapse";
                    divQrts.Attributes["class"] = "collapse";
                    RecalculaFechas();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private void ClrQrts(int iSw)
        {
            try
            {
                switch (iSw)
                {
                    case 1:
                        div1HQrts.Attributes["class"] = "collapse in";
                        div2HQrts.Attributes["class"] = "collapse";
                        div3HQrts.Attributes["class"] = "collapse";
                        div4HQrts.Attributes["class"] = "collapse";

                        div1Qrts.Attributes["class"] = "collapse in";
                        div2Qrts.Attributes["class"] = "collapse";
                        div3Qrts.Attributes["class"] = "collapse";
                        div4Qrts.Attributes["class"] = "collapse";
                        break;
                    case 2:
                        div1HQrts.Attributes["class"] = "collapse";
                        div2HQrts.Attributes["class"] = "collapse in";
                        div3HQrts.Attributes["class"] = "collapse";
                        div4HQrts.Attributes["class"] = "collapse";

                        div1Qrts.Attributes["class"] = "collapse";
                        div2Qrts.Attributes["class"] = "collapse in";
                        div3Qrts.Attributes["class"] = "collapse";
                        div4Qrts.Attributes["class"] = "collapse";
                        break;
                    case 3:
                        div1HQrts.Attributes["class"] = "collapse";
                        div2HQrts.Attributes["class"] = "collapse";
                        div3HQrts.Attributes["class"] = "collapse in";
                        div4HQrts.Attributes["class"] = "collapse";

                        div1Qrts.Attributes["class"] = "collapse";
                        div2Qrts.Attributes["class"] = "collapse";
                        div3Qrts.Attributes["class"] = "collapse in";
                        div4Qrts.Attributes["class"] = "collapse";
                        break;
                    case 4:
                        div1HQrts.Attributes["class"] = "collapse";
                        div2HQrts.Attributes["class"] = "collapse";
                        div3HQrts.Attributes["class"] = "collapse";
                        div4HQrts.Attributes["class"] = "collapse in";

                        div1Qrts.Attributes["class"] = "collapse";
                        div2Qrts.Attributes["class"] = "collapse";
                        div3Qrts.Attributes["class"] = "collapse";
                        div4Qrts.Attributes["class"] = "collapse in";
                        break;
                    default:
                        div1HQrts.Attributes["class"] = "collapse";
                        div2HQrts.Attributes["class"] = "collapse";
                        div3HQrts.Attributes["class"] = "collapse";
                        div4HQrts.Attributes["class"] = "collapse";

                        div1Qrts.Attributes["class"] = "collapse";
                        div2Qrts.Attributes["class"] = "collapse";
                        div3Qrts.Attributes["class"] = "collapse";
                        div4Qrts.Attributes["class"] = "collapse";
                        break;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        protected void BtnReminders_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(Session["SessReminders"] is DataTable dtReminders))
                {
                    LblRem.Text = "0";
                }
                else
                {
                    if (dtReminders.Rows.Count > 0)
                    {
                        LblRem.Text = dtReminders.Rows.Count.ToString();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}