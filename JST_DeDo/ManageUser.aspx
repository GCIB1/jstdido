﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageUser.aspx.cs" Inherits="JST_DeDo.ManageUser" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-3.2.1.js"></script>
    <script src="Scripts/bootstrap-multiselect.js"></script>
    <script src="Scripts/Bs3/bootstrap.min.js"></script>
    <script src="Scripts/Bs3/bootstrap-datetimepicker.js"></script>

    <link href="Styles/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/bootstrap-multiselect.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap-datetimepicker.min.css" rel="stylesheet" />
</head>
<body>
    <form id="frmManageUsr" runat="server">
        <div class="container">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="mb-3">User administration</h4>
                        <br />
                        <table class="table table-borderless" id="TblEmployees">
                            <tbody>
                                <tr>
                                    <td>
                                        <div id="divEmployee" runat="server" class="col-md-3 mb-3">
                                            <label for="DDEditUser">Select Employee: </label>
                                            <br />
                                            <asp:DropDownList ID="DDEditUser" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="DDEditUser_SelectedIndexChanged">
                                                <asp:ListItem Text="Select Employee" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-borderless" id="tblGen">
                            <thead>
                                <tr>
                                    <th>Id.
                                    </th>
                                    <th>Full Name.
                                    </th>
                                    <th>Phone Ext.
                                    </th>
                                    <th>Change Password.
                                    </th>
                                    <th>Status.
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ReadOnly="true" ID="TxtId" Text="" CssClass="form-control" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="TxtFullName" Text="" CssClass="form-control" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="TxtExt" Text="" CssClass="form-control" MaxLength="8" />
                                    </td>
                                    <td>
                                        <input class="form-control" type="password" id="TxtPassw" runat="server" value="" placeholder="*****************" />
                                        <input type="checkbox" onclick="showP();"/>Show Password
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DdEnabled" runat="server" AutoPostBack="False" CssClass="form-control">
                                            <asp:ListItem Text="Enabled" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Disabled" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-borderless" id="tblCombos">
                            <thead>
                                <tr>
                                    <th>Profile.
                                    </th>
                                    <th>Department.
                                    </th>
                                    <th>Group.
                                    </th>
                                    <th>Manager.
                                    </th>
                                    <th>Dependent(s).
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div id="divCtrlProfile" runat="server">
                                            <asp:DropDownList ID="DdFdIdProfile" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="DdFdIdProfile_SelectedIndexChanged">
                                                <asp:ListItem Text="Select Profile" Value="1"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="divCtrlDepartment" runat="server" class="">
                                            <select runat="server" id="sDepartment" multiple="true" class="" size="8" style="width: 100px"></select>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="divCtrlGroup" runat="server" class="">
                                            <asp:DropDownList ID="DdGroup" runat="server" AutoPostBack="False" CssClass="form-control">
                                                <asp:ListItem Text="Select Group" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>

                                    <td>
                                        <div id="divCtrlManager" runat="server">
                                            <asp:DropDownList ID="DdManager" runat="server" AutoPostBack="False" CssClass="form-control">
                                                <asp:ListItem Text="Select Manager" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>


                                    <td>
                                        <div id="divDependent" runat="server" class="">
                                            <select runat="server" id="sDependent" multiple="true" class="" size="8" style="width: 100px"></select>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-borderless" id="tblLogin">
                            <thead>
                                <tr>
                                    <th style="width:165px;">
                                        Last Login.
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:TextBox runat="server" ID="TxtLastLogin" Text="-" CssClass="form-control" Enabled="false" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>












                        <br />
                        <br />
                        <div style="vertical-align: middle; text-align: right">
                            <asp:Button ID="BtnCancel" runat="server" Text="Close" CssClass="btn btn-danger btn-xs" />
                            &nbsp;|&nbsp;
                            <asp:Button ID="BtnSaveAdmin" Text="Save" runat="server" CssClass="btn btn-primary btn-xs" OnClick="BtnSaveAdmin_Click"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $(document).ready(function () {
            $('#sDepartment').multiselect();
        });

        $(document).ready(function () {
            $('#sDependent').multiselect();
        });

        function showP() {
          var x = document.getElementById("TxtPassw");
          if (x.type === "password") {
            x.type = "text";
          } else {
            x.type = "password";
          }
        }
    </script>
</body>
</html>
