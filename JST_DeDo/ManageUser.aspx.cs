﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace JST_DeDo
{
    public partial class ManageUser : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    PopulateControls();
                }
            }
            
        }

        private void PopulateControls()
        {
            try
            {
                DropDownList ddGroup = (DropDownList)divCtrlGroup.FindControl("DdGroup");
                BindGroup(ddGroup, PopulateGroup());

                DropDownList ddAdmEmp = (DropDownList)divEmployee.FindControl("DDEditUser");
                BindAdminEmployees(ddAdmEmp, PopulateEmployees());
                DropDownList ddP = (DropDownList)divCtrlProfile.FindControl("DdFdIdProfile");
                BindProfiles(ddP, PopulateProfiles());
                BindListDeparments(PopulateDepartments());
                BindListDependents(PopulateDependents());
                PopulateSuperV();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void PopulateSuperV()
        {
            try
            {
                using (FdiaryEntities de = new FdiaryEntities())
                {
                    var Usuper = de.Employees.Join(de.FdLogins, emp => emp.EmployeeID, log => log.LEmpID, (emp, log) => new { Emp = emp, Log = log })
                    .Where(EmpAndLog => EmpAndLog.Log.LProfile == "4")
                    .Select(id => id.Emp.EmployeeName)
                    .ToList();
                    if (Usuper != null)
                    {
                        DdManager.Items.Clear();
                        DdManager.DataSource = Usuper;
                        DdManager.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void BindListDeparments(List<Department> departments)
        {
            sDepartment.Items.Clear();
            sDepartment.DataTextField = "DepartmentName";
            sDepartment.DataValueField = "DepartmentID";
            sDepartment.DataSource = departments;
            sDepartment.DataBind();
        }

        private void BindListDependents(List<Employee> dependents)
        {
            sDependent.Items.Clear();
            sDependent.DataTextField = "EmployeeName";
            sDependent.DataValueField = "EmployeeID";
            sDependent.DataSource = dependents;
            sDependent.DataBind();
        }

        private List<Employee> PopulateDependents()
        {
            using (FdiaryEntities emp = new FdiaryEntities())
            {
                return emp.Employees.Where(e=> e.Status == 1).OrderBy(em => em.EmployeeName).ToList();
            }
        }

        private List<Department> PopulateDepartments()
        {
            using (FdiaryEntities dep = new FdiaryEntities())
            {
                return dep.Departments.OrderBy(d => d.DepartmentName).ToList();
            }
        }

        private List<FdProfile> PopulateProfiles()
        {
            using (FdiaryEntities de = new FdiaryEntities())
            {
                return de.FdProfiles.OrderBy(p => p.PName).ToList();
            }
        }

        private List<FdGroup> PopulateGroup()
        {
            using (FdiaryEntities de = new FdiaryEntities())
            {
                return de.FdGroups.Where(g => g.GrStatus == 1).OrderBy(g => g.GrOrder).ToList();
            }
        }

        private List<Employee> PopulateEmployees()
        {
            using (FdiaryEntities de = new FdiaryEntities())
            {
                return de.Employees.OrderBy(e => e.EmployeeName).ToList();
            }
        }

        private void BindProfiles(DropDownList DdFdIdProfile, List<FdProfile> profiles)
        {
            DdFdIdProfile.Items.Clear();
            DdFdIdProfile.AppendDataBoundItems = true;
            DdFdIdProfile.DataTextField = "PName";
            DdFdIdProfile.DataValueField = "ProfileID";
            DdFdIdProfile.DataSource = profiles;
            DdFdIdProfile.DataBind();
        }


        private void BindGroup(DropDownList ddgroup, List<FdGroup> groups)
        {
            ddgroup.Items.Clear();
            ddgroup.AppendDataBoundItems = true;
            ddgroup.DataTextField = "GrName";
            ddgroup.DataValueField = "GrId";
            ddgroup.DataSource = groups;
            ddgroup.DataBind();
        }

        private void BindAdminEmployees(DropDownList ddAdemp, List<Employee> employee)
        {
            ddAdemp.Items.Clear();
            ddAdemp.AppendDataBoundItems = true;
            ddAdemp.DataTextField = "EmployeeName";
            ddAdemp.DataValueField = "EmployeeID";
            ddAdemp.DataSource = employee;
            ddAdemp.DataBind();
        }

        private void BindEmployees(DropDownList ddFdIdusuario, List<Employee> employee)
        {
            ddFdIdusuario.Items.Clear();
            ddFdIdusuario.AppendDataBoundItems = true;
            ddFdIdusuario.DataTextField = "EmployeeName";
            ddFdIdusuario.DataValueField = "EmployeeID";
            ddFdIdusuario.DataSource = employee;
            ddFdIdusuario.DataBind();
        }

        private void SelectedOptions(HtmlSelect htmlSelect, string strSelected)
        {
            try
            {
                string[] arrSelected = strSelected.Split(',');
                if (arrSelected.Length > 0)
                {
                    foreach (string ssel in arrSelected)
                    {
                        if (!ssel.Equals(""))
                        {
                            foreach (ListItem item in htmlSelect.Items)
                            {
                                if (ssel == item.Value)
                                {
                                    item.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                exc.ToString();
                throw;
            }
        }

        protected void BtnSaveAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtFullName.Text != "")
                {
                    string strDepartment = Utils.Util.SelectToString((HtmlSelect)divCtrlDepartment.FindControl("sDepartment"));
                    string strDependent = Utils.Util.SelectToString((HtmlSelect)divCtrlDepartment.FindControl("sDependent"));

                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        int idSuper = 0;
                        try
                        {
                            var superv = de.Employees.Where(em => em.EmployeeName == DdManager.SelectedValue).FirstOrDefault();
                            if (superv != null)
                            {
                                idSuper = superv.EmployeeID;
                            }
                        }
                        catch (Exception)
                        {
                            idSuper = 0;
                        }



                        var idEmp = Convert.ToInt32(TxtId.Text);
                        var emp = de.Employees.Where(em => em.EmployeeID == idEmp).FirstOrDefault();
                        if (emp != null)
                        {
                            emp.EmployeeName = TxtFullName.Text;
                            emp.Status = Convert.ToInt32(DdEnabled.SelectedValue);
                        }


                        strDependent += "," + TxtId.Text;



                        var log = de.FdLogins.Where(l => l.LEmpID == idEmp).FirstOrDefault();
                        if (log != null)
                        {
                            log.LExt = TxtExt.Text;
                            log.LProfile = DdFdIdProfile.SelectedValue;
                            log.LDepartment = strDepartment;
                            log.LIdDependent = strDependent;
                            log.LidGroup = Convert.ToInt32(DdGroup.SelectedValue);
                            if (DdManager.Enabled)
                            {
                                if (idSuper > 0)
                                {
                                    log.LIdSuper = idSuper;
                                }
                            }
                            else
                            {
                                log.LIdSuper = -1;
                            }
                            if (!TxtPassw.Value.Equals(""))
                            {
                                log.LPassw = Utils.Util.Encrypt(TxtPassw.Value);
                            }
                        }
                        else    // NewLogin
                        {
                            var fdlogin = new FdLogin()
                            {
                                LEmpID = idEmp,
                                LEmailID = 0,
                                LCreateDate = DateTime.Today,
                                LProfile = DdFdIdProfile.SelectedValue,
                                LDepartment = strDepartment,
                                LExt = TxtExt.Text,
                                LIdSuper = idSuper,
                                LidGroup = Convert.ToInt32(DdGroup.SelectedValue)
                            };
                            if (!TxtPassw.Value.Equals(""))
                            {
                                fdlogin.LPassw = Utils.Util.Encrypt(TxtPassw.Value);
                            }
                            de.FdLogins.Add(fdlogin);
                        }

                        try
                        {
                            de.SaveChanges();
                        }
                        catch (Exception ex1)
                        {
                            ex1.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();

                throw;
            }

        }

        protected void DDEditUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindListDeparments(PopulateDepartments());
                BindListDependents(PopulateDependents());
                DropDownList ddEChange = (DropDownList)divEmployee.FindControl("ddEditUser");
                int idSuper = 0;
                if (!ddEChange.SelectedValue.Equals(""))
                {
                    int idUsr = Convert.ToInt32(ddEChange.SelectedValue);
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        var objEmp = de.Employees.Where(emp => emp.EmployeeID == idUsr).FirstOrDefault();
                        var objLoginUsr = de.FdLogins.Where(l => l.LEmpID == idUsr).FirstOrDefault();
                        if (objEmp != null)
                        {
                            TxtId.Text = objEmp.EmployeeID.ToString();
                            TxtFullName.Text = objEmp.EmployeeName.ToString();
                            if (objEmp.Status == 1)
                            {
                                //DdEnabled.SelectedValue = "Enabled";
                                DdEnabled.SelectedValue = "1";
                            }
                            else
                            {
                                //DdEnabled.SelectedValue = "Disabled";
                                DdEnabled.SelectedValue = "4";
                            }
                        }
                        if (objLoginUsr != null)
                        {
                            if (objLoginUsr.LExt != null)
                            {
                                TxtExt.Text = objLoginUsr.LExt.ToString();
                            }
                            else
                            {
                                TxtExt.Text = "-";
                            }
                            DdFdIdProfile.SelectedValue = objLoginUsr.LProfile.ToString();
                            SelectedOptions((HtmlSelect)divCtrlDepartment.FindControl("sDepartment"), objLoginUsr.LDepartment);

                            if (objLoginUsr.LIdDependent != null)
                            {
                                SelectedOptions((HtmlSelect)divDependent.FindControl("sDependent"), objLoginUsr.LIdDependent);
                            }

                            if (objLoginUsr.LIdSuper > 0)
                            {
                                idSuper = Convert.ToInt32(objLoginUsr.LIdSuper);

                                var objEmployees = de.Employees.Where(em => em.EmployeeID == idSuper).FirstOrDefault();
                                if (objEmployees != null)
                                {
                                    //DdManager.SelectedValue = objEmp.EmployeeName;
                                    DdManager.SelectedValue = objEmployees.EmployeeName;
                                }
                            }
                            TxtLastLogin.Text = String.Format("{0:MM/dd/yyyy hh:mm tt}", objLoginUsr.LLastLogin);
                        }
                        else
                        {
                            TxtExt.Text = "-";
                            DdFdIdProfile.SelectedIndex = -1;
                            DdManager.SelectedIndex = -1;
                            TxtLastLogin.Text = " - ";
                            //DdEnabled.SelectedIndex = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        protected void DdFdIdProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdFdIdProfile.SelectedValue == "4")
            {
                DdManager.SelectedIndex = -1;
                DdManager.Enabled = false;
            }
            else
            {
                DdManager.Enabled = true;
            }
        }

        //private String SelectToString(HtmlSelect htmlSelect)
        //{
        //    string r = "";
        //    try
        //    {
        //        if (htmlSelect.SelectedIndex > 0)
        //        {
        //            foreach (ListItem item in htmlSelect.Items)
        //            {
        //                if (item.Selected)
        //                {
        //                    r += item.Value + ',';
        //                }
        //            }
        //            r = r.TrimEnd(',');
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        r = "";
        //        throw;
        //    }
        //    return r;
        //}
    }
}