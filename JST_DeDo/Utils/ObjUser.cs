﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JST_DeDo.Utils
{
    public class ObjUser
    {
        public int UsrProfile { get; set; }
        public List<string> UsrDepartment { get; set; }
        public int UsrGroup { get; set; }
    }
}