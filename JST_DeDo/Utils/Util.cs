﻿using System;
using Microsoft.Exchange.WebServices.Data;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using JST_DeDo.Models;
using System.Diagnostics;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace JST_DeDo.Utils
{
    public class Util
    {
        private const int Keysize = 256;
        private const int DerivationIterations = 1000;

        public static List<Department> StrToListDeptartment(string str)
        {
            List<Department> Department = new List<Department>();
            string[] arrSelected;
            List<string> lResult = new List<string>();
            List<string> t = new List<string>();
            int idep = 0;
            try
            {
                if (!str.Equals("-"))
                {
                    arrSelected = str.Split(',');
                    if (arrSelected.Length > 0)
                    {
                        List<Department> dep = new List<Department>().ToList();

                        using (FdiaryEntities de = new FdiaryEntities())
                        {
                            dep = de.Departments.ToList();
                        }

                        if (dep != null)
                        {
                            foreach (string ssel in arrSelected)
                            {
                                if (!ssel.Equals(""))
                                {
                                    idep = Convert.ToInt32(ssel);
                                    var d = dep.Find(x => x.DepartmentID == idep);
                                    if (d != null)
                                    {
                                        Department.Add(new Department() { DepartmentName = d.DepartmentName, DepartmentID = d.DepartmentID, DepartmentEnabled = d.DepartmentEnabled });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                exc.ToString();
                throw;
            }
            return Department;
        }

        private static byte[] Generate256BitsOfRandomEntropy()
        {
            var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }

        public static string Decrypt(string cipherText)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
            // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            //using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
            using (var password = new Rfc2898DeriveBytes("D1D0", saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        public static string Encrypt(string plainText)
        {
            // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
            // so that the same Salt and IV values can be used when decrypting.  
            var saltStringBytes = Generate256BitsOfRandomEntropy();
            var ivStringBytes = Generate256BitsOfRandomEntropy();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new Rfc2898DeriveBytes("D1D0", saltStringBytes, DerivationIterations))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 256;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        public static void SendEmail(string mail, string p)
        {
            string _Subject = "";
            string _Body = "";

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1)
            {
                Credentials = new WebCredentials("notification@gcib.net", "123%Abcd"),
                UseDefaultCredentials = false
            };
            service.AutodiscoverUrl("notification@gcib.net", RedirectionUrlValidationCallback);
            EmailMessage email = new EmailMessage(service);

            email.ToRecipients.Add(mail);
            email.ToRecipients.Add("mperez@gcib.net"); //TODO Eliminar esta LINEA

            _Subject = "De Do Password Recovery";
            _Body = @"<html>
                        <body>
                            <h3>We have reset your password to access your account. These are your credentials to access:</h3>
                            <h3>Username:<strong> " + mail + @"</strong ></h3>
                            <h3>New password:</h3>
                            <h2><strong> " + p + @"</strong ></h2>
                            <p> please access the following Link:
                            <a title = 'Di Do' href = 'http://192.168.30.4:3003/ToDo.aspx' target = '_blank' rel = 'noopener'> http://192.168.30.4:3003/ToDo.aspx </a></p>
                            </body>
                        </html>";
            email.Subject = _Subject;
            email.Body = _Body;
            try
            {
                email.Send();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public static void SendNEWEmail(string mail, string p)
        {
            string _Subject = "";
            string _Body = "";

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1)
            {
                Credentials = new WebCredentials("notification@gcib.net", "123%Abcd"),
                UseDefaultCredentials = false
            };
            service.AutodiscoverUrl("notification@gcib.net", RedirectionUrlValidationCallback);
            EmailMessage email = new EmailMessage(service);

            email.ToRecipients.Add(mail);
            email.ToRecipients.Add("mperez@gcib.net"); //TODO Eliminar esta LINEA

            _Subject = "Access to new software for Claims Department.";
            _Body = @"<html>
                        <body>
                            <h2>Good Morning:</h2>
                            <h3>Please see the link below to access to new software for Claims Department.</h3>
                            <a title = 'Di Do' href = 'http://192.168.30.4:3003/ToDo.aspx' target = '_blank' rel = 'noopener'> http://192.168.30.4:3003/ToDo.aspx </a></p>
                            <h3>This is your credentials to access:</h3>
                            <h3>Username:<strong> " + mail + @"</strong ></h3>
                            <h3>Password:</h3>
                            <h2><strong> " + p + @"</strong ></h2>
                            <p> If you have any question, please ask me <a href = 'mailto:mperez@gcib.net?Subject=DeDoQuestion'> mperez@gcib.net </a> or give me a call to ext. 244:</p>
                            </body>
                        </html>";

              email.Subject = _Subject;
            email.Body = _Body;
            try
            {
                email.Send();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        //public static void MergeClaims()
        //{
        //    try
        //    {
        //        Process process = new Process();
        //        process.StartInfo.FileName = "cmd";
        //        //process.StartInfo.WorkingDirectory = @"C:\Users\mperez\source\repos\jstdido\MapSWDiDo\bin\Debug";
        //        process.StartInfo.WorkingDirectory = @"C:\Users\mperez\source\repos\jstdido\te\bin\Debug\";
        //        process.StartInfo.Arguments = "/c \"" + "MergeFHDeDo.exe" + "\"";
        //        process.Start();
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }
        //}

        public static string SelectToString(HtmlSelect htmlSelect)
        {
            string r = "";
            try
            {
                if (htmlSelect.SelectedIndex > 0)
                {
                    foreach (ListItem item in htmlSelect.Items)
                    {
                        if (item.Selected)
                        {
                            r += item.Value + ',';
                        }
                    }
                    r = r.TrimEnd(',');
                }
            }
            catch (Exception)
            {
                r = "";
                throw;
            }
            return r;
        }

        public static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }

        public static bool IsMailValid(string emailaddress)
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

    }
}