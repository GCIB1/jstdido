﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class VendorController : Controller
    {
        Models.FdiaryEntities db = new Models.FdiaryEntities();

        // GET: Vendor
        public ActionResult Index()
        {
            return View(db.FdVendors.OrderBy(v => v.VndOrder).ToList());
        }

        //// GET: Vendor/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Vendor/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vendor/Create
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                // TODO: Add insert logic here
                db.FdVendors.Add(new Models.FdVendor
                {
                    VndName = frm["VndName"].ToString(),
                    VndOrder = Convert.ToInt32(frm["VndOrder"].ToString()),
                    VndStatus = Convert.ToInt32(frm["VndStatus"].ToString())
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Vendor/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdVendors.Where(v => v.VndId == id).FirstOrDefault());
        }

        // POST: Vendor/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection frme)
        {
            try
            {
                var v = db.FdVendors.Where(vn => vn.VndId == id).FirstOrDefault();
                if (v != null)
                {
                    v.VndName = frme["VndName"].ToString();
                    v.VndOrder = Convert.ToInt32(frme["VndOrder"].ToString());
                    v.VndStatus = Convert.ToInt32(frme["VndStatus"].ToString());
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //// GET: Vendor/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Vendor/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
