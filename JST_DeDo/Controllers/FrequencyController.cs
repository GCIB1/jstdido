﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class FrequencyController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();
        // GET: Frequency
        public ActionResult Index()
        {
            return View(db.FdFrequencies.OrderBy(f => f.FFOrder).ToList());
        }

        // GET: Frequency/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Frequency/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Frequency/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                db.FdFrequencies.Add(new FdFrequency
                {
                    FFrequency = collection["FFrequency"].ToString(),
                    FFOrder = Convert.ToInt32(collection["FFOrder"].ToString()),
                    FFStatus = Convert.ToInt32(collection["FFStatus"].ToString())
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Frequency/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdFrequencies.Where(f => f.FId == id).FirstOrDefault());
        }

        // POST: Frequency/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var fr = db.FdFrequencies.Where(f => f.FId == id).FirstOrDefault();
                if (fr != null)
                {
                    fr.FFrequency = collection["FFrequency"].ToString();
                    fr.FFOrder = Convert.ToInt32(collection["FFOrder"].ToString());
                    fr.FFStatus = Convert.ToInt32(collection["FFStatus"].ToString());
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Frequency/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Frequency/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
