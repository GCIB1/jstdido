﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class GroupController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();
        // GET: Group
        public ActionResult Index()
        {
            return View( db.FdGroups.Where(g => g.GrId != 1).OrderBy(gs => gs.GrOrder).ToList());
        }

        // GET: Group/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }

        // GET: Group/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Group/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                db.FdGroups.Add(new FdGroup
                {
                    GrName = collection["GrName"].ToString(),
                    GrOrder = Convert.ToInt32(collection["GrOrder"].ToString()),
                    GrStatus = Convert.ToInt32(collection["GrStatus"].ToString())
                });
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Group/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdGroups.Where(g => g.GrId == id).FirstOrDefault());
        }

        // POST: Group/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var gr = db.FdGroups.Where(g => g.GrId == id).FirstOrDefault();
                if (gr != null)
                {
                    gr.GrName = collection["GrName"].ToString();
                    gr.GrOrder = Convert.ToInt32(collection["GrOrder"].ToString());
                    gr.GrStatus = Convert.ToInt32(collection["GrStatus"].ToString());
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Group/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Group/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
