﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class DepartmentController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();
        // GET: Department
        public ActionResult Index()
        {
            return View(db.Departments.OrderBy(d => d.DepartmentName).ToList());
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                db.Departments.Add(new Department
                {
                    DepartmentName = frm["DepartmentName"].ToString()
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Department/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.Departments.Where(d => d.DepartmentID == id).FirstOrDefault());
        }

        // POST: Department/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var de = db.Departments.Where(d => d.DepartmentID == id).FirstOrDefault();
                if (de != null)
                {
                    de.DepartmentName= collection["DepartmentName"].ToString();
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
