﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class StatusController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();

        // GET: Status
        public ActionResult Index()
        {
            return View(db.FdStatus.Where(s => s.StatusId != 4 && s.StatusId != 1).OrderBy(s => s.StatusOrder).ToList());
        }

        // GET: Status/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Status/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Status/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                db.FdStatus.Add(new FdStatu
                {
                    StatusName = collection["StatusName"].ToString(),
                    StatusOrder = Convert.ToInt32(collection["StatusOrder"].ToString()),
                    StatusActivo = Convert.ToInt32(collection["StatusActivo"].ToString())
                });
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Status/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdStatus.Where(s => s.StatusId == id).FirstOrDefault());
        }

        // POST: Status/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var st = db.FdStatus.Where(s => s.StatusId == id).FirstOrDefault();
                if (st != null)
                {
                    st.StatusName = collection["StatusName"].ToString();
                    st.StatusOrder = Convert.ToInt32(collection["StatusActivo"].ToString());
                    st.StatusActivo = Convert.ToInt32(collection["StatusOrder"].ToString());
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Status/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Status/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
