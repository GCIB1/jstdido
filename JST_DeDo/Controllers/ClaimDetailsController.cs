﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using JST_DeDo.Models;

namespace JST_DeDo.Controllers
{
    public class ClaimDetailsController : Controller
    {
        private FdiaryEntities db = new FdiaryEntities();

        // GET: ClaimDetails
        public ActionResult Index()
        {
            int idUsr = Session["SsEmpId"] == null ? 0 : Convert.ToInt32(Session["SsEmpId"].ToString());
            int idProfile = Session["SsProfile"] == null ? 0 : Convert.ToInt32(Session["SsProfile"].ToString());
            string sDependent = Session["SsDependet"] == null ? "" : Session["SsDependet"].ToString();

            if (idProfile >= 3)
            {
                return View(db.FdClaimDetails.OrderByDescending(o => o.claim_num).ToList());
            }
            else
            {
                if (idUsr != 0)
                {
                    if (!sDependent.Equals(""))
                    {
                        string[] arrDependent = sDependent.Split(',');
                        var ETLMap = (from Em in db.FdETLMappings
                                     where arrDependent.Contains(Em.RHId.ToString())
                                     select Em.FHUserId).ToList();

                        var ClaimDetail = (from c in db.FdClaimDetails
                                     where ETLMap.Contains(c.handler_id.ToString())
                                     select c).ToList();
                        return View(ClaimDetail.OrderByDescending(o => o.claim_num));
                    }
                    else
                    {
                        var usr = db.FdETLMappings.Where(u => u.RHId == idUsr).FirstOrDefault();
                        if (usr != null)
                        {
                            return View(db.FdClaimDetails.Where(c => c.handler_id == usr.FHUserId).OrderByDescending(o => o.claim_num).ToList());
                        }
                    }
                }
                    return View(db.FdClaimDetails.OrderByDescending(o => o.claim_num).ToList());
            }
        }

        // GET: ClaimDetails/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FdClaimDetail fdClaimDetail = db.FdClaimDetails.Find(id);
            if (fdClaimDetail == null)
            {
                return HttpNotFound();
            }
            return View(fdClaimDetail);
        }

        // GET: ClaimDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClaimDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "user_id,claim_id,log_datetime,examiner_name,claim_num,IntakeWIP_,open_close_status,LIA_PD_CARGO,Lia,Cargo,policy_num,Policy_Effective_Date,holder_state,controlling_state,NameINs_DBA,BrokerName,ClaimantName,Assigned_to_DC_Name,Assigned_to_DC_Date,sum_alae_amt,sum_loss_amt,sum_alae_amtNegative,sum_alae_amtPositive,sum_loss_amtNegative,sum_loss_amtPositive,PD,diff_day,diff_date,map_date,sync_date,sync_status,log_datetime_id,claim_task,FdRoundTableC,FdAssignedAccdReconC,FdPhoneCallsInC,FdPhoneCallsOutC,FdEmailsInC,FdEmailsOutC,FdRemarksC,handler_id,touch_user_id,touch_claim_name")] FdClaimDetail fdClaimDetail)
        {
            if (ModelState.IsValid)
            {
                db.FdClaimDetails.Add(fdClaimDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fdClaimDetail);
        }

        // GET: ClaimDetails/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FdClaimDetail fdClaimDetail = db.FdClaimDetails.Find(id);
            if (fdClaimDetail == null)
            {
                return HttpNotFound();
            }
            return View(fdClaimDetail);
        }

        // POST: ClaimDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "user_id,claim_id,log_datetime,examiner_name,claim_num,IntakeWIP_,open_close_status,LIA_PD_CARGO,Lia,Cargo,policy_num,Policy_Effective_Date,holder_state,controlling_state,NameINs_DBA,BrokerName,ClaimantName,Assigned_to_DC_Name,Assigned_to_DC_Date,sum_alae_amt,sum_loss_amt,sum_alae_amtNegative,sum_alae_amtPositive,sum_loss_amtNegative,sum_loss_amtPositive,PD,diff_day,diff_date,map_date,sync_date,sync_status,log_datetime_id,claim_task,FdRoundTableC,FdAssignedAccdReconC,FdPhoneCallsInC,FdPhoneCallsOutC,FdEmailsInC,FdEmailsOutC,FdRemarksC,handler_id,touch_user_id,touch_claim_name")] FdClaimDetail fdClaimDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fdClaimDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fdClaimDetail);
        }

        // GET: ClaimDetails/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FdClaimDetail fdClaimDetail = db.FdClaimDetails.Find(id);
            if (fdClaimDetail == null)
            {
                return HttpNotFound();
            }
            return View(fdClaimDetail);
        }

        // POST: ClaimDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            FdClaimDetail fdClaimDetail = db.FdClaimDetails.Find(id);
            db.FdClaimDetails.Remove(fdClaimDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
