﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace JST_DeDo.Controllers
{
    public class CategoryController : Controller
    {
        Models.FdiaryEntities db = new Models.FdiaryEntities();
        // GET: Category
        public ActionResult Index()
        {
            return View(db.FdCategories.OrderBy(c=> c.CatOrder).ToList());
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                db.FdCategories.Add(new FdCategory
                {
                    CatName = frm["CatName"].ToString(),
                    CatOrder = Convert.ToInt32(frm["CatOrder"].ToString()),
                    CatStatus = Convert.ToInt32(frm["CatStatus"].ToString())
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdCategories.Where(c => c.CatId == id).FirstOrDefault());
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection frme)
        {
            try
            {
                var cat = db.FdCategories.Where(c => c.CatId == id).FirstOrDefault();
                if (cat != null)
                {
                    cat.CatName = frme["CatName"].ToString();
                    cat.CatOrder = Convert.ToInt32(frme["CatOrder"].ToString());
                    cat.CatStatus = Convert.ToInt32(frme["CatStatus"].ToString());
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Category/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
