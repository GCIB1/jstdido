﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class TimeZoneController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();
        // GET: TimeZone
        public ActionResult Index()
        {
            return View(db.FdTimeZones.Where(t => t.TzId != 0).OrderBy(t => t.TzOrder).ToList());
        }

        // GET: TimeZone/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TimeZone/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TimeZone/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                db.FdTimeZones.Add(new FdTimeZone
                {

                    TzName = collection["TzName"].ToString(),
                    TzFullName = collection["TzFullName"].ToString(),
                    TzOrder = Convert.ToInt32(collection["TzOrder"].ToString()),
                    TzActive= Convert.ToInt32(collection["TzActive"].ToString())
                });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TimeZone/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdTimeZones.Where(t => t.TzId == id).FirstOrDefault());
        }

        // POST: TimeZone/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var t = db.FdTimeZones.Where(tz => tz.TzId == id).FirstOrDefault();
                if (t != null)
                {
                    t.TzName = collection["TzName"].ToString();
                    t.TzFullName = collection["TzFullName"].ToString();
                    t.TzOrder = Convert.ToInt32(collection["TzOrder"].ToString());
                    t.TzActive = Convert.ToInt32(collection["TzActive"].ToString());
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TimeZone/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TimeZone/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
