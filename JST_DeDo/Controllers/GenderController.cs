﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class GenderController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();

        // GET: Gender
        public ActionResult Index()
        {
            return View(db.FdGenders.OrderBy(g=> g.GenOrder).ToList());
        }

        // GET: Gender/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Gender/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Gender/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                db.FdGenders.Add(new FdGender
                {
                    GenName = collection["GenName"].ToString(),
                    GenOrder = Convert.ToInt32(collection["GenOrder"].ToString()),
                    GenStatus = Convert.ToInt32(collection["GenStatus"].ToString())
                });
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gender/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.FdGenders.Where(g => g.GenId == id).FirstOrDefault());
        }

        // POST: Gender/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var gen = db.FdGenders.Where(g => g.GenId == id).FirstOrDefault();
                if (gen != null)
                {
                    gen.GenName = collection["GenName"].ToString();
                    gen.GenOrder = Convert.ToInt32(collection["GenOrder"].ToString());
                    gen.GenStatus = Convert.ToInt32(collection["GenStatus"].ToString());
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gender/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Gender/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
