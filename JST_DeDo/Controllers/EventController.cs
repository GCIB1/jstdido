﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JST_DeDo.Controllers
{
    public class EventController : Controller
    {
        FdiaryEntities db = new FdiaryEntities();
        // GET: Event
        public ActionResult Index()
        {
            List<Event> ev = new List<Event>();
            var frecuency = db.FdFrequencies.ToList();
            ev = db.Events.OrderByDescending(e => e.idFd).OrderBy(e => e.event_id).ToList();

            if (ev.Count > 0)
            {
                //foreach (var e in ev)
                //{
                //    foreach (var fre in frecuency)
                //    {
                //        if (e.Fdiary.FdIdFrequency == fre.FId)
                //        {
                //            e.FdFrequencyName = fre.FFrequency;
                //            break;
                //        }
                //    }
                //}
            }
            return View(ev.ToList());
        }

        // GET: Event/Details/5
        public ActionResult Details(int id)
        {
            List<Event> ev = new List<Event>();
            var frecuency = db.FdFrequencies.ToList();
            ev = db.Events.Where(e => e.idFd == id).ToList();

            if (ev.Count > 0)
            {
                //foreach (var e in ev)
                //{
                //    foreach (var fre in frecuency)
                //    {
                //        if (e.Fdiary.FdIdFrequency == fre.FId)
                //        {
                //            e.FdFrequencyName = fre.FFrequency;
                //            break;
                //        }
                //    }
                //}

            }
            return View(ev.ToList());
        }

        // GET: Event/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Event/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Event/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Event/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Event/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Event/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
