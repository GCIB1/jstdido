﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Reflection;
using System.Text;

namespace JST_DeDo
{
    public partial class JST : Page
    {
        private bool BClaims = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    if (Session["SsDepartments"] != null)
                    {
                        List<Department> d = (List<Department>)Session["SsDepartments"];
                        if (d != null)
                        {
                            var dep = d.Find(x => x.DepartmentID == 2);
                            if (dep != null)
                            {
                                BClaims = true;
                            }
                        }
                        TxtClaimsToDo.Value = BClaims.ToString();
                    }
                    PopulateFDiaries();
                }
            }
        }

        public HtmlControl Iframe => ifNewReg;

        private void PopulateFDiaries()
        {
            try
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    int idUsr = Session["SsEmpId"] == null ? 0 : Convert.ToInt32(Session["SsEmpId"].ToString());
                    int idProfile = Session["SsProfile"] == null ? 2 : Convert.ToInt32(Session["SsProfile"].ToString());
                    List<Fdiary> allDiaries = null;
                    bool bDeleted = false;
                    bool bDone = true;

                    //TODO
                    BtnReminders.Text = "Reminders(999)";
                    try
                    {
                        bDeleted = chkShowAll.Checked;
                    }
                    catch (Exception)
                    {
                        bDeleted = false;
                        throw;
                    }

                    try
                    {
                        bDone = ChkHideDone.Checked;
                    }
                    catch (Exception)
                    {
                        bDone = true;
                        throw;
                    }


                    using (FdiaryEntities FdiaryEnt = new FdiaryEntities())
                    {
                        List<Fdiary> diaries = new List<Fdiary>();
                        var deptos = FdiaryEnt.Departments.ToList();
                        var employees = FdiaryEnt.Employees.ToList();
                        var status = FdiaryEnt.FdStatus.ToList();
                        var frecuency = FdiaryEnt.FdFrequencies.ToList();
                        var category = FdiaryEnt.FdCategories.ToList();
                        var gender = FdiaryEnt.FdGenders.ToList();
                        var timeZ = FdiaryEnt.FdTimeZones.ToList();
                        var events = FdiaryEnt.Events.Select(ev => ev.idFd).Distinct();

                        try
                        {
                            if (employees != null)
                            {
                                var t = employees.Where(e => e.EmployeeID == idUsr).FirstOrDefault();
                                if (t != null)
                                {
                                    LblUsr.Text = "Hello: [  " + t.EmployeeName + "  ] ";
                                }
                            }
                        }
                        catch (Exception)
                        {
                            LblUsr.Text = "USR.ERR";
                            throw;
                        }


                        if (idProfile >= 4)
                        {
                            idUsr = 0;
                        }

                        if (idUsr > 0)
                        {
                            if (bDeleted)
                            {
                                if (bDone)
                                {
                                    diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 2 && fd.FdIdusuario.Contains(idUsr.ToString())).OrderBy(fd => fd.FdId).ToList();
                                }
                                else
                                {
                                    diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdIdusuario.Contains(idUsr.ToString())).OrderBy(fd => fd.FdId).ToList();
                                }
                            }
                            else
                            {
                                if (bDone)
                                {
                                    diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 3 && fd.FdStatus != 2 && fd.FdIdusuario.Contains(idUsr.ToString())).OrderBy(fd => fd.FdId).ToList();
                                }
                                else
                                {
                                    diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 3 && fd.FdIdusuario.Contains(idUsr.ToString())).OrderBy(fd => fd.FdId).ToList();
                                }
                            }
                        }
                        else
                        {
                            if (bDeleted)
                            {
                                if (bDone)
                                {
                                    //diaries = FdiaryEnt.Fdiaries.OrderByDescending(fd => fd.FdStatus != 2).ToList();
                                    diaries = FdiaryEnt.Fdiaries.OrderBy(fd => fd.FdId).ToList();
                                }
                                else
                                {
                                    //diaries = FdiaryEnt.Fdiaries.OrderByDescending(fd => fd.FdId).ToList();
                                    diaries = FdiaryEnt.Fdiaries.OrderBy(fd => fd.FdId).ToList();
                                }

                            }
                            else
                            {
                                if (bDone)
                                {
                                    //diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 3 && fd.FdStatus != 2).OrderByDescending(fd => fd.FdId).ToList();
                                    diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 3 && fd.FdStatus != 2).OrderBy(fd => fd.FdId).ToList();
                                }
                                else
                                {
                                    //diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 3).OrderByDescending(fd => fd.FdId).ToList();
                                    diaries = FdiaryEnt.Fdiaries.Where(fd => fd.FdStatus != 3).OrderBy(fd => fd.FdId).ToList();
                                }

                            }
                        }

                        if (diaries.Count > 0)
                        {
                            allDiaries = new List<Fdiary>();
                            foreach (var a in diaries)
                            {
                                Fdiary d = a;
                                d.FdId = a.FdId;
                                d.FdIdDept = a.FdIdDept;
                                d.FdIdusuario = a.FdIdusuario;
                                d.FdReqDate = a.FdReqDate;
                                d.FdstrFdReqDate = string.Format("{0:MM/dd/yyyy hh:mm tt}", a.FdReqDate);
                                d.FdFinishDate = a.FdFinishDate;
                                d.FdstrFinishDate = string.Format("{0:MM/dd/yyyy hh:mm tt}", a.FdFinishDate);
                                d.FdFollow = a.FdFollow;
                                d.FdstrFollow = string.Format("{0:MM/dd/yyyy hh:mm tt}", a.FdFollow);
                                d.FdSubject = a.FdSubject;
                                d.FdStatus = a.FdStatus;
                                d.FdIdTz = a.FdIdTz;
                                foreach (var st in status)
                                {
                                    if (d.FdStatus == st.StatusId)
                                    {
                                        d.FdstatusName = st.StatusName;
                                        break;
                                    }
                                }

                                d.FdReKey = a.FdReKey;

                                if (!a.FdIdDept.Equals(""))
                                {
                                    d.FdDeptoName = DeptostoString(a.FdIdDept, deptos);
                                }
                                if (!a.FdIdusuario.Equals(""))
                                {
                                    d.FdEmployyeName = EmployeetoString(a.FdIdusuario, employees);
                                }

                                foreach (var gen in gender)
                                {
                                    if (d.FdIdGender == gen.GenId)
                                    {
                                        d.FdGenderName = gen.GenName;
                                        break;
                                    }
                                }
                                foreach (var fre in frecuency)
                                {
                                    if (d.FdIdFrequency == fre.FId)
                                    {
                                        d.FdFrequencyName = fre.FFrequency;
                                        break;
                                    }
                                }
                                foreach (var cat in category)
                                {
                                    if (d.FdIdCategory == cat.CatId)
                                    {
                                        d.FdCategoryName = cat.CatName;
                                        break;
                                    }
                                }
                                foreach (var tz in timeZ)
                                {
                                    if (d.FdIdTz == tz.TzId)
                                    {
                                        d.FdTimeZoneName = tz.TzName;
                                        break;
                                    }
                                }
                                if (!events.Any())
                                {
                                    d.FdReKey = "N";
                                }
                                else
                                {
                                    foreach (var e in events)
                                    {
                                        if (d.FdId == e)
                                        {
                                            d.FdReKey = "D";
                                            break;
                                        }
                                        else
                                        {
                                            d.FdReKey = "N";
                                        }
                                    }
                                }
                                allDiaries.Add(d);
                            }
                            if (TxtClaimsToDo.Value.Equals("True"))
                            {
                                var diaryFilter = allDiaries.Select(d => new FilterDiariesClaims()
                                {
                                    IdStatus = Convert.ToInt32(d.FdStatus),
                                    DetalsRem = d.FdReKey,
                                    Id = d.FdId,
                                    Category = d.FdCategoryName,
                                    Department = d.FdDeptoName,
                                    Gender = d.FdGenderName,
                                    Employee = d.FdEmployyeName,
                                    TimeZone = d.FdTimeZoneName,
                                    Recived = Convert.ToInt32(d.FdRecivedC),
                                    Closed = Convert.ToInt32(d.FdClosedC),
                                    Pending_Days = Convert.ToInt32(d.FdPendingC),
                                    Pending30_Days = Convert.ToInt32(d.FdPending30C),
                                    Pending60_Days = Convert.ToInt32(d.FdPending60C),
                                    Pending90_Day = Convert.ToInt32(d.FdPending90C),

                                    PhoneCallsIn = Convert.ToInt32(d.FdPhoneCallsInC),
                                    PhoneCallsOut = Convert.ToInt32(d.FdPhoneCallsOutC),
                                    EmailsIn = Convert.ToInt32(d.FdEmailsInC),
                                    EmailsOut = Convert.ToInt32(d.FdEmailsOutC),

                                    LossRuns = Convert.ToInt32(d.FdLRunC ?? 0),
                                    FNOL = Convert.ToInt32(d.FdFNOLC ?? 0),
                                    Clerance = Convert.ToInt32(d.FdCleranceC ?? 0),

                                    LossRun_Clearance = d.FdLostRunC,
                                    Frequency = d.FdFrequencyName,
                                    Begin_Date = d.FdstrFdReqDate,
                                    End_Date = d.FdstrFinishDate,
                                    Follow_UpReview = d.FdstrFollow,
                                    Subject = Server.HtmlDecode(d.FdSubject),
                                    Status = d.FdstatusName
                                }).ToList();

                                PopulateDataTableClaims(ToDataTable(diaryFilter));
                                PopulateChekersClaims(diaryFilter.ToList());
                                //BtnClaims.Visible = false;

                            }
                            else
                            {
                                var diaryFilter = allDiaries.Select(d => new FilterDiaries()
                                {
                                    IdStatus = Convert.ToInt32(d.FdStatus),
                                    DetalsRem = d.FdReKey,
                                    Id = d.FdId,
                                    Category = d.FdCategoryName,
                                    Department = d.FdDeptoName,
                                    Gender = d.FdGenderName,
                                    Employee = d.FdEmployyeName,
                                    TimeZone = d.FdTimeZoneName,
                                    Frequency = d.FdFrequencyName,
                                    Begin_Date = d.FdstrFdReqDate,
                                    End_Date = d.FdstrFinishDate,
                                    Follow_UpReview = d.FdstrFollow,
                                    Subject = Server.HtmlDecode(d.FdSubject),
                                    Status = d.FdstatusName
                                }).ToList();

                                PopulateDataTable(ToDataTable(diaryFilter));
                                PopulateChekers(diaryFilter.ToList());
                                //BtnClaims.Visible = false;
                            }
                        }
                    }
                    GetNotifications();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        protected void ChkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            PopulateFDiaries();
        }

        private string DeptostoString(string str, List<Department> deptos)
        {
            string r = "";
            try
            {
                string[] arrDept = str.Split(',');
                if (arrDept.Length > 0)
                {
                    foreach (string pos in arrDept)
                    {
                        if (!pos.Equals(""))
                        {
                            foreach (var dep in deptos)
                            {
                                if (Convert.ToInt32(pos) == dep.DepartmentID)
                                {
                                    r += dep.DepartmentName + ',';
                                    break;
                                }
                            }
                        }
                    }
                    if (r.Length > 0)
                    {
                        r = r.TrimEnd(',');
                    }
                }
            }
            catch (Exception)
            {
                r = "";
                throw;
            }
            return r;
        }

        private string EmployeetoString(String str, List<Employee> employees)
        {
            string r = "";
            try
            {
                string[] arrEmp = str.Split(',');
                if (arrEmp.Length > 0)
                {
                    foreach (string pos in arrEmp)
                    {
                        if (!pos.Equals(""))
                        {
                            foreach (var empl in employees)
                            {
                                if (Convert.ToInt32(pos) == empl.EmployeeID)
                                {
                                    r += empl.EmployeeName + ',';
                                    break;
                                }
                            }
                        }
                    }
                    if (r.Length > 0)
                    {
                        r = r.TrimEnd(',');
                    }
                }
            }
            catch (Exception)
            {
                r = "";
                throw;
            }
            return r;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /*Verifies that the control is rendered */
        }

        protected void ChkHideDone_CheckedChanged(object sender, EventArgs e)
        {
            PopulateFDiaries();
        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            PopulateFDiaries();
        }

        protected void BtnClaims_Click(object sender, EventArgs e)
        {
            try
            {
                //Utils.Util.MergeClaims();

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        private DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        private void PopulateDataTable(DataTable dt)
        {
            int s = 0;
            StringBuilder html = new StringBuilder();

            string strVal = "";
            html.Append("<table id='example' class='table table-striped table-bordered' style='width:100%; font-size:x-small'>");

            html.Append("<thead>");

            html.Append("<tr>");
            foreach (DataColumn column in dt.Columns)
            {
                if (column.Caption.ToString().Equals("IdStatus") || column.Caption.ToString().Equals("DetalsRem"))
                {
                    html.Append("<th>");
                    html.Append("</th>");
                }
                else
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
            }
            html.Append("</tr>");
            html.Append("</thead>");

            html.Append("<tbody>");

            //Building the Data rows.
            foreach (DataRow row in dt.Rows)
            {
                try { s = Convert.ToInt32(row[0].ToString()); } catch (Exception) { s = 0; }

                switch (s)
                {
                    case 2:
                        html.Append("<tr style='background-color: orange; color: white; text-decoration-line: line-through;'>");
                        break;
                    case 3:
                        html.Append("<tr style='text-decoration-line: line-through;'>");
                        break;
                    case 5:
                        html.Append("<tr style='background-color: Maroon; color: white;'>");
                        break;
                    default:
                        html.Append("<tr>");
                        break;
                }

                foreach (DataColumn column in dt.Columns)
                {
                    strVal = "";
                    strVal = row[column.ColumnName].ToString();
                    if (column.Caption.ToString().Equals("IdStatus"))
                    {
                        strVal = "<button id='btnUpd' data-toggle='modal' data-target='#NewReg' class='btn btn-default btn-xs' onclick='document.getElementById(&#39;ifNewReg&#39;).src=&#39;Create.aspx?i=" + row[2] + "&#39;;return false;'>Edit</button>";
                    }
                    if (column.Caption.ToString().Equals("DetalsRem"))
                    {
                        if (strVal.Equals("D"))
                        {
                            strVal = "<a class='btn btn-warning btn-xs' href=Detail.aspx?e=" + row[2] + ">Detail</a>";
                        }
                        else
                        {
                            strVal = "";
                        }

                    }
                    html.Append("<td>");
                    html.Append(strVal);
                    html.Append("</td>");
                }
                html.Append("</tr>");
            }
            html.Append("</tbody>");


            //Building the Footer row.
            html.Append("<tfoot>");
            html.Append("<tr>");

            foreach (DataColumn column in dt.Columns)
            {
                if (column.Caption.ToString().Equals("IdStatus") || column.Caption.ToString().Equals("DetalsRem"))
                {
                    html.Append("<th>");
                    html.Append("</th>");
                }
                else
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
            }
            html.Append("</tr>");
            html.Append("</tfoot>");

            //Table end.
            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolderDiary.Controls.Add(new Literal { Text = html.ToString() });
        }

        private void PopulateDataTableClaims(DataTable dt)
        {
            int s = 0;
            int col = 0;
            StringBuilder html = new StringBuilder();

            string strVal = "";
            html.Append("<table id='example' class='table table-striped table-bordered' style='width:100%; font-size:x-small'>");

            html.Append("<thead>");

            html.Append("<tr>");
            foreach (DataColumn column in dt.Columns)
            {
                if (col == 0 ||col == 1 ||col == 10 || col ==11 || col == 12|| col == 13|| col == 14|| col == 15|| col == 16|| col == 17)
                {
                    if (col == 0 || col == 1)
                    {
                        html.Append("<th rowspan='2'>");
                        html.Append("</th>");
                    }
                    if (col == 10)
                    {
                        html.Append("<th colspan='4' style='text-align:center;'>Pending</th>");
                    }
                    if (col == 11)
                    {
                        html.Append("<th colspan='2' style='text-align:center;'>Phone</th>");
                    }if (col == 12)
                    {
                        html.Append("<th colspan='2' style='text-align:center;'>Emails</th>");
                    }
                }
                else
                {
                    html.Append("<th rowspan='2'>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
                col++;
            }
            html.Append("</tr>");

            html.Append("<tr>");
            html.Append("<th>Day</th>");
            html.Append("<th>30</th>");
            html.Append("<th>60</th>");
            html.Append("<th>90</th>");
            html.Append("<th>In</th>");
            html.Append("<th>Out</th>");
            html.Append("<th>In</th>");
            html.Append("<th>Out</th>");
            html.Append("</tr>");

            html.Append("</thead>");
            html.Append("<tbody>");
            //Building the Data rows.
            foreach (DataRow row in dt.Rows)
            {
                col = 0;
                try { s = Convert.ToInt32(row[0].ToString()); } catch (Exception) { s = 0; }

                switch (s)
                {
                    case 2:
                        html.Append("<tr style='background-color: orange; color: white; text-decoration-line: line-through;'>");
                        break;
                    case 3:
                        html.Append("<tr style='text-decoration-line: line-through;'>");
                        break;
                    case 5:
                        html.Append("<tr style='background-color: Maroon; color: white;'>");
                        break;
                    default:
                        html.Append("<tr>");
                        break;
                }

                foreach (DataColumn column in dt.Columns)
                {
                    strVal = "";
                    strVal = row[column.ColumnName].ToString();
                    if (column.Caption.ToString().Equals("IdStatus"))
                    {
                        strVal = "<button id='btnUpd' data-toggle='modal' data-target='#NewReg' class='btn btn-default btn-xs' onclick='document.getElementById(&#39;ifNewReg&#39;).src=&#39;Create.aspx?i=" + row[2] + "&#39;;return false;'>Edit</button>";
                    }
                    if (column.Caption.ToString().Equals("DetalsRem"))
                    {
                        if (strVal.Equals("D"))
                        {
                            strVal = "<a class='btn btn-warning btn-xs' href=Detail.aspx?e=" + row[2] + ">Detail</a>";
                        }
                        else
                        {
                            strVal = "";
                        }
                    }
                    switch (col)
                    {
                        case 10:
                            html.Append("<td title='Pending Day(s) (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 11:
                            html.Append("<td title='Pending 30 Days (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 12:
                            html.Append("<td title='Pending 60 Days (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 13:
                            html.Append("<td title='Pending 90 Days + (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 14:
                            html.Append("<td title='Phone Call(s) In (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 15:
                            html.Append("<td title='Phone Call(s) Out (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 16:
                            html.Append("<td title='Email(s) In (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        case 17:
                            html.Append("<td title='Email(s) Out (" + strVal + ")'>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                        default:
                            html.Append("<td>");
                            html.Append(strVal);
                            html.Append("</td>");
                            break;
                    }
                    col++;
                }
                html.Append("</tr>");
            }
            html.Append("</tbody>");


            //Building the Footer row.
            html.Append("<tfoot>");
            html.Append("<tr>");

            col = 0;
            foreach (DataColumn column in dt.Columns)
            {
                if (col == 0 || col == 1 || col == 10 || col == 11 || col == 12 || col == 13 || col == 14 || col == 15 || col == 16 || col == 17)
                {
                    if (col == 0 || col == 1)
                    {
                        html.Append("<th>");
                        html.Append("</th>");
                    }
                    if (col == 10)
                    {
                        html.Append("<th colspan='4' style='text-align:center;'>Pending</th>");
                    }
                    if (col == 11)
                    {
                        html.Append("<th colspan='2' style='text-align:center;'>Phone</th>");
                    }
                    if (col == 12)
                    {
                        html.Append("<th colspan='2' style='text-align:center;'>Emails</th>");
                    }
                }
                else
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
                col++;
            }
            html.Append("</tr>");
            html.Append("</tfoot>");

            //Table end.
            html.Append("</table>");

            //Append the HTML string to Placeholder.
            PlaceHolderDiary.Controls.Add(new Literal { Text = html.ToString() });
        }

        private void PopulateChekers(List<FilterDiaries> diaryFilter)
        {
            int col = 0;
            PropertyInfo[] Props = typeof(FilterDiaries).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            StringBuilder html = new StringBuilder();
            foreach (PropertyInfo prop in Props)
            {
                if (col > 1)
                {
                    html.Append("<div class='checkbox checkbox-info checkbox-circle checkbox-inline'>" +
                    "<input id='check"+col+ "' class='styled' type='checkbox' onclick='CheckSH(&#39;check" + col+ "&#39;, &#39;" + col + "&#39;);' checked/>" +
                    "<label for='check" + col + "'>" +
                    "" + prop.Name + "" +
                    "</label>" +
                    "</div>");
                }
                col++;
            }
            PlaceHolderChekers.Controls.Add(new Literal { Text = html.ToString() });
        }

        private void PopulateChekersClaims(List<FilterDiariesClaims> diaryFilter)
        {
            int col = 0;
            PropertyInfo[] Props = typeof(FilterDiariesClaims).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            StringBuilder html = new StringBuilder();
            foreach (PropertyInfo prop in Props)
            {
                if (col > 1)
                {
                    html.Append("<div class='checkbox checkbox-info checkbox-circle checkbox-inline'>" +
                    "<input id='check" + col + "' class='styled' type='checkbox' onclick='CheckSH(&#39;check" + col + "&#39;, &#39;" + col + "&#39;);' checked/>" +
                    "<label for='check" + col + "'>" +
                    "" + prop.Name + "" +
                    "</label>" +
                    "</div>");
                }
                col++;
            }
            PlaceHolderChekers.Controls.Add(new Literal { Text = html.ToString() });
        }

        private void GetNotifications()
        {
            try
            {
                BtnReminders.Text = "";
                LblReminders.Text = "";
                lblJavaScript.Text = "";
                int idUsr = Session["SsEmpId"] == null ? 0 : Convert.ToInt32(Session["SsEmpId"].ToString());
                if (idUsr > 0)
                {
                    string t = "";
                    using (FdiaryEntities de = new FdiaryEntities())
                    {
                        int usr = Convert.ToInt32(idUsr);
                        var rem = de.FdReminders.Where(r => r.RemId_usr == idUsr && r.RemStatus == 1).OrderByDescending(r => r.RemDate).ToList();
                        if (rem.Count > 0)
                        {
                            //BtnReminders.Text = "Reminders: " + rem.Count.ToString();
                            LblReminders.Text = "Reminders: " + rem.Count.ToString();
                            lblJavaScript.Text += "<script type='text/javascript'>";
                            foreach (var r in rem)
                            {
                                t = Server.HtmlDecode(r.RemTitle);
                                t += "<br>";
                                t += " scheduled on:  ";
                                try
                                {
                                    t += string.Format("{0:MM/dd/yyyy hh:mm tt}", r.RemDate);
                                }
                                catch (Exception)
                                {
                                    t += " --- ";
                                }
                                lblJavaScript.Text += "fxReminderScreen('" + t + "');";

                            }
                            lblJavaScript.Text += "</script>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        protected void BtnReminders_Click1(object sender, EventArgs e)
        {
            GetNotifications();
            PopulateFDiaries();
        }
    }
}