﻿using System;
using System.Linq;
using System.Data;
using JST_DeDo.Models;
using System.Web.Security;

namespace JST_DeDo
{
    public partial class Login : System.Web.UI.Page
    {
        protected void ValidateUser(object sender, EventArgs e)
        {
            divErr.Visible = false;
            if (txtEmail.Value.Equals("jst") || txtEmail.Value.Equals("JST") || txtEmail.Value.Equals("Jst"))
            {
                CheckLogin("jst@ghins.com");
            }
            else
            {
                if (Utils.Util.IsMailValid(txtEmail.Value))
                {
                    CheckLogin("");
                }
                else
                {
                    divErr.Visible = true;
                    LblMsj1.Text = " Please check E-mail format.";
                }

            }
        }

        protected void CheckLogin(string s)
        {
            try
            {
                string p, pe = "";
                p = txtPassw.Value;
                if (s.Equals(""))
                {
                    s = txtEmail.Value;
                }
                using (FdiaryEntities de = new FdiaryEntities())
                {
                    var objm = de.EmployeeEmails.Where(m => m.Email == s).FirstOrDefault();
                    if (objm != null)
                    {
                        var objL = de.FdLogins.Where(l => l.LEmailID == objm.EmployeeEmailID).FirstOrDefault();
                        pe = Utils.Util.Decrypt(objL.LPassw);
                        if (p.Equals(pe))
                        {
                            DateTime dtupd = DateTime.Today;
                            TimeSpan tsStar = DateTime.Now.TimeOfDay;
                            dtupd = dtupd.Add(tsStar);
                            objL.LLastLogin = dtupd;
                            de.SaveChanges();
                            Session["SsEmpId"] = objL.LEmpID;
                            Session["SsEmailId"] = objL.LEmailID;
                            Session["SsEmail"] = objm.Email;
                            Session["SsProfile"] = objL.LProfile;
                            Session["SsDepartments"] = Utils.Util.StrToListDeptartment(objL.LDepartment);
                            Session["SsDepartmentsCodes"] = objL.LDepartment;
                            Session["SsExt"] = objL.LExt;
                            Session["SsIdSuper"] = objL.LIdSuper;
                            Session["SsGroup"] = objL.LidGroup;
                            Session["SsDependet"] = objL.LIdDependent;
                            //if (objL.LProfile.Equals("2"))
                            //{
                            //    Utils.Util.MergeClaims();
                            //}
                            FormsAuthentication.RedirectFromLoginPage(s, true);
                        }
                        else
                        {
                            divErr.Visible = true;
                            LblMsj1.Text = "Mail and/or password is incorrect.";
                        }
                    }
                    else
                    {
                        divErr.Visible = true;
                        LblMsj1.Text = " Mail provided is not registered. Please add in New User section.";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }
    }
}