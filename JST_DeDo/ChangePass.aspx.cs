﻿using JST_DeDo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JST_DeDo
{
    public partial class ChangePass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    LblMsjErr.Text = "";
                    LblSucces.Text = "";
                }
            }
        }

        protected void BtnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                LblMsjErr.Text = "";
                LblSucces.Text = "";
                if (!Page.User.Identity.IsAuthenticated || Session["SsEmpId"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    if (TxtPassw1.Value == "" || TxtPassw2.Value == "" || (TxtPassw1.Value != TxtPassw2.Value))
                    {
                        divErr.Visible = true;
                        LblMsjErr.Text = "Verify the information in the passwords, passwords must be the same!";
                    }
                    else
                    {
                        DateTime dtupd = DateTime.Today;
                        TimeSpan tsStar = DateTime.Now.TimeOfDay;
                        dtupd = dtupd.Add(tsStar);

                        int idMail = Convert.ToInt32(Session["SsEmailId"].ToString());
                        int idEmpl = Convert.ToInt32(Session["SsEmpId"].ToString());
                        string sMail = Session["SsEmail"] == null ? "" : Session["SsEmail"].ToString();

                        string p, pe = "";
                        p = TxtPassw2.Value;
                        pe = Utils.Util.Encrypt(p);

                        using (FdiaryEntities de = new FdiaryEntities())
                        {
                            var objLogin = de.FdLogins.Where(l => l.LEmailID == idMail && l.LEmpID == idEmpl).FirstOrDefault();
                            if (objLogin != null)
                            {
                                objLogin.LPassw = pe;
                            }
                            de.SaveChanges();
                            divSuccess.Visible = true;
                            LblSucces.Text = "Done! please check your inbox.";
                            if (!sMail.Equals(""))
                            {
                                Utils.Util.SendEmail(sMail, p);
                            }
                            BtnSendMail.Enabled = false;
                            BtnSendMail.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

        }
    }
}