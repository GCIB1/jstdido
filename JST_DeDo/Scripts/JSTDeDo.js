var currFFZoom = 1;
var currIEZoom = 100;

function plus() {
    var step = 0.02;
    currFFZoom += step;
    $('body').css('MozTransform', 'scale(' + currFFZoom + ')');
    var stepie = 2;
    currIEZoom += stepie;
    $('body').css('zoom', ' ' + currIEZoom + '%');
};
function minus() {
    var step = 0.02;
    currFFZoom -= step;
    $('body').css('MozTransform', 'scale(' + currFFZoom + ')');
    var stepie = 2;
    currIEZoom -= stepie;
    $('body').css('zoom', ' ' + currIEZoom + '%');
};