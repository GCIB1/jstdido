﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePass.aspx.cs" Inherits="JST_DeDo.ChangePass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-3.2.1.js"></script>
    <script src="Scripts/Bs3/bootstrap.min.js"></script>

    <link href="Styles/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/Bs3/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <br />
    <form id="frmManageUsr" runat="server">
        <div class="container">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="mb-3">Change Password.</h4>
                        <br />
                        <table class="table table-borderless" id="TblPass">
                            <thead>
                                <tr>
                                    <th>New Password.
                                    </th>
                                    <th>Confirm Password.
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input class="form-control" type="password" id="TxtPassw1" runat="server" value="" placeholder="*****************" />
                                        <input type="checkbox" onclick="showP();" />Show Password
                                    </td>
                                    <td>
                                        <input class="form-control" type="password" id="TxtPassw2" runat="server" value="" placeholder="*****************" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <p class="text-center"><a href="Login.aspx" class="btn">Back to Login.</a></p>
                            </div>
                            <div class="col-md-3 mb-3">
                                <asp:Button ID="BtnSendMail" runat="server" Text="Save & Send Password by Email" class="btn btn-primary btn-xs" Enabled="true" Visible="true" OnClick="BtnSendMail_Click" />
                            </div>
                            <div class="col-md-3 mb-3"></div>
                            <div class="col-md-3 mb-3"></div>
                        </div>
                        <hr class="mb-4" />
                        <div class="alert alert-danger" id="divErr" runat="server" visible="false">
                            <strong>
                                <asp:Label runat="server" ID="LblMsjErr" Text=""></asp:Label>
                            </strong>
                        </div>
                        <div class="alert alert-success" id="divSuccess" runat="server" visible="false">
                            <strong>
                                <asp:Label runat="server" ID="LblSucces" Text=""></asp:Label>
                            </strong>
                        </div>


                        <br />
                        <%--<br />
                        <div style="vertical-align: middle; text-align: right">
                            <asp:Button ID="BtnCancel" runat="server" Text="Close" CssClass="btn btn-danger btn-xs" />
                            &nbsp;|&nbsp;
                            <asp:Button ID="BtnSaveAdmin" Text="Save" runat="server" CssClass="btn btn-primary btn-xs" />
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        function showP() {
            var x = document.getElementById("TxtPassw1");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
            var x = document.getElementById("TxtPassw2");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
</body>
</html>
