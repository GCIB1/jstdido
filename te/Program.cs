﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MergeFHDeDo
{
    class Program
    {
        static void Main(string[] args)
        {
            string _StrCnx = "Data Source=192.168.2.54\\MSSQLSERVER2012;Initial Catalog=FdiaryToDo;User ID=HumanResources;Password=sa123";
            //string _StrCnx = "Data Source=192.168.2.54\\MSSQLSERVER2012;Initial Catalog=FdiaryDes1;User ID=HumanResources;Password=sa123";
            Console.WriteLine(">>> Process Start... <<<");
            DataTable dtClaimsFH = GetLastClaimsFH(_StrCnx, GetAllClaims(_StrCnx));
            Console.WriteLine("Claims on FH [" + dtClaimsFH.Rows.Count+"]");
            DataTable dtClaimsDedo = GetAllClaimsDedo(_StrCnx);
            Console.WriteLine("Claims on DEDO[" + dtClaimsDedo.Rows.Count + "]");
            if (dtClaimsDedo.Rows.Count > 0)
            {
                Console.WriteLine("Update / Insert Proccess Start");
                CompareInsertUpdate(_StrCnx, dtClaimsFH, dtClaimsDedo);
            }
            else
            {
                Console.WriteLine("INSERT Proccess Start");
                SetAllNewClaims(_StrCnx, dtClaimsFH);
            }
            Console.WriteLine(">>> END PROCESS... <<<");
        }

        private static void CompareInsertUpdate(string _StrCnx, DataTable dtClaimsFH, DataTable dtClaimsDedo)
        {
            try
            {
                int i = 0;
                string sQryUpd = "";
                string expression = "";
                DataRow[] foundRows;
                SqlCommand commandUpd = null;
                foreach (DataRow rowFH in dtClaimsFH.Rows)
                {
                    sQryUpd = "";
                    foundRows = null;
                    expression = "claim_num = '" + rowFH[4].ToString() + "'";
                    foundRows = dtClaimsDedo.Select(expression);
                    if (foundRows.Length > 0)
                    {
                        Console.WriteLine("Update <<<<< " + foundRows[0][4]);
                        using (SqlConnection conn = new SqlConnection())
                        {
                            conn.ConnectionString = _StrCnx;
                            conn.Open();
                            commandUpd = new SqlCommand(" update FdClaimDetail set " +
                                "user_id = '" + rowFH[0].ToString().Replace("'", "") + "', claim_id = '" + rowFH[1].ToString().Replace("'", "") + "', log_datetime = '" + rowFH[2].ToString().Replace("'", "") + "', examiner_name = '" + rowFH[3].ToString().Replace("'", "") + "', IntakeWIP#='" + rowFH[5].ToString().Replace("'", "") + "'," +
                                "open_close_status='" + rowFH[6].ToString().Replace("'", "") + "', LIA_PD_CARGO='" + rowFH[7].ToString().Replace("'", "") + "', Lia='" + rowFH[8].ToString().Replace("'", "") + "', Cargo='" + rowFH[9].ToString().Replace("'", "") + "', policy_num='" + rowFH[10].ToString().Replace("'", "") + "', " +
                                "Policy_Effective_Date='" + rowFH[11].ToString().Replace("'", "") + "', holder_state='" + rowFH[12].ToString().Replace("'", "") + "', controlling_state='" + rowFH[13].ToString().Replace("'", "") + "', NameINs_DBA='" + rowFH[14].ToString().Replace("'", "") + "', BrokerName='" + rowFH[15].ToString().Replace("'", "") + "', " +
                                "ClaimantName='" + rowFH[16].ToString().Replace("'", "") + "', Assigned_to_DC_Name='" + rowFH[15].ToString().Replace("'", "").Replace("'", "") + "', Assigned_to_DC_Date='" + rowFH[18].ToString().Replace("'", "") + "', sum_alae_amt='" + rowFH[19].ToString().Replace("'", "") + "', sum_loss_amt='" + rowFH[20].ToString().Replace("'", "") + "', " +
                                "sum_alae_amtNegative='" + rowFH[21].ToString().Replace("'", "") + "', sum_alae_amtPositive='" + rowFH[22].ToString().Replace("'", "") + "', sum_loss_amtNegative='" + rowFH[23].ToString().Replace("'", "") + "', sum_loss_amtPositive='" + rowFH[24].ToString().Replace("'", "") + "', PD='" + rowFH[25].ToString().Replace("'", "") + "', " +
                                "diff_day='" + rowFH[26].ToString().Replace("'", "") + "', diff_date='" + rowFH[27].ToString().Replace("'", "") + "', map_date='" + rowFH[28].ToString().Replace("'", "") + "', sync_date='" + rowFH[29].ToString().Replace("'", "") + "', sync_status='" + rowFH[30].ToString().Replace("'", "") + "', " +
                                "log_datetime_id='" + rowFH[31].ToString().Replace("'", "") + "', claim_task='" + rowFH[32].ToString().Replace("'", "") + "', handler_id='" + rowFH[33].ToString().Replace("'", "") + "', touch_user_id='" + rowFH[34].ToString().Replace("'", "") + "', touch_claim_name='" + rowFH[35].ToString().Replace("'", "") + "' " +
                                "WHERE claim_num = '" + foundRows[0][4] + "'", conn);
                            //Console.WriteLine(commandUpd.CommandText);
                            commandUpd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }else{
                        Console.WriteLine("Insert >>>>>>> " + expression);
                        sQryUpd = "INSERT INTO FdClaimDetail " +
                        "(user_id, claim_id, log_datetime, examiner_name, claim_num " +
                        " ,IntakeWIP#, open_close_status, LIA_PD_CARGO, Lia, Cargo " +
                        " ,policy_num, Policy_Effective_Date, holder_state, controlling_state, NameINs_DBA" +
                        " ,BrokerName, ClaimantName, Assigned_to_DC_Name, Assigned_to_DC_Date, sum_alae_amt " +
                        " ,sum_loss_amt, sum_alae_amtNegative, sum_alae_amtPositive, sum_loss_amtNegative, sum_loss_amtPositive " +
                        " ,PD, diff_day, diff_date, map_date, sync_date, sync_status " +
                        " ,log_datetime_id,claim_task,handler_id,touch_user_id, touch_claim_name)" +
                        " VALUES ";
                            sQryUpd += "(";
                            for (i = 0; i <= 35; i++)
                            {
                                sQryUpd += "'" + rowFH[i].ToString().Replace("'", "") + "',";
                            }
                            sQryUpd = sQryUpd.TrimEnd(',');
                            sQryUpd += ")";
                        using (SqlConnection conn = new SqlConnection())
                        {
                            conn.ConnectionString = _StrCnx;
                            conn.Open();
                            commandUpd = new SqlCommand(sQryUpd, conn);
                            commandUpd.ExecuteReader();
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ex.ToString();
            }
            return;
        }

        private static void SetAllNewClaims(string _StrCnx, DataTable dtClaimsFH)
        {
            try
            {
                Console.WriteLine(">>> ((( SetAllNewClaims ))) <<<");
                int i = 0;
                string sQry = "";
                if (dtClaimsFH.Rows.Count > 0)
                {
                    sQry = "INSERT INTO FdClaimDetail " +
                            "(user_id, claim_id, log_datetime, examiner_name, claim_num " +
                            " ,IntakeWIP#, open_close_status, LIA_PD_CARGO, Lia, Cargo " +
                            " ,policy_num, Policy_Effective_Date, holder_state, controlling_state, NameINs_DBA" +
                            " ,BrokerName, ClaimantName, Assigned_to_DC_Name, Assigned_to_DC_Date, sum_alae_amt " +
                            " ,sum_loss_amt, sum_alae_amtNegative, sum_alae_amtPositive, sum_loss_amtNegative, sum_loss_amtPositive " +
                            " ,PD, diff_day, diff_date, map_date, sync_date, sync_status " +
                            " ,log_datetime_id,claim_task,handler_id,touch_user_id, touch_claim_name)" +
                            " VALUES ";

                    foreach (DataRow row in dtClaimsFH.Rows)
                    {
                        sQry += "(";
                        for (i = 0; i <= 35; i++)
                        {
                            sQry += "'"+ row[i].ToString().Replace("'", "")+ "',";
                        }
                        sQry = sQry.TrimEnd(',');
                        sQry += "),";
                    }
                    sQry = sQry.TrimEnd(',');
                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = _StrCnx;
                        conn.Open();
                        SqlCommand commandUpd = new SqlCommand(sQry, conn);
                        commandUpd.ExecuteReader();
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ex.ToString();
            }
            return;
        }

        private static DataTable GetLastClaimsFH(string _StrCnx, DataTable dtClaimsId)
        {
            var dt = new DataTable("ClaimsFH");
            string c = "";
            int i = 0;
            try
            {
                for (i = 0; i <= 35; i++)
                {
                    if (i == 4)
                    {
                        dt.Columns.Add("claim_num");
                    }
                    else
                    {
                        dt.Columns.Add();
                    }
                }

                if (dtClaimsId.Rows.Count > 0)
                {
                    foreach (DataRow row in dtClaimsId.Rows)
                    {
                        if (!row.Field<string>(0).ToString().Equals(""))
                        {
                            using (SqlConnection conn = new SqlConnection())
                            {
                                c = row.Field<string>(0);
                                conn.ConnectionString = _StrCnx;
                                conn.Open();
                                SqlCommand commandSel = new SqlCommand("select user_id,claim_id,log_datetime,examiner_name,claim_num,IntakeWIP#,open_close_status,LIA_PD_CARGO,Lia,Cargo,policy_num,Policy_Effective_Date,holder_state,controlling_state,NameINs_DBA,BrokerName,ClaimantName,Assigned_to_DC_Name,Assigned_to_DC_Date,sum_alae_amt,sum_loss_amt,sum_alae_amtNegative,sum_alae_amtPositive,sum_loss_amtNegative,sum_loss_amtPositive,PD,diff_day,diff_date,map_date,sync_date,sync_status,log_datetime_id,claim_task,handler_id,touch_user_id,touch_claim_name from FHTestClaim where claim_num = '" + c + "' and FHId = (select Max(FHId) from FHTestClaim where claim_num = '" + c + "')", conn);
                                using (SqlDataReader readerSel = commandSel.ExecuteReader())
                                {
                                    if (readerSel.HasRows)
                                    {
                                        while (readerSel.Read())
                                        {
                                            var rowFH = dt.NewRow();
                                            for (i = 0; i <= 35; i++)
                                            {
                                                if (i == 4)
                                                {
                                                    rowFH["claim_num"] = readerSel[i].ToString();
                                                }
                                                else
                                                {
                                                    rowFH[i] = readerSel[i].ToString();
                                                }
                                            }
                                            dt.Rows.Add(rowFH);
                                        }
                                    }
                                }
                                conn.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ex.ToString();
            }
            return dt;
        }

        private static DataTable GetAllClaimsDedo(string _StrCnx)
        {
            var dt = new DataTable("ClaimsDeDo");
            int i = 0;
            try
            {
                for (i = 0; i <= 35; i++)
                {
                    if (i == 4)
                    {
                        dt.Columns.Add("claim_num");
                    }
                    else
                    {
                        dt.Columns.Add();
                    }
                }
                
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = _StrCnx;
                    conn.Open();
                    SqlCommand commandD = new SqlCommand(" SELECT user_id,claim_id,log_datetime,examiner_name,claim_num,IntakeWIP#,open_close_status,LIA_PD_CARGO,Lia,Cargo,policy_num,Policy_Effective_Date,holder_state,controlling_state,NameINs_DBA,BrokerName,ClaimantName,Assigned_to_DC_Name,Assigned_to_DC_Date,sum_alae_amt,sum_loss_amt,sum_alae_amtNegative,sum_alae_amtPositive,sum_loss_amtNegative,sum_loss_amtPositive,PD,diff_day,diff_date,map_date,sync_date,sync_status,log_datetime_id,claim_task,handler_id,touch_user_id,touch_claim_name FROM FdClaimDetail ", conn);
                    using (SqlDataReader readerD = commandD.ExecuteReader())
                    {
                        if (readerD.HasRows)
                        {
                            while (readerD.Read())
                            {
                                var row = dt.NewRow();
                                for (i = 0; i <= 35; i++)
                                {
                                    if (i == 4)
                                    {
                                        row["claim_num"] = readerD[i].ToString();
                                    }
                                    else
                                    {
                                        row[i] = readerD[i].ToString();
                                    }
                                }
                                dt.Rows.Add(row);
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ex.ToString();
            }
            return dt;
        }

        private static DataTable GetAllClaims(string _StrCnx)
        {
            var dt = new DataTable("Claims");
            try
            {
                dt.Columns.Add("claim");
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = _StrCnx;
                    conn.Open();
                    SqlCommand commandD = new SqlCommand(" select distinct(claim_num) from FHTestClaim ", conn);
                    using (SqlDataReader readerD = commandD.ExecuteReader())
                    {
                        if (readerD.HasRows)
                        {
                            while (readerD.Read())
                            {
                                var row = dt.NewRow();
                                row["Claim"] = readerD[0].ToString();
                                dt.Rows.Add(row);
                            }
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ex.ToString();
            }
            return dt;
        }
    }
}
